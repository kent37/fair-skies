# The mystery of 2017_06
# Why are there so many low flights?
# Why are there so many operations on other runways?

library(tidyverse)
library(here)
library(mapview)
library(sf)

date = '2017_06'
smooth = 2
path = "/Users/kent/Dev/Fair Skies-TEKKK//TEKKK_2017_06.gpkg"
tr = read_sf(path) %>%
  filter(between(altitude_at_min, 500, 8000), equip != 'HELO')
label_format = '{flight} ({equip})'
popup_fields = c('flight', 'equip', 'waypoint',
  'min_dist', 'altitude_at_min', 'time')
tr = tr %>%
  st_zm %>% st_simplify(dTolerance=0.001*smooth) %>%
  mutate(label = str_glue(label_format),
  popup = popupTable(.,
    popup_fields, row.numbers=FALSE))

range = c(500, 2000)
tr = tr %>% filter(between(altitude_at_min, range[1], range[2]))
mapview(tr, alpha=0.1. lwd=1)

fl = tr %>% filter(flight=='AAL9677')
mapview(fl)
fl$time
# "2017-06-17 23:14:31 EDT" "2017-06-17 23:14:32 EDT"

# Read the raw data for this flight
source(here('TrackR/dump1090.R'))

base_path = '/Volumes/TRACKS/FAA FOIA data/2017/'
pattern = 'Offload_track_IFR_2017061[7|8].*'
files = list.files(base_path, pattern=pattern, full.names=TRUE)
cat('\nImporting', length(files), 'files from', pattern, '\n')

raw = map_df(files, ~read_faa_tracks(.x))
r = raw %>% filter(AIRCRAFT_ID=='AAL9677') %>% 
  st_as_sf(coords=c('lon', 'lat'), crs=4326)
mapview(r, zcol='TRACK_POINT_TIME_UTC')

pattern = 'Offload_track_IFR_2017062[2|3].*'
files = list.files(base_path, pattern=pattern, full.names=TRUE)
raw2 = map_df(files, ~read_faa_tracks(.x))
r2 = raw2 %>% filter(AIRCRAFT_ID=='XOJ772') %>% 
  st_as_sf(coords=c('lon', 'lat'), crs=4326)
mapview(r2, zcol='TRACK_POINT_TIME_UTC')
