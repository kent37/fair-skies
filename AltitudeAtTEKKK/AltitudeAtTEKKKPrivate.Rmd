---
title: "Altitude of 33L Departures at TEKKK 2010-2017"
author: ""
date: '`r Sys.Date()`'
output: kjutil::small_format
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(tidyverse)
library(lubridate)
library(here)
library(readxl)

knitr::opts_chunk$set(echo=FALSE, fig.width=8, fig.height=4, comment=NA, warning=FALSE, message=FALSE)
```

This is a preliminary investigation of the altitude of 
Logan 33L departures at the point of nearest approach to
the TEKKK waypoint for the period Jan. 2010 - Nov. 2017.

Flight data is from the FAA via FOIA request except for Oct. 2016 which
is from ADS-B.

## Flight counts

Other than 2016-10, the number of flights corresponds roughly to the 
Massport published departure numbers. Discrepancies are likely due to
the difficulty of converting the FAA data to flight tracks and assigning
the tracks to runways.

```{r data}
data_base = '~/Dev/Fair Skies-TEKKK/' %>% path.expand

all_flights = read_csv(file.path(data_base, 'all_flights.csv')) %>% 
  filter(between(altitude_at_min, 500, 8000))

# Compare the number of flights to the Massport numbers
month_lookup = 1:12 %>% set_names(month.abb)
massport = read_excel(here('KBos Data 2015/YearlyRunwayUse.xlsx')) %>% 
  filter(Runway=='33L') %>% 
  gather(Month_type, Count, Jan_arr:Dec_dep) %>% 
  separate(Month_type, c('Month', 'Type')) %>% 
  filter(Type=='dep') %>% 
  mutate(Month = month_lookup[Month]) %>% 
  select(Year, Month, Count)

faa_months = all_flights %>% 
  mutate(Year = year(time), Month=month(time)) %>% 
  group_by(Year, Month) %>% 
  summarise(Count=n()) %>% ungroup %>% 
  complete(Year, Month, fill=list(Count=0)) %>% 
  filter(Year!=2017 | Month!=12)

all_months = faa_months %>% 
  left_join(massport, by=c('Year', 'Month'), suffix=c('.faa', '.mp'))
```

```{r fig.width=6, fig.height=4}
qplot(Count.mp, Count.faa, data=all_months) + 
  geom_abline(slope=1, color='blue') + theme_bw() +
  labs(x='Massport flight count', y='FAA-derived flight count', 
       title='Comparison of 33L departure counts',
       subtitle = 'FAA-derived data vs Massport published counts')

```

## Median altitude

This chart shows the distribution of altitude of 33L departures at the
point of nearest approach to TEKKK. The red dots are the median altitude.

This data shows a clear seasonal trend.

```{r}
all_flights = all_flights %>% 
  mutate(ym=floor_date(time, unit='months'), 
         year=year(time), month=month(time)) 

ggplot(all_flights, aes(as.Date(ym), altitude_at_min, group=ym)) + 
  geom_boxplot(outlier.shape='.') + 
  stat_summary(fun.y = "median", colour = "red", geom = "point") +
  scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y') +
  labs(x='Year and Month', y='Altitude at closest point to TEKKK (ft)',
       title='Logan 33L Departures - Altitude at TEKKK') +
  ylim(NA, 10000) + theme_bw()
```

The number of flights also has a seasonal trend:

```{r}
alt_median = all_flights %>% group_by(year, month) %>% 
  summarize(alt_median=median(altitude_at_min), count=n()) %>% 
  ungroup %>% 
  complete(year, month, 
           fill=list(alt_median=2600, count=0)) %>% 
  filter(year!=2017 | month!=12) %>% 
  mutate(ym=as.POSIXct(str_glue('{year}-{month}-01'), tz='UTC'))

ggplot(alt_median, aes(as.Date(ym), count)) + geom_line() + theme_bw() +
  scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y') +
  labs(x='Year and Month', y='Number of Departures',
       title='Number of 33L Departures per Month, 2010-2017')
```

Perhaps there is a seasonal change in equipment mix which is responsible
for the variation? Look at the four most prevalent equipment types.
They each
show prominent seasonal variation so equipment mix does not seem to explain
the variation.

```{r fig.height=6}
all_flights %>% filter(equip %in% c('A320', 'E190', 'B738', 'A319')) %>% 
ggplot(aes(as.Date(ym), altitude_at_min, group=ym)) + 
  geom_boxplot(outlier.shape='.') + 
  stat_summary(fun.y = "median", colour = "red", geom = "point", size=1) +
  scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y') +
  facet_wrap(~equip, ncol=1) +
  ylim(NA, 7500) + theme_bw() +
  labs(x='Year and Month', y='Altitude at closest point to TEKKK (ft)',
       title='Logan 33L Departures - Altitude at TEKKK',
       subtitle='Most prevalent equipment types')
```

### Seasonal variation

Decomposing the median altitude into seasonal variation, trend, and residual
shows a clear seasonal component and a slight decreasing trend.

The seasonal component varies by +300=-500 feet. The trend decreases
by about 200-300 feet over the period.

```{r fig.height=6}
alt_ts = ts(alt_median[,c('alt_median', 'count')], frequency = 12, start = c(2010, 1))

library(ggseas)
ggsdc(tsdf(alt_ts), aes(x, alt_median), method = "seas",
          facet.titles = c("Median of observed altitudes", "Trend",
                           "Seasonal component", "Residual")) +
  geom_line(color='darkred') + 
  scale_x_continuous(breaks=2010:2018, minor_breaks=seq(2010, 2018, by=1/12)) +
  theme_bw() + labs(x='Year and Month', 
                    y='Altitude (change) at closest point to TEKKK (ft)',
       title='Logan 33L Departures - Seasonality and Trend of Altitude at TEKKK')

ggsdc(tsdf(alt_ts), aes(x, count), method = "seas",
          facet.titles = c("Number of departures", "Trend",
                           "Seasonal component", "Residual")) +
  geom_line(color='darkred') + 
  scale_x_continuous(breaks=2010:2018, minor_breaks=seq(2010, 2018, by=1/12)) +
  theme_bw() + labs(x='Year and Month', 
                    y='Number of departures',
       title='Logan 33L Departures - Seasonality and Trend of Departure Counts')

```

<small>Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>