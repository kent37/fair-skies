# Get 4LR flights with some metrics

library(tidyverse)
library(glue)
library(here)
library(sf)

source(here::here('PostGIS/DB.R'))
source(here::here('TrackR/DistanceAndLocation.R'))
source(here::here('TrackR/Waypoints.R'))
con=connect()

# This is the intersection of Morissey Blvd and Hancock St
# at the mouth of the Neponset River
# 4R arrivals seem to go right over it both pre and post 2013.
morrissey = st_point(c(-71.042617, 42.285804)) %>% st_sfc(crs=4326)

# Approximately 1/2 nm in degrees of arc
dist = (1/60/2) %>% units::set_units('arc_degrees')
morrissey_box = st_buffer(morrissey, dist) %>% st_bbox %>% st_as_sfc

# Fontbonne Academy
fontbonne = st_point(c(-71.0594803, 42.2595406)) %>% st_sfc(crs=4326)

# Waypoints
MILTT = st_point(c(-71.049192, 42.273650)) %>% st_sfc(crs=4326)
NABBO = st_point(c(-71.087000, 42.195189)) %>% st_sfc(crs=4326)

# Get distance to morrissey and fontbonne for a month
distance_to_morrissey_for_month = function(year, month) {
  tr = morrissey_for_month(year, month)
  if (nrow(tr) == 0) return(NULL)
  
  dm = distance_to_morrissey(tr) %>% 
    mutate(time=as.POSIXct(time,origin = "1970-01-01", 
                                  tz = "America/New_York"))

  dfb = distance_to_fontbonne(tr)
  tr %>% select(-time) %>% bind_cols(dm) %>% 
    bind_cols(dfb) %>% 
    filter(altitude_at_min<3000)
}

# Get rough 4R/L data for one month
morrissey_for_month = function(year, month) {
  morrissey_box_wkt = st_as_text(morrissey_box)
  query = glue("select * from tracks.foia where operation='arrival' and ST_Intersects(track, ST_GeomFromEWKT('SRID=4326;{morrissey_box_wkt}')) and year={year} and month={month}")
  tr = with_db_connection(list(con=connect()),
    st_read_db(con, query=query))
}

# Get distance to morrissey for some tracks
distance_to_morrissey = function(tr) {
  near = nearest_point_and_altitude(tr, st_zm(morrissey)) %>% 
    select(min_dist, altitude_at_min, min_lon, min_lat, time)
}

distance_to_fontbonne = function(tr) {
  near = nearest_point_and_altitude(tr, st_zm(fontbonne)) %>% 
    select(min_dist_fb=min_dist, 
           altitude_at_fb=altitude_at_min, 
           min_lon_fb = min_lon, min_lat_fb=min_lat)
}

distance_to_miltt = function(tr) {
  near = nearest_point_and_altitude(tr, st_zm(MILTT)) %>% 
    select(min_dist_miltt=min_dist, 
           altitude_at_miltt=altitude_at_min, 
           min_lon_miltt = min_lon, min_lat_miltt=min_lat)
}

distance_to_nabbo = function(tr) {
  near = nearest_point_and_altitude(tr, st_zm(NABBO)) %>% 
    select(min_dist_nabbo=min_dist, 
           altitude_at_nabbo=altitude_at_min, 
           min_lon_nabbo = min_lon, min_lat_nabbo=min_lat)
}
