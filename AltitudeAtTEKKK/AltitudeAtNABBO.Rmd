---
title: "Altitude of 4R/L arrivals in Milton 2010-2017"
author: ""
date: '`r Sys.Date()`'
output: kjutil::small_format
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(tidyverse)
library(imputeTS)
library(lubridate)

source(here::here('AltitudeAtTEKKK/EquipmentTypes.R'))
theme_set(theme_minimal(base_size=12) + theme(legend.position="bottom"))
knitr::opts_chunk$set(echo=FALSE, fig.width=8, fig.height=6
                      , comment=NA, warning=FALSE, message=FALSE)
```

<style>
body { 
 font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
 font-weight: 400;
 font-size: 1.0em;
}
</style>

## Summary

This report investigates the altitude of flights arriving on Logan runway 4R/L
as they pass near the NABBO waypoint in Milton. 
Using flight track data from 2010-2017,
it compares altitude over time. 

There is a  seasonal component to the altitude; the mean altitude in winter is 
about 100 feet higher than the mean summer altitude. 
This is true for all aircraft types.

There is some variation in altitude across aircraft types. Of the jet
aircraft, single-aisle and wide-body jets are the lowest. 
Regional jets are slightly higher. 
Business jets (biz jets) are higher
still.

There may be a slight decreasing trend of the mean altitude. Over the study
period, the mean altitude at NABBO decreases by about 30 feet.

The seasonal variation is several times larger than any trend. 

```{r data, cache=TRUE}
data_base = '~/Dev/Fair Skies-Milton/' %>% path.expand

all_milton = read_csv(file.path(data_base, 'all_flights.csv')) %>% 
  mutate(category=equip_category(equip))
```

## Mean altitude

This chart shows the distribution of altitude of 4R/L arrivals at the
point of nearest approach to NABBO. The red dots are the mean altitude.

This data shows a clear seasonal trend, with flights flying higher in winter
than in summer.

```{r fig.height=4}
all_milton = all_milton %>% 
  mutate(time=with_tz(time_morrissey, 'US/Eastern'),
         ym=floor_date(time, unit='months'), 
         year=year(time), month=month(time)) 

ggplot(all_milton, aes(as.Date(ym), altitude_at_nabbo, group=ym)) + 
  geom_boxplot(outlier.shape='.') + 
  stat_summary(fun.y = "mean", colour = "red", geom = "point") +
  scale_x_date(date_breaks='years', date_minor_breaks='6 months', date_labels='%Y') +
  labs(x='Year and Month', y='Altitude at closest point to NABBO (ft)',
       title='Logan 4R/L Arrivals - Monthly altitude at NABBO') +
  ylim(2000, 5000)
```

## Equipment types

Single-aisle jets account for most jet arrivals.

```{r fig.width=6, fig.height=4}
non_jets = c('GA', 'Other', 'Helo', 'Regional prop')
all_milton %>%
  filter(!category %in% non_jets) %>% 
  count(category) %>% 
  ggplot(aes(fct_reorder(category, n), n, fill=category)) +
  geom_col() + coord_flip() +
  scale_fill_brewer(palette='Set1', guide='none') +
  labs(y='Number of arrivals', x='Equipment type', 
       title='Total jet arrivals by equiment type, 2010-2017')

all_milton %>%
  filter(!category %in% non_jets) %>% 
  group_by(year) %>% 
  count(category) %>% ungroup %>% 
  ggplot(aes(year, n, color=category)) +
  geom_line(size=1) + 
  scale_color_brewer(palette='Set1') +
  scale_y_continuous(minor_breaks=NULL) +
  labs(x='Year', color='Equipment type:', y='Number of arrivals',
       title='Yearly jet arrivals by equipment type, 2010-2017')

```

## Altitude by equipment category

Wide-body and single-aisle jets have the lowest mean altitude, followed by 
regional jets, and biz jets (the highest). 

Seasonal variation occurs for all jet categories.

```{r fig.height=6}

all_milton %>% 
  filter(!category %in% non_jets) %>% 
ggplot(aes(as.Date(ym), altitude_at_nabbo, color=category)) + 
  geom_line(size=1, stat='summary', fun.y = "mean", ) +
  scale_x_date(date_breaks='years', 
               minor_breaks=NULL,
               date_labels='%Y') +
  scale_y_continuous(minor_breaks=NULL) +
  scale_color_brewer(palette='Set1') +
  labs(x='Year and Month', y='Altitude at closest point to NABBO (ft)',
       title='Logan 4R/L Arrivals - Monthly altitude at NABBO', color='Equipment type:')

```

## Seasonal variation and trend

Decomposing the mean altitude into seasonal variation, trend, and residual
shows a significant seasonal component. All jets other than wide-body show
a slight decreasing trend in altitude.

The seasonal component varies by about 100 feet from summer to winter.

```{r fig.height=7}
library(ggseas)

alt_all = all_milton %>% 
  filter(!category %in% c('GA', 'Other', 'Helo', 'Regional prop')) %>% 
  group_by(category, year, month) %>% 
  summarize(alt_mean=mean(altitude_at_nabbo)) %>% 
  ungroup %>% 
  complete(year, month, category) %>% 
  filter(year!=2017 | month!=12) %>% 
  mutate(ym=as.Date(paste0(year, '-', month, '-01'))) %>% 
  select(-year, -month) %>% 
  spread(category, alt_mean) %>% 
  # mutate(`Biz jet`=na.interpolation(`Biz jet`),
  #        `Regional jet`=na.interpolation(`Regional jet`),
  #        `Single aisle`=na.interpolation(`Single aisle`),
  #        `Wide body`=na.interpolation(`Wide body`)) %>% 
  gather('category', 'alt_mean', -ym)

ggsdc(alt_all, aes(x=ym, y=alt_mean, color=category), method = "stl",
      frequency=12, start=2010, s.window=7,
          facet.titles = c("Mean of observed altitudes", "Trend",
                           "Seasonal component", "Residual")) +
  geom_line(size=0.5) + 
  scale_color_brewer(palette='Set1') +
  #scale_x_continuous(breaks=2010:2018, minor_breaks=seq(2010, 2018, by=1/12)) +
  labs(x='Year and Month', 
                    y='Number of departures', color='Equipment type:',
       title='Logan 4R/L Arrivals - Seasonality and Trend of Altitude at NABBO', subtitle='Jets only') +
  theme(strip.text=element_text(hjust=0, size=rel(1.1)))

```

## Sources

Altitude data is derived from detailed flight track data obtained from 
the FAA via FOIA request for the period Jan. 2010 - Nov. 2017.

<small>Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>