# Get and save info about 4R/L flights
library(lubridate) # before `here`
library(tidyverse)

source('AltitudeAtMilton.R')
library(readxl)

years = 2010:2017
months = 1:12

out_base = '~/Dev/Fair Skies-Milton/' %>% path.expand
process_month = function(year, month) {
  cat('Processing', year, '-', month, '\n')
  tr = distance_to_morrissey_for_month(year, month)
  
  if (!is.null(tr) && nrow(tr) > 0) {
    out_name = glue('Milton_{year}_{sprintf("%02d", month)}.gpkg')
    st_write(tr, file.path(out_base, out_name))
  }
}

ym = cross_df(list(year=years, month=months))
ym %>% pwalk(.f=process_month)

# First run succeeded through 2011-09
ym = ym[-(1:66),]

# Third batch of FAA data
process_month(2017, 12)

ym = cross_df(list(year=2018, month=1:12))
ym %>% pwalk(.f=process_month)

ym = cross_df(list(year=2019, month=1:3))
ym %>% pwalk(.f=process_month)

# Create an aggregated data frame with everything but the track
# This is not working with the data from third batch of FAA data
out_files = list.files(out_base, pattern='\\.gpkg', full.names=TRUE)

all_flights = map_dfr(out_files, ~{
  cat(.x, '\n')
  tr = st_read(.x, stringsAsFactors=FALSE, quiet=TRUE) 
  
  # Add distances to MILTT and NABBO
  dmiltt = distance_to_miltt(tr)
  dnabbo = distance_to_nabbo(tr)
  tr = bind_cols(tr, dmiltt, dnabbo)
  tr %>% as_data_frame %>% select(-geom) %>% 
    rename(min_dist_morrissey=min_dist, 
           altitude_at_morrissey=altitude_at_min, time_morrissey=time,
           min_lon_morrissey = min_lon, min_lat_morrissey=min_lat)
})

write_csv(all_flights, file.path(out_base, 'all_flights.csv'))
saveRDS(all_flights, file.path(out_base, 'all_flights.RDS'))
