# Explore altitude at TEKKK
library(tidyverse)
library(leaflet)
library(lubridate)
library(mapview)
library(readxl)
library(sf)

out_base = '~/Dev/Fair Skies-Milton/' %>% path.expand
all_flights = read_csv(file.path(out_base, 'all_flights.csv'))
all_flights = all_flights %>% 
  mutate(runway = ifelse(min_dist_morrissey<1000, '04R', '04L'))

# Compare the number of flights to the Massport numbers
month_lookup = 1:12 %>% set_names(month.abb)
massport = read_excel(
  here::here('KBos Data 2015/YearlyRunwayUse.xlsx')) %>% 
  filter(Runway %in% c('04R', '04L')) %>% 
  gather(Month_type, Count, Jan_arr:Dec_dep) %>% 
  separate(Month_type, c('Month', 'Type')) %>% 
  filter(Type=='arr') %>% 
  mutate(Month = month_lookup[Month]) %>% 
  select(Year, Month, Runway, Count)

faa_months = all_flights %>% 
  mutate(ym=floor_date(time_morrissey, unit='months'), 
         Year = year(time_morrissey), Month=month(time_morrissey)) %>% 
  group_by(ym, Year, Month, runway) %>% 
  summarise(Count=n()) %>% 
  ungroup

all_months = faa_months %>% 
  left_join(massport, by=c('Year', 'Month', runway='Runway'), suffix=c('.faa', '.mp'))

qplot(Count.mp, Count.faa, color=runway, data=all_months) + 
  geom_abline(slope=1, color='blue') +
  facet_wrap(~runway, scales='free')
qplot((Count.mp+Count.faa)/2, (Count.mp-Count.faa), color=runway, data=all_months)

qplot(as.Date(ym), Count.mp, data=all_months, geom='line') +
  geom_line(aes(y=Count.faa), color='red') +
    scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y') + facet_wrap(~runway)

all_flights = all_flights %>% 
  mutate(ym=floor_date(time_morrissey, unit='months'), 
         year=year(time_morrissey), month=month(time_morrissey)) 

all_4r = all_flights %>% filter(runway=='04R')
all_4l = all_flights %>% filter(runway=='04L')

# How about a heatmap?
ggplot(all_4r, aes(as.Date(ym), altitude_at_miltt)) + 
  geom_bin2d(binwidth=c(31, 100)) +
    scale_x_date(date_breaks='years', 
                 date_minor_breaks='months', date_labels='%Y')+
  theme_bw() + scale_fill_viridis_c(option='E') + ylim(500, 4000)

ggplot(all_4l, aes(as.Date(ym), altitude_at_miltt)) + 
  geom_bin2d(binwidth=c(31, 100)) +
    scale_x_date(date_breaks='years', 
                 date_minor_breaks='months', date_labels='%Y')+
  theme_bw() + scale_fill_viridis_c(option='E') + ylim(500, 4000)

ggplot(all_4r, aes(as.Date(ym), altitude_at_nabbo)) + 
  geom_bin2d(binwidth=c(31, 100)) +
    scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y')+
  theme_bw() + scale_fill_viridis_c(option='E') + ylim(1000, 6000)

ggplot(all_4l, aes(as.Date(ym), altitude_at_nabbo)) + 
  geom_bin2d(binwidth=c(31, 100)) +
    scale_x_date(date_breaks='years', date_minor_breaks='months', date_labels='%Y')+
  theme_bw() + scale_fill_viridis_c(option='E') + ylim(1000, 6000)

# Look at median altitude over time
alt_median = all_flights %>% group_by(year, month, ym, runway) %>% 
  summarize(alt_median=median(altitude_at_miltt), count=n()) %>% 
  ungroup

(bp = ggplot(all_flights, aes(ym, altitude_at_miltt, group=ym)) + 
  geom_boxplot(outlier.shape='.') + 
  stat_summary(fun.y = "median", colour = "red", geom = "point") +
  labs(x='Year and Month', y='Altitude at closest point to TEKKK (ft)',
       title='Logan 33L Departures - Altitude at TEKKK') +
  ylim(1000, 2500) + theme_bw()) + facet_wrap(~runway)

# Look at just the most prevalent equipment types
all_flights %>% filter(equip %in% c('A320', 'E190', 'B738')) %>% 
ggplot(aes(ym, altitude_at_miltt, group=ym)) + 
  geom_boxplot(outlier.shape='.') + 
  stat_summary(fun.y = "median", colour = "red", geom = "point") +
  facet_grid(runway~equip) +
  ylim(1000, 2500) + theme_bw()

all_flights %>% 
  filter(equip %in% c('A320', 'E190', 'B738')) %>% 
  group_by(ym, equip) %>% 
  summarize(count=n()) %>% 
  ggplot(aes(ym, count, group=equip)) + geom_line() + facet_wrap(~equip, ncol=1)

ggplot(all_flights, aes(y=altitude_at_nabbo, x=as.numeric(ym))) + 
  geom_jitter(size=1, alpha=0.008) + ylim(1500, 5000) + theme_bw()


# Seasonal decomposition
alt_ts = ts(alt_median %>% filter(runway=='04R') %>% pull(alt_median), frequency = 12, start = c(2010, 1))
plot(decompose(alt_ts))

library(ggseas)
p = ggsdc(tsdf(alt_ts), aes(x, y), method = "stl",
          frequency=12, s.window=7,
          facet.titles = c("Median of observed altitudes", "Trend",
                           "Seasonal component", "Residual"))
p + geom_line(color='darkred') +
  scale_x_continuous(breaks=2010:2018, minor_breaks=seq(2010, 2018, by=1/12)) +
  theme_bw() + labs(x='Year and Month', 
                    y='Altitude at closest point to TEKKK (ft)',
       title='Logan 33L Departures - Seasonal Decomposition of Altitude at TEKKK')

qplot(ym, count, data=alt_median, geom='line')

alt_count = ts(alt_median$count, frequency = 12, start = c(2010, 1))
plot(decompose(alt_count))

qplot(year, alt_median, data=alt_median, geom='line') + facet_grid(runway~month) + stat_smooth()
