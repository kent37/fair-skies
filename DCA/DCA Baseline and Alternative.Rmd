---
title: "DCA Baseline & Alternative Scenarios Noise Levels for the LAZIR-B proposal"
author: "Kent Johnson"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output:
  html_vignette:
    css: ~/Google Drive/vignette.css
---

Baseline and alternative noise levels for Reagan National Airport LAZIR-B proposal.

__Please be patient!__ The maps take a few moments to load!

Data from https://www.faa.gov/nextgen/communityengagement/dc/media/DCA%20Baseline%20&%20Alternative%20Scenarios%20Noise%20Levels.xlsx. Visualization by Kent S Johnson.

The first map shows the baseline and alternative DNL levels. Use the layers control to switch between them. Mouse over the map to see the level at any location.

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE, fig.width=8, fig.height=8, results='asis',
                      comment=NA, warning=FALSE, message=FALSE)

# Never split tables
pander::panderOptions("table.split.table", Inf)

library(tidyverse)
library(leaflet)
library(readxl)
baseline = read_excel('DCA Baseline & Alternative Scenarios Noise Levels.xlsx',
                      skip=1) %>% 
  rename(id=`Grid Point Identifier`, lat=`Latitude (deg)`, lon=`Longitude (deg)`, DNL=`Baseline Scenario DNL Noise Level (dB)`) %>% select(-id)

alternate = read_excel('DCA Baseline & Alternative Scenarios Noise Levels.xlsx',
                      skip=1, sheet=2) %>% 
  rename(id=`Grid Point Identifier`, lat=`Latitude (deg)`, lon=`Longitude (deg)`, DNL=`Alternative Scenario DNL Noise Level (dB)`) %>% select(-id)

all = merge(baseline, alternate, by=c('lat', 'lon'), suffixes=c('', '.alt'))
all$DNL.delta = all$DNL.alt - all$DNL
all_dnl = c(all$DNL, all$DNL.alt)
```

```{r}
# Make a map
nCuts = 9
pal <- colorBin("YlGnBu", all_dnl, bins=nCuts)

# These are rounded up a little to reduce artifacts. 
# The actual values are 0.004171 and 0.005313
lat_diff = 0.00419 / 2
lon_diff = 0.00533 / 2

map = leaflet() %>%
  fitBounds(min(baseline$lon), max(baseline$lat), max(baseline$lon), min(baseline$lat)) %>% 
  addProviderTiles('CartoDB.Positron')

map %>%
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$DNL), group='Baseline',
                   label=as.character(round(all$DNL, 2))) %>% 
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$DNL.alt), group='Alternative',
                   label=as.character(round(all$DNL.alt, 2))) %>% 
  addLayersControl(baseGroups = c('Baseline', 'Alternative'),
                   options=layersControlOptions(collapsed=FALSE)) %>% 
  addLegend(pal = pal, values=all_dnl, title = "DNL")
```

The second map shows the increase or decrease in DNL when changing from baseline to alternative.

```{r}
# We are hacking the color palette to make blue good (-) and red bad (+)
diff_pal <- colorBin("RdYlBu", -all$DNL.delta, bins=nCuts)

# From leaflet::addLegend
cuts = attr(diff_pal, 'colorArgs')$bins
n = length(cuts)
mids = (cuts[-1] + cuts[-n])/2
colors = diff_pal(mids)
labels = labelFormat()(type = "bin", -cuts)

map %>% 
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=diff_pal(-all$DNL.delta), group='Change',
                   label=as.character(round(all$DNL.delta, 2))) %>% 
  addLegend(values=-all$DNL.delta, title = "DNL Change",
            colors=colors, labels=labels)





```

Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson 
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0;margin:0;vertical-align:middle;display:inline" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>
