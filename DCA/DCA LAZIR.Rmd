---
title: "DCA LAZIR"
author: "Kent Johnson"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output:
  html_vignette:
    css: ~/Google Drive/vignette.css
---


__Please be patient!__ The map takes a few moments to load!

Data from FOIA request. Visualization by Kent S Johnson.

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE, fig.width=8, fig.height=8, results='asis',
                      comment=NA, warning=FALSE, message=FALSE)

# Never split tables
pander::panderOptions("table.split.table", Inf)

library(tidyverse)
library(leaflet)
library(readxl)
read_lazir = function(path) {
  read_excel(path) %>% select(-`Noise Result Index`, -`Metric Type`, -`Metric Name`) %>% 
    rename(lat=`Latitude (deg)`, lon=`Longitude (deg)`, 
           elev=`Elevation (ft)`, noise=`Noise Level (dB)`)
}
l5 = read_lazir('LAZIR alternatives/LAZIR5.xlsx')
la = read_lazir('LAZIR alternatives/LAZIRA.xlsx')
lb = read_lazir('LAZIR alternatives/LAZIRB.xlsx')
lc = read_lazir('LAZIR alternatives/LAZIRC.xlsx')

merge_by=c('lat', 'lon', 'elev')

all = merge(l5, la, by=merge_by, suffixes=c('', '.a'))
all = merge(all, lb, by=merge_by, suffixes=c('', '.b'))
all = merge(all, lc, by=merge_by, suffixes=c('', '.c'))
all = all %>% rename(noise.5=noise)
all_noise = c(all$noise.5, all$noise.a, all$noise.b, all$noise.c)
```

```{r}
# Make a map
nCuts = 9
pal <- colorBin("YlGnBu", all_noise, bins=nCuts)

# These are rounded up a little to reduce artifacts. 
# The actual values are 0.004173 and 0.005345
lat_diff = 0.00419 / 2
lon_diff = 0.00535 / 2

map = leaflet() %>%
  fitBounds(min(l5$lon), max(l5$lat), max(l5$lon), min(l5$lat)) %>% 
  addProviderTiles('CartoDB.Positron')

map %>%
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$noise.5), group='LAZIR5',
                   label=as.character(round(all$noise.5, 2))) %>% 
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$noise.a), group='LAZIRA',
                   label=as.character(round(all$noise.a, 2))) %>% 
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$noise.b), group='LAZIRB',
                   label=as.character(round(all$noise.b, 2))) %>% 
  addRectangles(lng1=all$lon-lon_diff, lng2=all$lon+lon_diff,
                lat1=all$lat-lat_diff, lat2=all$lat+lat_diff,
                fillOpacity=0.7, stroke=FALSE, 
                   color=pal(all$noise.c), group='LAZIRC',
                   label=as.character(round(all$noise.c, 2))) %>% 
  addLayersControl(baseGroups = c('LAZIR5', 'LAZIRA', 'LAZIRB', 'LAZIRC'),
                   options=layersControlOptions(collapsed=FALSE)) %>% 
  addLegend(pal = pal, values=all_noise, title = "Noise Level")
```

Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson 
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0;margin:0;vertical-align:middle;display:inline" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>
