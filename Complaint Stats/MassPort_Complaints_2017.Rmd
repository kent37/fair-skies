---
title: "Logan Complaint Stats 2017"
author: "Kent Johnson"
output:
  html_vignette:
    css: ~/Google Drive/vignette.css
---

Logan airport noise complaints for 2017.

Compiled from the most recent  [Massport data](http://www.massport.com/environment/environmental-reporting/noise-abatement/complaints-by-towns/), consolidated by a [Boston West Fair Skies](http://bostonwestfairskies.org) volunteer.

To submit a complaint, [start here](https://secure.symphonycdm.com/publicvue/Frames.asp?sys=BOS&HeaderFrame=HeaderPage.asp&MenuFrame=leftMenu.asp?Context=Complaint&ContentFrame=TitlePage_Complaints.asp).

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=7, fig.height=6, comment=NA, warning=FALSE, message=FALSE)
```

```{r}
# Read and clean the table of noise complaints
# For 2017 complaints in tall-format Excel sheet

library(ggplot2)
library(lubridate)
library(readxl)
library(plyr)
library(scales)
library(tidyr)
library(stringr)

towns = c('Arlington', 'Belmont', 'Cambridge', 'Somerville', 'Medford', 'Watertown', 'Winchester')

read_and_clean = function(path, sheet)
{
  data = read_excel(path, sheet)[,1:4]
  data = data[complete.cases(data),]

  data$Community = str_trim(data$Community)
  data$Community = factor(data$Community)
  data$Month = as.Date(data$Month)
  data
}

colorScale = scale_color_brewer(palette='Set1')
dateScale =  scale_x_date(date_minor_breaks='1 month', date_breaks='1 month', labels = date_format("%b %y"))
legendTL = theme(legend.justification=c(0, 1), legend.position=c(0, 1), 
          legend.background = element_rect(fill=NA), 
          legend.key = element_rect(color=NA, fill=NA))

legendPlain = theme(legend.background = element_rect(fill=NA), 
          legend.key = element_rect(color=NA, fill=NA))

legendType = legendPlain # Don't use legendTL until there are more months

theme_set(theme_linedraw())

monthly = read_and_clean('Complaint stats 2017.xlsx', sheet=1)
monthly_towns = subset(monthly, Community %in% towns)

cum = read_and_clean('Complaint stats 2017.xlsx', sheet=2)
cum_towns = subset(cum, Community %in% towns)

# Complaints by town and month, 2017
p = qplot(Month, Complaints, data=monthly_towns, color=Community, geom='line', size=I(2), main='2017 Monthly Complaints by Town and Date')
p + colorScale + dateScale + legendPlain

# Number of callers by town and month
p = qplot(Month, Callers, data=monthly_towns, color=Community, geom='line', size=I(2), main='2017 Monthly Callers by Town and Date')
p + colorScale + dateScale + legendPlain

# Average number of complaints per caller by town and month
# p = qplot(Month, Complaints/Callers, data=monthly_towns, color=Community, geom='line', size=I(2), main='2017 Monthly Average Number of Complaints per Caller by Town and Date')
# p + colorScale + dateScale + legendPlain

# Cumulative number of complaints
p = qplot(Month, Complaints, data=cum_towns, color=Community, geom='line', size=I(2), main='2017 Cumulative Complaints by Town and Date')
p + colorScale + dateScale + legendPlain

# Cumulative number of callers
p = qplot(Month, Callers, data=cum_towns, color=Community, geom='line', size=I(2), main='2017 Cumulative Callers by Town and Date')
p + colorScale + dateScale + legendPlain
```


The next plots show all towns which have had more than one complaint for the year. 
Towns which had exactly one complaint are
`r paste(as.character(subset(cum, Month==max(cum$Month) & Complaints == 1)$Community), collapse=', ')`.

```{r}
# Cumulative results for all towns
# Complaints
last_month = droplevels(subset(cum, Month==max(cum$Month) & Complaints > 1))
last_month = transform(last_month, Community=reorder(Community, Complaints))

# To color just 'towns' is tricky because ggplot2 is going to re-order the towns
colors = last_month$Community[order(last_month$Community)] %in% towns
colors = ifelse(colors, 'red', 'black')

# Dot colors are a different order !?
dot_colors = ifelse(last_month$Community %in% towns, 'red', 'black')

qplot(Complaints, Community, data=last_month, color=I(dot_colors), size=I(2),
      main='Total number of complaints, by community') + theme(axis.text.y=element_text(size=rel(0.7), color=colors))

# Callers
last_month = transform(last_month, Community=reorder(Community, Callers))
colors = last_month$Community[order(last_month$Community)] %in% towns
colors = ifelse(colors, 'red', 'black')

qplot(Callers, Community, data=last_month, color=I(dot_colors), size=I(2), 
      main='Total number of callers, by community') + theme(axis.text.y=element_text(size=rel(0.7), color=colors))

# Complaints / caller
last_month = transform(last_month, Community=reorder(Community, Complaints/Callers))
colors = last_month$Community[order(last_month$Community)] %in% towns
colors = ifelse(colors, 'red', 'black')

qplot(Complaints/Callers, Community, data=last_month, color=I(dot_colors), size=I(2), 
      main='Average number of complaints per caller, by community') + theme(axis.text.y=element_text(size=rel(0.7), color=colors))
```

<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style="float:right;font-style: italic;">`r Sys.Date()`</span></small>