# Compare Massport-reported YTD complaints with the sum of 
# reported monthly complaints

library(tidyverse)
library(gt)
library(lubridate)
library(readxl)

read_and_clean = function(path, sheet)
{
  data = read_excel(path, sheet)[,1:4]
  data = data[complete.cases(data),]

  data$Community = str_trim(data$Community)
  data$Community = factor(data$Community)
  data$Month = as.Date(data$Month)
  data
}

data_path = here::here('Complaint Stats/Complaint stats 2019.xlsx')
monthly = read_and_clean(data_path, sheet=1) %>% 
  arrange(Community, Month) %>% 
  group_by(Community) %>% 
  mutate(Cumulative=cumsum(Complaints))

cum = read_and_clean(data_path, sheet=2)%>% 
  arrange(Community, Month)

combined = monthly %>% 
  select(Community, Month, Cumulative) %>% 
  left_join(cum %>% 
              select(Community, Month, Complaints)) %>% 
  rename(Reported=Complaints)

towns = c('Arlington', 'Belmont', 'Cambridge', 'Somerville',
          'Malden', 'Medford', 'Watertown', 'Winchester')

reduced = combined %>% 
  filter(Community %in% towns) %>% 
  mutate(Mon=month(Month, label=TRUE)) %>% 
  rename(Actual=Cumulative) %>% 
  ungroup()

spec = reduced %>%
  build_wider_spec(names_from=Mon, values_from=c(Actual, Reported)) %>% 
  mutate(.name=str_replace(.name, '(.*)_(.*)', '\\2 \\1')) %>% 
  arrange(Mon)

# All months
reduced %>% 
  select(-Month) %>% 
  pivot_wider_spec(spec) %>% 
  gt() %>% 
  tab_style(style=cell_text(size='small'), 
    locations=cells_column_labels(columns=everything()))%>% 
  tab_style(style=cell_text(size='small'), 
    locations=cells_data(rows=everything(), 
                         columns=everything()))

# Just October
reduced %>% 
  select(-Month) %>% 
  filter(Mon=='Oct') %>% 
  pivot_wider_spec(spec %>% filter(Mon=='Oct')) %>% 
  mutate(Diff=`Oct Reported`-`Oct Actual`) %>% 
  gt() %>% 
  tab_style(style=cell_text(size='small'), 
    locations=cells_column_labels(columns=everything()))%>% 
  tab_style(style=cell_text(size='small'), 
    locations=cells_data(rows=everything(), 
                         columns=everything()))

