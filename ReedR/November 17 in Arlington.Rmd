---
title: November 17, 2016 in Arlington
author: "Kent Johnson"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output: 
  markdowntemplates::kube:
    self_contained: false
    lib_dir: Media
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE, fig.width=10, fig.height=6, results='asis',
                      comment=NA, warning=FALSE, message=FALSE)

# Never split tables
pander::panderOptions("table.split.table", Inf)

source('ExploreDataKathryn.R')
```

The charts below shows measured sound levels at a home on Scituate St in Arlington, MA on November 17, 2016. The sound levels are annotated with the flight numbers of aircraft passing overhead at the time.

The chart is interactive. Mouse over a red dot or flight number to see the time and approximate noise level. Zoom and pan for more detail.

The chart covers 64 minutes from 3pm to 4:04 pm.

```{r}
gg_nov17
```

### Sources

- Sound levels were measured with a Reed SD-4023 sound meter set to  A-weighting, automatic range selection, slow response, and 1-second sample intervals. The calibration of the instrument was checked before collection with a Reed SC-05 Sound Level Calibrator.
- Sound level measurements by 
<a href="http://www.bostonwestfairskies.org" target="_blank">Boston West Fair Skies</a> volunteers.
- Visualization by Kent S Johnson

<small>Copyright 2016 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>
  