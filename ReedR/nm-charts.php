<?php
	// Noise monitor charting support script
	// Rene Vega, 2015/08/29
	//
	// This is invoked from WEB form using a POST method.
	//
	//
	if ($_SERVER['REQUEST_METHOD'] != 'POST') exit;
	$Debug = false;

	/// <summary>
	///  A running variance and standard deviation class
	///  used to gather real time metrics.
	/// </summary>
	// Borrowed from: http://www.johndcook.com/standard_deviation.html and http://www.johndcook.com/skewness_kurtosis.html
	// In turn borrowed from Knuth, The Art of Computer Programming.
	class Stats
	{
		private $N,$M1,$M1W,$M2,$M3,$M4,$M5,$M6;
		private $X,$XP;
		private	$Mweight,$Outlier;

		function __construct()
		{
			$this->Clear();
		}

		public function Clear()
		{
			$this->N  = 0;
			$this->M1 = 0.0;
			$this->M1W = 0.0;
			$this->M2 = 0.0;
			$this->M3 = 0.0;
			$this->M4 = 0.0;
			$this->M5 = 0.0;
			$this->M6 = 0.0;
			$this->Mweight = 0;
			$this->Outlier = 0;
			$this->X = array();
			$this->XP = 9;
		}

		public function Push($x)
		{
			// See Knuth TAOCP vol 2, 3rd edition, page 232
			$n1 = $this->N;
			$this->N++;

			$delta = $x - $this->M1;
			$delta_n = $delta / $this->N;
			$delta_n2 = $delta_n * $delta_n;
			$term1 = $delta * $delta_n * $n1;

			if ($this->Mweight == 0)
			{
				$this->M1 += $delta_n;
			}
			else
			{
				// Weighted mean
				$this->M1 = $this->M1 * (1.0 - $this->Mweight) + ($x * $this->Mweight);
			}

			$this->M4 += $term1 * $delta_n2 * ($this->N*$this->N - 3*$this->N + 3) + 6 * $delta_n2 * $this->M2 - 4 * $delta_n * $this->M3;
			$this->M3 += $term1 * $delta_n * ($this->N - 2) - 3 * $delta_n * $this->M2;
			$this->M2 += $term1;

			if ($this->N == 1)
			{
				$this->M5 = $x;
				$this->M6 = $x;
			}
			else
			{
				if ($x < $this->M5) $this->M5 = $x;
				if ($x > $this->M6) $this->M6 = $x;
			}
			
			// Triangle smoothing, save the last XP points.
			// Prefill the array at initialization.
			$k = count($this->X);
			for (; $k<=$this->XP; $k++) $this->X[] = $x;
			array_shift($this->X);
		}

		public function Num()
		{
			return $this->N;
		}

		public function Min()
		{
			return $this->M5;
		}

		public function Max()
		{
			return $this->M6;
		}

		public function Mean()
		{
			return $this->M1;
		}

		public	function Mweight($w)
		{
			$this->Mweight = $w;
		}

		public function Variance()
		{
			if ($this->N < 2) return 0.0;
			return $this->M2/($this->N-1.0);
		}

		public function StandardDeviation()
		{
			return sqrt( $this->Variance() );
		}

		public function Skewness()
		{
			if ($this->M2 == 0) return 0;
			$result = sqrt($this->N) * $this->M3/ pow($this->M2, 1.5);
			if (is_nan($result)) $result = 0.0;
			return $result;
		}

		public function Kurtosis()
		{
			if ($this->M2 == 0) return 0;
			return $this->N * $this->M4 / ($this->M2*$this->M2) - 3.0;
		}
		
		public function SetSmoothSize($ss)
		{
			if ($ss%2 == 0) $ss++;
			if ($ss < 3) $ss = 3;
			$this->XP = $ss;
			$this->X = array();
		}
		
		public function Smoothed()
		{
			$n = 0;
			$t = 0;
			$k = floor($this->XP / 2);
			$w = 1;
			$i = 0;
			for ($i=0; $i<$k; $i++)
			{
				$t += $this->X[$i] * $w;
				$t += $this->X[$this->XP - $i - 1] * $w;
				$n += $w * 2;
				$w++;
			}
			$t += $this->X[$i] * $w; // midpoint
			$n += $w;
			return $t / $n;
		}
	}
	
	function IsValidShape($rn0, $rn1, $peak)
	{
		global $data;
		global $Debug;
		
		// This will look for a shape that increases then drops (unimodal);
		
		$sstats = new Stats();
		$sstats->SetSmoothSize(15);
		$dir = 1;	// Initial direction is up
		$tol = 0.10;	// percentage of reverse direction tolerance
		
		for ($rn = $rn0; $rn < $rn1; $rn++)
		{
			$spl1 = $data[$rn]['SPL1'];
			$spl2 = $data[$rn]['SPL2'];
			$dt   = $data[$rn]['Timestamp'];
			
			// Compute the linear average of the accumulated dB readings then convert back to dB
			$spllin = (pow(10, $spl1/10.0) + pow(10, $spl2/10.0))/ 2.0;
			$sstats->Push($spllin);
			$spllin = $sstats->Smoothed();
			$spl = round(10.0 * log10($spllin), 2);
			if ($rn == $rn0)
			{
				$lastspl = $spl;
				$maxspl  = $spl;
				$minspl  = $spl;
				if ($Debug) error_log("$dt Bgn analyis @$rn spl=$spl");
			}
			
			// dir=1 going up, dir=-1 going down.
			if ($dir == 1)
			{
				if ($spl > $maxspl) $maxspl = round($spl, 2);
				if ($spl < ($maxspl * (1.0 - $tol)))
				{
					$dir = -1;
					$minspl = $maxspl;
					if ($Debug) error_log("$dt Peak @$rn spl=$spl");
				}
				$lastspl = $spl;
				
			}
			else
			{
				if ($spl < $minspl) $minspl =round($spl, 2);
				if ($spl > ($minspl * (1.0 + $tol)))
				{
					if ($Debug) error_log("$dt End analyis (is rnd noise) @$rn spl=$spl");
					return false;
				}
				$lastspl = $spl;
			}
		}
		
		if ($dir == -1 || $spl < ($peak * (1.0 - $tol)))
		{
			if ($Debug) error_log("$dt End analyis (is overflight) @$rn spl=$spl min=$minspl max=$maxspl");
			return true;
		}
		
		if ($Debug) error_log("$dt End analyis (is wind noise) @$rn spl=$spl");
		return false;
	}
	
	function SetupLogSource()
	{
		global $sqllink;
		global $source;
		global $sdate, $edate, $timezone;
		global $stime, $etime;
		global $maxts, $tref;
		global $name, $email;
		global $cpostalcode, $ser3;
		global $cthreshday, $cthreshnight, $dttime, $nttime,$cmaskdb;
		
		$_SESSION['c_sdate'] = $sdate;
		$_SESSION['c_edate'] = $edate;
		$_SESSION['c_stime'] = $stime;
		$_SESSION['c_etime'] = $etime;

		//$csource = $_SESSION['c_source'];
		$_SESSION['c_source'] = $source;
		$a = "$source|$cthreshday|$cthreshnight|$dttime|$nttime|$cmaskdb";
		$cset = setcookie('noisedata',$a,time()+ (60*60*24*30), '/', '');
		$csplit = explode(',', $source);
		$_SESSION['c_postalcode']	= trim($csplit[0]);
		$_SESSION['c_city']			= trim($csplit[1]);
		$_SESSION['c_state']		= trim($csplit[2]);

		$cpostalcode = $_SESSION['c_postalcode'];
		$csplit = explode('-',$cpostalcode);
		$cpostalcode = trim($csplit[0]);
		$ser3 = trim($csplit[1]);
		$ccity = $_SESSION['c_city'];
		$cstate = $_SESSION['c_state'];

		$stime = date("H:i:s", strtotime($stime));
		$etime = date("H:i:s", strtotime($etime));
		
		if ($sdate > $edate || (($sdate == $edate) && ($stime > $etime)))
		{
			$_SESSION['status'] = "The starting date/time must be less than the ending date/time:<br />$sdate $stime to $edate $etime";
			return false;
		}
		
		// Sanitize these for the SQL search
		$cpostalcode	= mysql_real_escape_string($cpostalcode,	$sqllink);
		$name			= mysql_real_escape_string($name,			$sqllink);
		$email			= mysql_real_escape_string($email,			$sqllink);

		$query  = "SELECT * FROM DataLogger WHERE PostalCode='$cpostalcode' AND City='$ccity' AND State='$cstate' AND Serial LIKE '%$ser3' ";
		$r1 = mysql_query($query);
		if (mysql_num_rows($r1) != 0)
		{
			$row = mysql_fetch_assoc($r1);
			$tref = $row['tRef'];
			$maxts = $row['hTimestamp'];
			$_SESSION['lat'] = $row['Latitude'];
			$_SESSION['long'] = $row['Longitude'];
			$_SESSION['tz'] = $row['TimeZone'];
			$timezone = $row['TimeZone'];
		}
		else
		{
			$_SESSION['lat'] = '';
			$_SESSION['long'] = '';
			$_SESSION['c_spl'] = array();
			$_SESSION['status'] = "Data source not found: $cpostalcode, $ccity, $cstate";
			return false;
		}
		
		return true;
	}
	
	function jtime2sec($jt)
	{
		sscanf($jt, "%d:%d %s", $hours, $minutes, $ampm);
		if ($hours == 12) $hours = 0;
		$tod = $hours * 3600 + $minutes * 60;
		if ($ampm == "pm") $tod += 43200;
		return $tod;
	}
	
	function GetDist($lat1, $lon1, $lat2, $lon2)
	{
		// Haversine formula
		$R = 6371000; // metres
		$d2r = 0.017453;
		$lat1r = $lat1 * $d2r;
		$lat2r = $lat2 * $d2r;
		$dlat  = $lat1r - $lat2r;
		$dlon  = ($lon2-$lon1) * $d2r;

		$a = sin($dlat/2) * sin($dlat/2) +
			 cos($lat1r) * cos($lat2r) *
			 sin($dlon/2) * sin($dlon/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));

		$d = $R * $c;
		return $d;
	}
	
	function GetNearestFlight($utcstart, $utcend, $lat, $long)
	{
		global	$sqllink;
		global	$timezone;
		
		$flight = 'n/a';
		$altitude = '';
		$lat = intval($lat * 10000);
		$long = intval($long * 10000);
		
		$utcstart	= mysql_real_escape_string($utcstart,	$sqllink);
		$utcend		= mysql_real_escape_string($utcend,		$sqllink);
		$lat		= mysql_real_escape_string($lat,		$sqllink);
		$long		= mysql_real_escape_string($long,		$sqllink);
		
		$ldelta = 10000 * 10.0/60.0; // Miles to latitude/longitude delta (at equator)
		$query  = "SELECT * FROM Flights WHERE Timestamp BETWEEN '$utcstart' AND '$utcend' AND Longitude BETWEEN $long-$ldelta AND $long+$ldelta
				   AND Latitude BETWEEN ($lat-$ldelta) AND ($lat+$ldelta) ";
		$r1 = mysql_query($query);
		$rows = mysql_num_rows($r1);
		// error_log("GetNearestFlight $timezone $utcstart $utcend $lat $long $rows");
		$dist = 9999999999;
		if ($rows != 0)
		{
			$row = mysql_fetch_assoc($r1);
			$flt = $row['Flight'];
			$icao = $row['ICAO'];
			$latf = $row['Latitude'];
			$longf = $row['Longitude'];
			$alt = $row['Altitude'];
			
			if ($flt == '') $flt = $icao;
			
			$d = GetDist($lat, $long, $latf, $longf);
			if ($d < $dist)
			{
				$flight = $flt;
				$altitude = $alt;
				$dist = $d;
			}
		}
		
		return [$flight,$altitude];
	}

	// host, database, and password
	require 'nm-database.inc.php';

	session_start();
	error_reporting(E_ERROR);

	// These are the variables posted by the form
	$submit         = trim($_POST['submit'], '"\' ');
	$sdate			= trim($_POST['sdate'], '"\' ');
	$edate			= trim($_POST['edate'], '"\' ');
	$stime			= trim($_POST['stime'], '"\' ');
	$etime			= trim($_POST['etime'], '"\' ');
	$source			= trim($_POST['source'], '"\' ');
	$showraw		= trim($_POST['showraw'], '"\' ');
	$showsmooth		= trim($_POST['showsmooth'], '"\' ');
	$showmax		= trim($_POST['showmax'], '"\' ');
	$showmean		= trim($_POST['showmean'], '"\' ');
	$showtrig		= trim($_POST['showtrig'], '"\' ');
	$showevnt		= trim($_POST['showevnt'], '"\' ');
	$cthreshday		= trim($_POST['c_thresh'], '"\' ');
	$cthreshnight	= trim($_POST['c_threshnight'], '"\' ');
	$dttime			= trim($_POST['dttime'], '"\' ');
	$nttime			= trim($_POST['nttime'], '"\' ');
	$cmaskdb		= trim($_POST['c_maskdb'], '"\' ');
	if ($cthreshday == '') $cthreshday = 50;
	$_SESSION['c_thresh'] = $cthreshday;
	if ($cthreshnight == '') $cthreshnight = 40;
	$_SESSION['c_threshnight'] = $cthreshnight;
	if ($dttime == '') $dttime = '07:00 am';
	$_SESSION['c_dttime'] = $dttime;
	if ($nttime == '') $nttime = '10:00 pm';
	$_SESSION['c_nttime'] = $nttime;
	if ($cmaskdb == '') $cmaskdb = 6.0;
	$_SESSION['c_maskdb'] = $cmaskdb;

	$refid	= $_SESSION['refid'];
	$name   = $_SESSION['name'];
	$email  = $_SESSION['email'];

	$c_sdate	= $_SESSION['c_sdate'];
	$c_edate	= $_SESSION['c_edate'];
	$c_stime	= $_SESSION['c_stime'];
	$c_etime	= $_SESSION['c_etime'];
	$c_spl		= $_SESSION['c_spl'];
	$c_leq		= $_SESSION['c_leq'];
	$c_sources	= $_SESSION['c_sources'];

	error_reporting(E_WARNING);
	if ($Debug) error_log("nm-charts submit=$submit source=$source sdate:$sdate stime:$stime edate:$edate etime:$etime");

	$_SESSION['c_showraw'] = '';
	$_SESSION['c_showsmooth'] = '';
	$_SESSION['c_showmean'] = '';
	$_SESSION['c_showmax'] = '';
	$_SESSION['c_showtrig'] = '';
	$_SESSION['c_showevnt'] = '';
	if ($showraw != '') $_SESSION['c_showraw'] = 'Checked';
	if ($showsmooth != '') $_SESSION['c_showsmooth'] = 'Checked';
	if ($showmean != '') $_SESSION['c_showmean'] = 'Checked';
	if ($showmax != '') $_SESSION['c_showmax'] = 'Checked';
	if ($showtrig != '') $_SESSION['c_showtrig'] = 'Checked';
	if ($showevnt != '') $_SESSION['c_showevnt'] = 'Checked';

	$status = "OK";
	//header("Cache-Control: max-age=0");

	// Set the return page.
	if (isset($_SESSION['frompage']))  $frompage = $_SESSION['frompage'];
	else
	{
		error_log("frompage missing");
		$frompage = $_SERVER['HTTP_REFERER'];
		header("Location:$frompage");
		$_SESSION['status'] = "Session ended - restarting";
		return;
	}

	$_SESSION['status'] 	= '';

	// Open and select the database
	$sqllink = mysql_connect($hostname,$username,$password);

	if ($sqllink)
	{
		$selected = mysql_select_db($database, $sqllink);
		if (!$selected) $status = "DB error";
	}
	else $status = "DB error";

	// If a database error occurred, stop now.
	if ($status != "OK")
	{
		$_SESSION=array();
		$_SESSION['status'] = "System unavailable, please try again later";
		header("Location:$frompage");
		return;
	}

	// Sanitize these for the SQL search
	$action			= mysql_real_escape_string($action,			$sqllink);
	$name			= mysql_real_escape_string($name,			$sqllink);
	$email			= mysql_real_escape_string($email,			$sqllink);
	$sdate			= mysql_real_escape_string($sdate,			$sqllink);
	$edate			= mysql_real_escape_string($edate,			$sqllink);
	$stime			= mysql_real_escape_string($stime,			$sqllink);
	$etime			= mysql_real_escape_string($etime,			$sqllink);
	$source			= mysql_real_escape_string($source,			$sqllink);

	// ------- Process the actions ----------
	$status = "OK";

	if ($submit == 'GetSources')
	{
		header("Location:$frompage");
		// Order by the date to sort out of system units at the bottom.
		$query  = "SELECT *, DATE(Modified) AS dMod FROM DataLogger WHERE 1 ORDER BY dMod DESC, PostalCode, City, State ";
		$r1 = mysql_query($query);
		if (mysql_num_rows($r1) != 0)
		{
			$c_sources = array();

			while ($row = mysql_fetch_assoc($r1))
			{	// Add to the array
				$ser3 = substr($row['Serial'], -3);
				$c_sources[] = $row['PostalCode'].'-'.$ser3.', '.$row['City'].', '.$row['State'];
			}
			$_SESSION['c_sources'] = $c_sources;

			$c = $_COOKIE['noisedata'];
			$a = explode('|', $c);
			$source = $a[0];
			$cthreshday = $a[1];
			$cthreshnight = $a[2];
			$dttime = $a[3];
			$nttime = $a[4];
			$cmaskdb = $a[5];
			
			if ($source == '') $source = $_SESSION['c_source'];
			if ($source == '') $source = $_SESSION['c_sources'][0];
			$_SESSION['c_source'] = $source;
			$a = "$source|$cthreshday|$cthreshnight|$dttime|$nttime|$cmaskdb";
			$cset = setcookie('noisedata',$a,time()+ (60*60*24*30), '/', '');
			$csplit = explode(',', $source);
			$_SESSION['c_postalcode']	= trim($csplit[0]);
			$_SESSION['c_city']			= trim($csplit[1]);
			$_SESSION['c_state']		= trim($csplit[2]);

			$_SESSION['status'] = "Data logger sources list was updated";
		}
		else $_SESSION['status'] = "No data loggers present";
	}
	
	else if ($submit == 'SetSource')
	{
		header("Location:$frompage");
		$_SESSION['c_source'] = $source;
		$a = "$source|$cthreshday|$cthreshnight|$dttime|$nttime|$cmaskdb";
		$cset = setcookie('noisedata',$a,time()+ (60*60*24*30), '/', '');
		$csplit = explode(',', $source);
		$_SESSION['c_postalcode']	= trim($csplit[0]);
		$_SESSION['c_city']			= trim($csplit[1]);
		$_SESSION['c_state']		= trim($csplit[2]);

		$cpostalcode = $_SESSION['c_postalcode'];
		$csplit = explode('-',$cpostalcode);
		$cpostalcode = trim($csplit[0]);
		$ser3 = trim($csplit[1]);
		$ccity = $_SESSION['c_city'];
		$cstate = $_SESSION['c_state'];
		
		// Sanitize these for the SQL search
		$cpostalcode	= mysql_real_escape_string($cpostalcode,	$sqllink);
		$ccity			= mysql_real_escape_string($ccity,			$sqllink);
		$cstate			= mysql_real_escape_string($cstate,			$sqllink);

		$query  = "SELECT * FROM DataLogger WHERE PostalCode='$cpostalcode' AND City='$ccity' AND State='$cstate' AND Serial LIKE '%$ser3' ";
		$r1 = mysql_query($query);
		if (mysql_num_rows($r1) != 0)
		{
			$row = mysql_fetch_assoc($r1);
			$tref = $row['tRef'];
			$maxts = $row['hTimestamp'];
			$_SESSION['lat'] = $row['Latitude'];
			$_SESSION['long'] = $row['Longitude'];
			$_SESSION['tz'] = $row['TimeZone'];
		}
		else
		{
			$_SESSION['lat'] = '';
			$_SESSION['long'] = '';
			$_SESSION['c_spl'] = array();
			$_SESSION['status'] = "Data source not found: $cpostalcode, $ccity, $cstate";
			header("Location:$frompage");
			mysql_close();
			return;
		}
			
		$_SESSION['c_source'] = $source;
		$a = "$source|$cthreshday|$cthreshnight|$dttime|$nttime|$cmaskdb";
		$cset = setcookie('noisedata',$a,time()+ (60*60*24*30), '/', '');
		$_SESSION['status'] = "New data source is $source";
		//return;
		$submit = 'MakeChart';
	}
	
	if ($submit == 'MakeChart')
	{
		header("Location:$frompage");
		if (!SetupLogSource())
		{
			mysql_close();
			return;
		}

		$ledt = strtotime($maxts);
		$sdt = strtotime("$sdate $stime");
		$edt = strtotime("$edate $etime");
		$range = $edt - $sdt;

		$sdt = date("Y-m-d H:i:s", strtotime("$sdate $stime"));
		$edt = date("Y-m-d H:i:s", strtotime("$edate $etime"));
		//$query  = "SELECT *, db10a*0.1 AS SPL1, db10b*0.1 AS SPL2 FROM Log WHERE tRef=$tref AND Timestamp >= '$sdt' AND Timestamp < '$edt' ORDER BY Timestamp ";
		$query  = "SELECT * FROM Log WHERE tRef=$tref AND Timestamp >= '$sdt' AND Timestamp < '$edt' ORDER BY Timestamp ";

		$r2 = mysql_query($query);
		$rows = 0;
		if ($r2) $rows = mysql_num_rows($r2);
		if ($rows != 0)
		{
			$cminx = 0;
			$cmaxy = 0;
			$skipcnt = 1 + floor(($rows + 1800) / 3600);

			$_SESSION['c_spl'] = array();
			$_SESSION['c_splsmooth'] = array();
			$_SESSION['c_splhist'] = array();
			$_SESSION['c_splmean'] = array();
			$_SESSION['c_splmax'] = array();
			$_SESSION['c_spltrig'] = array();
			$_SESSION['c_splevnt'] = array();
			$_SESSION['c_splthresh'] = array();
			$currcnt = 0;
			$spldtn = 0;
			$splmax = 0;
			$splmean = 0;
			$splstats = new Stats();
			$splsmooth = new Stats();
			$eventstats = new Stats();
						
			// Preflight the data cleaning out spikes, and create
			// the data array.
			$data = array();
			$rcnt = 0;
			while ($row = mysql_fetch_assoc($r2))
			{
				$data[$rcnt] = $row;
				if ($rcnt > 1)
				{
					$splp = $data[$rcnt-2]['SPL1'];
					$splm = $data[$rcnt-1]['SPL1'];
					$spln = $data[$rcnt-0]['SPL1'];
					if ($splm > 95.0 && ($splm - $splp) > 20 && ($splm - $spln) > 20) $data[$rcnt-1]['SPL1'] = $splp;
					$splp = $data[$rcnt-2]['SPL2'];
					$splm = $data[$rcnt-1]['SPL2'];
					$spln = $data[$rcnt-0]['SPL2'];
					if ($splm > 95.0 && ($splm - $splp) > 20 && ($splm - $spln) > 20) $data[$rcnt-1]['SPL2'] = $splp;
				}
				$rcnt++;
			}			

			// Take a small sample to derive a decent starting mean value.
			$cnt = 300;
			if ($rcnt < $cnt) $cnt = $rcnt;
			for ($rn=0; $rn < $cnt; $rn++)
			{
				$spllin = (pow(10, $data[$rn]['SPL1']/10.0) + pow(10, $data[$rn]['SPL2']/10.0)) / 2;
				$spl = 10.0 * log10($spllin);

				if ($splmean == 0) $splmean = $spl;
				$spldiff = $spl - $splmean;
				if ($spl <= $splmean) $splwt = 0.5;
				else $splwt = 0.5 / pow(4, abs($spldiff));
				$splmean = $splmean * (1.0 - $splwt) + ($spl * $splwt);
			}

			$c_row = 0;
			$splaccum = 0;
			$spltrig = $splmean;
			$hitmax = 0;
			$hitaccum = 0;
			$hitcnt = 0;
			
			// Time of day for the two thresholds
			$dttod = jtime2sec($dttime);
			$nttod = jtime2sec($nttime);
			$dt = $data[0]['Timestamp'];
			$dtn= strtotime($dt);
			//error_log("dt=$dt dtn=$dtn  dttime=$dttime dttod=$dttod  nttime=$nttime nttod=$nttod");
					
			for ($rn=0; $rn < $rcnt; $rn++)
			{
				$dt = $data[$rn]['Timestamp'];
				$dtn= strtotime($dt);
				$dtutc = date("Y-m-d H:i:s", $dtn -($timezone*3600));

				// Compute a max spl using the fast measurements.
				$spl1 = $data[$rn]['SPL1'];
				$spl2 = $data[$rn]['SPL2'];
				$splmax = $spl1;
				if ($spl2 > $spl1) $splmax = $spl2;
				
				// Compute the linear average of the accumulated dB readings then convert back to dB
				$spllin = (pow(10, $spl1/10.0) + pow(10, $spl2/10.0));
				$splaccum += $spllin;
				$spl = 10.0 * log10($spllin/2);
				$hitaccum += $spllin;
				$hitcnt += 1;

				// Apply data thinning.
				if (($c_row++ % $skipcnt) == 0)
				{
					$spl = 10.0 * log10($splaccum/$skipcnt/2);
					$splsmooth->Push($spl);
					$splaccum = 0;
					$_SESSION['c_spl'][] = array($dt, $spl);
					$_SESSION['c_splsmooth'][] = array($dt, $splsmooth->Smoothed());
				}

				if ($cminx == 0)
				{
					// initialize first time
					$cminx = $dt;
					$spldtn = $dtn;
					$_SESSION['c_splevnt'][] = array($dt, 0);
				}
				
				if ($cmaxy < $spl) $cmaxy = $spl;
				if ($hitmax < $splmax) $hitmax = round($splmax,1);
				
				$splstats->Push($spl);
				$spls = $splstats->Smoothed();

				// Compute an exponentially weighted mean to reduce the
				// impact of the peak disturbances. The goal is to find the
				// background mean (absent the overflights).
				$spldiff = $spls - $splmean;
				if ($spls <= $splmean) $splwt = 0.08;
				else $splwt = 0.45 / pow(4, abs($spldiff));

				$splmean = $splmean * (1.0 - $splwt) + ($spls * $splwt);

				// Add a hard cutoff on steep rises to exclude the overflight
				// observations from influencing the SD.
				if ($splwt > 0.02)
				{
					// Compute statistics
					$laststd = $splstats->StandardDeviation() * 1.50;
				}
				else
				{
					$splmeanfwd = $splmean + $splstats->StandardDeviation();
					$splmean += 0.025;
					$laststd = 0;
					$splstats->Clear();
				}
				
				$spltrig = $splmean + $laststd;

				// When the interval is over, emit the data points.
				if (($dtn - $spldtn) > 3 * $skipcnt) //seconds
				{
					$spldt = date("Y-m-d H:i:s",$spldtn);
					$_SESSION['c_splmean'][] = array($spldt, $splmean);
					$_SESSION['c_splmax'][]  = array($spldt, $splmax);
					$_SESSION['c_spltrig'][] = array($spldt, $spltrig);
					
					$tod = sscanf($spldt, "%s %d:%d:%d", $d, $h,$m,$s);
					$tod = $h*3600 + $m*60 + $s;
					if ($tod >= $dttod && $tod < $nttod) $cthresh = $cthreshday;
					else								 $cthresh = $cthreshnight;
					$_SESSION['c_splthresh'][] = array($spldt, $cthresh);
					
					$splmax = $spl;
					$spldtn = $dtn;
				}

				// Compute histogram values bucket [20-100] quantized to 3db units
				$bucket = floor($spl + 0.5);
				if (isset($_SESSION['c_splhist'][$bucket])) $_SESSION['c_splhist'][$bucket]++;
				else $_SESSION['c_splhist'][$bucket] = 1;
				
				// First go at detection.
				$hitlow = 0.0;
				$hithigh = 1.0;
				$hitmindur = 29.0;
				$hitmaxdur = 240.0;
				if ($spls > $spltrig) 
				{
					// Beginning of event
					if ($hitdtn == 0)
					{
						$eventstats->Clear();
						$hitaccum = $spllin;
						$hitcnt = 1;
						$hitmax = $spl;
						$hitmeanbgn = $splmean;
						$hitdtn = $dtn;
						$hitdtutc = $dtutc;
						$hitrn  = $rn; // Sample index
						$_SESSION['c_splevnt'][] = array(date("Y-m-d H:i:s",$dtn), $hitlow);
						$_SESSION['c_splevnt'][] = array(date("Y-m-d H:i:s",$dtn), $hithigh, $hitmax, 0);
					}
					
					// Push the events data
					$eventstats->Push($spllin);
				}
				else
				{
					// end of event
					$eventstats->Push($spllin);
					
					if ($hitdtn != 0)
					{
						$hitleq = round(10.0 * log10($hitaccum/$hitcnt/2), 1);
						$hitaccum = 0;
						$hitcnt = 0;
				
						$durhit = $dtn - $hitdtn;
						$hitdtn = 0;
						
						// Compute kertosis
						$hitkurt = round($eventstats->Kurtosis(), 2);
						$hitskew = round($eventstats->Skewness(), 2);
						$hitsd   = round(log10($eventstats->StandardDeviation()), 2);
						$hitbgnd = round($hitmeanbgn, 1);
						
						// toss out of range durations
						if ($durhit >= $hitmindur && $durhit < $hitmaxdur && $hitmax >= $cthresh && $hitmax >= ($hitmeanbgn+$cmaskdb))
						{
							// Perform shape analysis to reject noise.
							$valid = IsValidShape($hitrn, $rn, $hitmax);
						}
						else $valid = false;
						
						if ($valid)
						{
							$res = GetNearestFlight($hitdtutc, $dtutc, $_SESSION['lat'], $_SESSION['long']);
							$hitflight = $res[0];
							$hitalt = $res[1];
							$_SESSION['c_splevnt'][] = array(date("Y-m-d H:i:s",$dtn), $hithigh, $hitmax, $hitleq, $hitkurt, $hitskew, $hitsd, $hitbgnd, $hitflight, $hitalt);
						}
						else
						{
							array_pop($_SESSION['c_splevnt']);
							array_pop($_SESSION['c_splevnt']);
							//$splmean = $splmeanfwd;
						}
						
						$_SESSION['c_splevnt'][] = array(date("Y-m-d H:i:s",$dtn), $hitlow);
						$hitmax = 0;
						$splstats->Clear();
					}
				}
			}
			
			// Final observation
			$_SESSION['c_splminx'] = $cminx;
			$_SESSION['c_splmaxx'] = $edt;
			$_SESSION['c_splrangex'] = $range;
			$_SESSION['c_splmaxy'] = $cmaxy + 0.5;
			//$_SESSION['c_splmean'][] = array(date("Y-m-d H:i:s",$dtn), 10.0 * log10($splmean));
			$_SESSION['c_splmean'][] = array(date("Y-m-d H:i:s",$dtn), $splmean);
			$_SESSION['c_splmax'][] = array(date("Y-m-d H:i:s",$dtn), $splmax);
			$_SESSION['c_spltrig'][] = array(date("Y-m-d H:i:s",$dtn), $splmean + $splstats->StandardDeviation() * 1.0);
			
			$tod = sscanf($dt, "%s %d:%d:%d", $d, $h,$m,$s);
			$tod = $h*3600 + $m*60 + $s;
			if ($tod >= $dttod && $tod < $nttod) $cthresh = $cthreshday;
			else								 $cthresh = $cthreshnight;
			$_SESSION['c_splthresh'][] = array($dt, $cthresh);

			if ($sdt <= $ledt)
			{
				$_SESSION['status'] = "Chart of $source from $sdate $stime to $edate $etime Leq($skipcnt sec)";
			}
			else
			{	// Have to adjust the date time to show what there is available.
				$edt = $ledt;
				$sdt = $edt - $range;
				$sdate = date("Y-m-d", $sdt);
				$stime = date("H:i:s", $sdt);
				$edate = date("Y-m-d", $edt);
				$etime = date("H:i:s", $edt);
				$_SESSION['c_sdate'] = $sdate;
				$_SESSION['c_stime'] = $stime;
				$_SESSION['c_edate'] = $edate;
				$_SESSION['c_etime'] = $etime;
				$_SESSION['status'] = "Chart of $source <strong>adjusted: $sdate $stime to $edate $etime</strong>  Leq($skipcnt sec)";
			}
			
			// Build the threshold array
			//$_SESSION['c_splthresh'] = array(array($cminx,$cthreshday), array($edt, $cthreshday));
		}
		else
		{
			$_SESSION['c_spl'] = array();
			$_SESSION['status'] = $_SESSION['status'] . "<br />No data recorded from <strong>$source</strong> for the indicated date/time range";
		}
	}
	
	else if ($submit == 'Download')
	{
		$format = 0;
		/**/ if ($format == 0)	$eol = "\r\n";	// Windows
		else if ($format == 1)	$eol = "\r";	// Classic Mac
		else if ($format == 2)	$eol = "\n";	// unix or OS X
		
		if (!SetupLogSource())
		{
			header("Location:$frompage");
			mysql_close();
			return;
		}
		
		$sdt = date("Y-m-d H:i:s", strtotime("$sdate $stime"));
		$edt = date("Y-m-d H:i:s", strtotime("$edate $etime"));
		//$query  = "SELECT *, db10a*0.1 AS SPL1, db10b*0.1 AS SPL2 FROM Log WHERE tRef=$tref AND Timestamp >= '$sdt' AND Timestamp < '$edt' ORDER BY Timestamp ";
		$query  = "SELECT * FROM Log WHERE tRef=$tref AND Timestamp >= '$sdt' AND Timestamp < '$edt' ORDER BY Timestamp ";
		$r2 = mysql_query($query);
		$rows = 0;
		if ($r2) $rows = mysql_num_rows($r2);
		if ($rows != 0)
		{
			$ymdhms = date("Y-m-d_H:i:s");
			header("Pragma: public");
			header("Expires: -1");
			header("Cache-Control: public, must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment;filename=$cpostalcode-$ser3-$ymdhms.csv");

			echo("Date,Leq,Max\n");
			while ($row = mysql_fetch_assoc($r2))
			{
				$dt = $row['Timestamp'];
				$dtn= strtotime($dt);
				$spl1 = $row['SPL1'];
				$spl2 = $row['SPL2'];

				// compute the linear average of the two dB readings then convert back to dB
				$spllin = (pow(10, $spl1/10.0) + pow(10, $spl2/10.0)) / 2;
				$spl = round(10.0 * log10($spllin), 2);
				if ($spl1 > $spl2) $max = $spl1;
				else			   $max = $spl2;
				echo("$dt,$spl,$max$eol");
			}
			flush();
			header("Location:$frompage");
			$msg = "Downloaded $rows datapoints of $source from $sdate $stime to $edate $etime";
			$_SESSION['status'] = $msg;
			error_log($msg . " refid=$refid");
		}
		else
		{
			header("Location:$frompage");
			$_SESSION['status'] = $_SESSION['status'] . "<br />No data recorded from <strong>$source</strong> for the indicated date/time range";
		}
	}
	
	else
	{
		header("Location:$frompage");
	}

	mysql_close();
?>