(defun tcr-sel ()
  "By Thomas C. Rindfleisch, Stanford University, July 20, 2015."
"Scan recorded sound level data (linear intensities, not dBAs)
for aircraft overflight peaks: establish a running median
background level, a threshold detection level based on the
standard deviation about the background median, and ensure that
peaks are neither too short or too long to be plausible aircraft
overflight events. Then for detected peaks, mark their location,
record the peak maximum intensity value, the integrated sound
energy under each peak, and the width of the peak in a secondary
buffer. Also record the background median intensity values and
the background detection thresholds used to identify peaks
throughout the record."
(interactive)
(let (bfr-beg bfr-end beg end tmp locptr stblock endblock
	      stline endline nbackstep
	      (debugsw 1)
	      (avewgt 0.98)
	      cmpavewgt
	      (boxFilterWidth 7)
;;	      (boxFilterWidth 5)
	      hpreFilterW
	      (Nparabfit 9)
	      hNparabfit
	      (CurveAveRng 5)		;Range to average parab curvature
	      hCurveAveRng
	      (ParabSlopeThresh 450.0)	;Min ave pk slope in parabola fit
;;	      (ParabSlopeThresh 700.0)	;Min ave pk slope in parabola fit
;;	      (ParabSlopeThresh 1000.0)	;Min ave pk slope in parabola fit
	      (ParabCurveThresh 175.0)	;Min ave pk curvature in parabola fit
;;	      (ParabCurveThresh 200.0)	;Min ave pk curvature in parabola fit
;;	      (ParabCurveMin 300.0)	;Min max pk curvature in parabola fit
	      (ParabCurveMin 200.0)	;Min max pk curvature in parabola fit
	      (ParabCurveMax 2000)	;Max max pd curv in parab fit
	      (ParabRefAmp 50000)	;Ref net pk amplitude for thresholds
	      (MinPKmax-EdgeDist 3)	;Min dist btw pk max & complex edges
;;	      (MinPKmax-EdgeDist 5)	;Min dist btw pk max & complex edges
	      (minNslopePklet 3)	;Min # of up/down slope pts in pklet
;;	      (minPkletSep 20)
	      (minPkletSep 15)
;;	      (MultipletThresh 0.6)	;Min inter-pk dip ht for multiplet
	      (MultipletThresh 0.8)	;Min inter-pk dip ht for multiplet
;;	      (MultipletFlank 2.5)	;Flank of peaklet, factor x bkgthresh
	      (MultipletFlank 1.1)	;Flank of peaklet, factor x bkgthresh
	      (MinPKslope 450)		;To make sure peaklet stands out
;;	      (MinPKslope 750)		;To make sure peaklet stands out
;;	      (MinPKslope 1000)		;To make sure peaklet stands out
	      (minPKletDuration 15)	;Min subpeak width
;;	      (minDuration 15) 
;;	      (minDuration 20) 
;;	      (minDuration 25)	       ;Setting <3/14/16
	      (minDuration 35)
;;	      (maxDuration 200)
	      (maxDuration 240)
	      (megaPKfactor 1.0)
;;	      (megaPKfactor 1.3)
	      (SampleInterval 2)
;;	      (TallPkLim 80)		;Max aircraft pk amp (dBA)
	      (TallPkLim 90)		;Max aircraft pk amp (dBA)
	      (nTall 0)			;Number of too tall peaks
	      (nDiscrd 0)		;Number of pks discarded (~parabola)
;;	      (AmplitudePass 320000)	;About 55 DbA (not used currently)
	      (OPkThreshold 100000.0)	;Threshold for outputting BIG peak repeat frequencies (50 dBA)
	      (nfreqave 5)		;# of pks to average aircraft overflight freq estimate
	      hnfreqave
	      (NoisyPkUnitInterval 15) 	;Base time interval for noisy pk criteria
	      (MaxChangesPerPk 4.2)	;Max extrema per minimum peak duration
;;	      (MaxChangesPerPk 3.9)	;Max extrema per minimum peak duration
;;	      (MaxChangesPerPk 3.0)	;Max extrema per minimum peak duration
;;	      (MaxChangesTot 15)	;Max # extrema total per pk complex
	      (MinChangeSep 1.3)	;Minimum ave direction chg separation
;;	      (MinChangeSep 1.6)	;Minimum ave direction chg separation
;;	      (MinChangeSep 1.75)	;Minimum ave direction chg separation
;;	      (MaxPklets 3)		;Max # of satellite peaklets in group
	      (MaxPklets 5)		;Max # of satellite peaklets in group
	      (HistogramMedian 0.4)	;Params for median reset
	      (HistogramThresh 0.45)
;;	      (HistogramMedian 0.5)	;Params for median reset
;;	      (HistogramThresh 0.6)
	      fwgt
	      (medianamp 75000) 
	      lmedianamp
	      (errbar 0) 
	      runamp 
	      (runvar 0) 
	      (runnorm 1.0) 
;;	      (sdevthrshfact 3.0)
;;	      (sdevthrshfact 3.5)
	      (sdevthrshfact 2.75) ;# std devs above bkgnd for threshold
;;	      (BkgndAdjFactor 0.25)
	      (ithrshoffset 1.35)	;Factor (x bkgnd) for min threshold
;;	      (ithrshoffset 1.4)	;Factor (x bkgnd) for min threshold
	      (BigPKfactor 2.5)
;;	      (BigPKfactor 3.0)
	      (linecnt 0) 
	      (Data-bfr) 
	      (Data-Debug-bfr "Debug")
	      (PK-bfr "Pk-Markup") 
	      (BKG-bfr "Bkgnd-Markup") 
	      totPKenergy 
	      PKlength 
	      PKtime 
	      PK1sthalf 
	      PK2ndhalf 
	      energyvalue 
	      benergyvalue 
	      PKthresh 
	      iPKthresh 
;;	      (dfltThrshMeanRatio 1.1)
	      (nPK 0) 
	      (nShort 0) 
	      (nLong 0)
	      (nThAdj 0)
	      (nInThAdj 0)		;Look out for loops
	      (nInThAdjMax 4)		;Maximum tries at thresh adj
	      (nMult 0)
	      (nMarginalPks 0)
	      (nAbortedPks 0)
	      (nPairPrune 0)		;Counter for pruning close pairs of pks
;;	      (minPairTimeDiff 60.0)	;Min pair distance
	      (minPairTimeDiff 40.0)	;Min pair distance
	      minPairLocDiff		;Same but measured in ticks
	      (nBurstPrune 0)		;Counter for pruning big burst freqs
	      (maxBurstFreq 75.0)	;Max burst freq
	      (bkgrndsw 1) 
	      (ampwidthscale 3.0) 
	      maxPKenergy
	      bmaxPKenergy
	      PKthreshenergy
	      PKbaseenergy
	      PKboxenergy	  ;Pk box area w/o detection threshold
	      PKnetenergy	  ;Net pk area w/o detection threshold
	      (expctdPKareafraction 0.75)
;;	      (expctdPKareafraction 0.6)
;;	      (excessPKareafraction 0.8) ;Was in effect to 2/10/16
	      (excessPKareafraction 0.62) ;Was 0.7 (2/10/16)
	      aRawData	      ;box-filtered raw data
	      aParabFit-a     ;parabolic fit coeff a (peak amplitude)
	      aParabFit-b     ;parabolic fit coeff b (1st derivative)
	      aParabFit-c     ;parabolic coeff c (peak sharpness, up/down)
	      aParabFit-A     ;Normalized A param from form y = A + C*[x-B]^2
	      aParabFit-B     ;Normalized B param from form y = A + C*[x-B]^2
	      aParabFit-C     ;Normalized C param from form y = A + C*[x-B]^2
	      aRMSParabFitErr ;RMS error in parab fit at pk max
	      aAveUpslope     ;Normalized ave upslope in detected peak
	      aAveDownslope   ;Normalized ave downslope in detected peak
	      aAveCurvature   ;Normalized ave curvature in detected peak
	      aMaxCurvature   ;Normalized max curvature in detected peak
	      aZeroCrossings  ;Place for reduced extrema info in multiplet anal
	      bZeroCrossings  ;Place for raw extrema info in multiplet anal
	      bnumDirChanges  ;# of up/down changes in peak complex
	      baveAmpChange   ;Average amp change between direction changes
	      baveAmpChgDist  ;Average distance between dir changes
	      aPKSEL	      ;working pass 1-4 SELs
	      aPKMax	      ;working pass 1-4 peak maxima
	      aPKNetHt	      ;working pass 1-4 net peak heights above background
	      aPKWidth	      ;working pass 1-4 peak widths (sec)
	      aPKMarker	      ;working pass 1-4 peak markers
	      aBkgMean	      ;working pass 1-4 background mean
	      aBkgThresh      ;working pass 1-4 background threshold
	      bRawData	      ;original raw data
	      bPKSEL	      ;saved pass 1 SELs
	      bPKMax	      ;saved pass 1 peak maxima
	      bPKNetHt	      ;saved pass 1 net peak heights above background
	      bPKWidth	      ;saved pass 1 peak widths (sec)
	      bPKMarker	      ;saved pass 1 peak markers
	      bBkgMean	      ;saved pass 1 background mean
	      bBkgThresh      ;saved pass 1 background threshold
	      (aVectorSize 45000)
	      (LineTotal 0)
	      hLineTotal
	      phLineTotal
	      (idx -1)
	      idx1
	      idx2)
  (save-excursion
    (save-restriction

      ;; Get the user's estimate of the starting background
      (setq medianamp (string-to-number
		       (read-from-minibuffer "Initial background intensity estimate: ")))

      ;; Save full buffer end points
      (setq bfr-beg (point-min)
	    bfr-end (point-max))

      ;; Decide if region is specified and if so, limit action to region
      (if (not mark-active) (setq beg (point-min)
				  end (point-max))
	(setq beg (region-beginning)
	      end (region-end)))
      (goto-char beg)			;Make sure we start at the beginning
      (narrow-to-region beg end)

      ;; Other set up actions
      (setq Data-bfr (buffer-name)) ;Remember starting (data) buffer name and name other output buffers
      (setq Data-Debug-bfr (concat Data-bfr "_" Data-Debug-bfr)
	    PK-bfr (concat Data-bfr "_" PK-bfr)
	    BKG-bfr (concat Data-bfr "_" BKG-bfr))

      ;; Make sure buffers exist and are empty
      (if (= debugsw 0) nil
	(switch-to-buffer Data-Debug-bfr)
	(delete-region (point-min) (point-max)))
      (switch-to-buffer PK-bfr)
      (delete-region (point-min) (point-max))
      (switch-to-buffer Data-bfr)     ;And then back to the input data

      (setq aRawData (make-vector aVectorSize 0.0) ;Set up primary data arrays
	    aParabFit-a (make-vector aVectorSize 0.0)
	    aParabFit-b (make-vector aVectorSize 0.0)
	    aParabFit-c (make-vector aVectorSize 0.0)
	    aParabFit-A (make-vector aVectorSize 0.0)
	    aParabFit-B (make-vector aVectorSize 0.0)
	    aParabFit-C (make-vector aVectorSize 0.0)
	    aRMSParabFitErr (make-vector aVectorSize 0.0)
	    aAveUpslope (make-vector aVectorSize 0.0)
	    aAveDownslope (make-vector aVectorSize 0.0)
	    aAveCurvature (make-vector aVectorSize 0.0)
	    aMaxCurvature (make-vector aVectorSize 0.0)
	    aZeroCrossings (make-vector aVectorSize 0.0)
	    bZeroCrossings (make-vector aVectorSize 0.0)
	    bnumDirChanges (make-vector aVectorSize 0)
	    baveAmpChange (make-vector aVectorSize 0.0)
	    baveAmpChgDist (make-vector aVectorSize 0.0)
	    aPKSEL (make-vector aVectorSize 0.0)
	    aPKMax (make-vector aVectorSize 0.0)
	    aPKNetHt (make-vector aVectorSize 0.0)
	    aPKWidth (make-vector aVectorSize 0)
	    aPKMarker (make-vector aVectorSize 0)
	    aBkgMean (make-vector aVectorSize 0.0)
	    aBkgThresh (make-vector aVectorSize 0.0)
	    bRawData (make-vector aVectorSize 0.0) ;Set up 2ndary data arrays
	    bPKSEL (make-vector aVectorSize 0.0)
	    bPKMax (make-vector aVectorSize 0.0)
	    bPKNetHt (make-vector aVectorSize 0.0)
	    bPKWidth (make-vector aVectorSize 0)
	    bPKMarker (make-vector aVectorSize 0)
	    bBkgMean (make-vector aVectorSize 0.0)
	    bBkgThresh (make-vector aVectorSize 0.0))

      ;; Now begin the work by reading in the data array
	    (setq stblock beg
		  idx -1
		  energyvalue 1.0)

      ;; Read the data set into the raw data array. Note floating
      ;; point representation is needed for the RawData since it has
      ;; such a large dynamic range.
      (while (and (< stblock end)		;Read to end of buffer
		  (> energyvalue 0.0))
	(setq energyvalue 
	      (float (string-to-number 
		      (buffer-substring stblock 
					(progn (end-of-line) (point))))))
	(if (<= energyvalue 0.0) nil
	  (aset aRawData (setq idx (1+ idx)) energyvalue)
	  (aset bRawData idx energyvalue) ;Copy to 2ndary buffer
	  (setq stblock (progn (forward-line) (point)))))
      
      (setq LineTotal (1+ idx))	       ;Note total line count in input

      ;; Pre-filter the raw data to help smooth analysis of noise
      (tcr-do-box-filter bRawData LineTotal boxFilterWidth)

      (tcr-write-debugging-info-2 (concat "Pass 0.a: Initial raw data & box-LPF on raw data ("
					(format "%d" boxFilterWidth)
					" pt box width)
")
				  "%d\t%d"
				  LineTotal bRawData aRawData)

      ;; Least squares fit a parabola point by point along the entire
      ;; data record.

      (tcr-do-parabolic-fit aRawData LineTotal Nparabfit)

      ;; Then copy the "a" parameter (local extremum of each parabola)
      ;; to the aRawData array to work on this more highly filtered
      ;; form of the data.
;;      (setq hNparabfit (floor Nparabfit 2)
;;	    idx hNparabfit
;;	    phLineTotal (- LineTotal hNparabfit))
;;    (while (< idx phLineTotal)
;;	(aset aRawData idx (aref aParabFit-a idx))
;;	(setq idx (1+ idx)))

      ;; Now write the header and the point by point parabola fit
      ;; parameters to the debugging buffer.
      (tcr-write-debugging-info-3 (concat "Pass 0.b: Parabolic fit of box-LPF Raw Data ("
					  (format "%d" Nparabfit)
					  " pt parabola fit)
")
				  "%.1f\t%.1f\t%.1f"
				  LineTotal aParabFit-a aParabFit-b aParabFit-c)


      ;; Now scan forward through all data pts and process background
      ;; and peaks to produce a new record of raw data plus peaks in
      ;; buffer PK-Markup, and of the smoothed background in buffer
      ;; BKG-Markup

      (setq idx 0			;Init variables
	    linecnt 0
	    stblock idx
	    endblock idx
	    stline 0
	    endline stline
	    cmpavewgt (- 1.0 avewgt)
	    PKthresh (* ithrshoffset medianamp) ;init bkg detector threshold
	    iPKthresh PKthresh
	    runamp medianamp
	    TallPkLim (float (expt 10 (/ TallPkLim 10.0))))

      (while (< endblock LineTotal)
	;; Start search for next block, peak or not. It may be a peak
	;; if its starting amplitude >= PKthresh, its duration
	;; > minDuration and < maxDuration
	(if (< (aref aRawData endblock) PKthresh)
	    (setq endblock (tcr-dobkgndregion)) ;Start background block
	  (setq endblock (tcr-dopossiblePK))) ;Else start possible peak

	;; Now get ready for the next block
	(setq stblock endblock	;update start of next block
	      stline endblock
	      endline stline))

      ;; Here we're done with the pass through the data in the forward
      ;; direction. Write the intermediate results into the output
      ;; buffers (unless not debugging) and copy the results to the
      ;; secondary arrays.
      (if (/= debugsw 0)
	  (tcr-write-debugging-buffer-peaks "Pass 1: forward time analysis"))

      ;; And add a message with stats in the minibuffer
      (message "Pass 1: %d lines read; %d pks found; %d short pks; %d long pks; %d tall pks; %d unparabolic; %d thresh adjs"
	       LineTotal nPK nShort nLong nTall nDiscrd nThAdj)

      ;; Save the pass 1 data in the bxxx arrays and re-zero the axxx
      ;; arrays
      (setq idx 0)
      (while (<= idx LineTotal)
	(aset bPKSEL idx (aref aPKSEL idx))
	(aset bPKMax idx (aref aPKMax idx))
	(aset bPKNetHt idx (aref aPKNetHt idx))
	(aset bPKWidth idx (aref aPKWidth idx))
	(aset bPKMarker idx (aref aPKMarker idx))
	(aset bBkgMean idx (aref aBkgMean idx))
	(aset bBkgThresh idx (aref aBkgThresh idx))

	(aset aPKSEL idx 0.0)
	(aset aPKMax idx 0.0)
	(aset aPKNetHt idx 0.0)
	(aset aPKWidth idx 0.0)
	(aset aPKMarker idx 0.0)
	(aset aBkgMean idx 0.0)
	(aset aBkgThresh idx 0.0)
	(aset aParabFit-A idx 0.0)
	(aset aParabFit-B idx 0.0)
	(aset aParabFit-C idx 0.0)
	(aset aRMSParabFitErr idx 0.0)
	(aset aAveUpslope idx 0.0)
	(aset aAveDownslope idx 0.0)
	(aset aAveCurvature idx 0.0)
	(aset aMaxCurvature idx 0.0)

	(setq idx (1+ idx)))

      ;; Pass 2: Now reverse the order of the raw data buffer and the
      ;; corresponding parabola fit data so we can scan for background
      ;; and peaks in the reverse direction
      (setq idx 0			;Start at the beginning
	    hLineTotal (floor LineTotal 2))
      (while (< idx hLineTotal)
	;; Swap values for aRawData
	(setq tmp (aref aRawData idx))
	(aset aRawData idx (aref aRawData (- LineTotal idx 1)))
	(aset aRawData (- LineTotal idx 1) tmp)

	;; Swap values for bRawData
	(setq tmp (aref bRawData idx))
	(aset bRawData idx (aref bRawData (- LineTotal idx 1)))
	(aset bRawData (- LineTotal idx 1) tmp)

	;; Swap values for aParabFit-a
	(setq tmp (aref aParabFit-a idx))
	(aset aParabFit-a idx (aref aParabFit-a (- LineTotal idx 1)))
	(aset aParabFit-a (- LineTotal idx 1) tmp)

	;; Swap values for aParabFit-b
	(setq tmp (aref aParabFit-b idx))
	(aset aParabFit-b idx (aref aParabFit-b (- LineTotal idx 1)))
	(aset aParabFit-b (- LineTotal idx 1) tmp)

	;; Swap values for aParabFit-c
	(setq tmp (aref aParabFit-c idx))
	(aset aParabFit-c idx (aref aParabFit-c (- LineTotal idx 1)))
	(aset aParabFit-c (- LineTotal idx 1) tmp)

	(setq idx (1+ idx)))		;End while

      ;; Pass 2: Set up to scan for background and peaks in the
      ;; reversed data
      (setq idx 0		  ;init array indices
	    linecnt 0
	    stblock 0
	    endblock 0
	    stline 0
	    endline 0
	    nPK 0
	    nShort 0
	    nLong 0
	    nTall 0
	    nDiscrd 0
	    nThAdj 0
	    nInThAdj 0
	    medianamp (aref aBkgMean (1- LineTotal)) ;Start with last P1 mean
	    PKthresh (aref aBkgThresh (1- LineTotal)) ;and threshold
	    runamp medianamp)

      ;; Now scan through all reversed data pts and process
      ;; background and peaks to produce a new record of raw data
      ;; plus peaks in buffer PK-Markup, and of the smoothed
      ;; background in buffer BKG-Markup

      (while (< endblock LineTotal)
	;; Start search for next block, peak or not. It may be a peak
	;; if its starting amplitude >= PKthresh, its duration
	;; > minDuration and < maxDuration
	(if (< (aref aRawData endblock) PKthresh)
	    (setq endblock (tcr-dobkgndregion)) ;Start background block
	  (setq endblock (tcr-dopossiblePK))) ;Else start possible peak

	;; Now get ready for the next block
	(setq stblock endblock	;update start of next block
	      stline endblock
	      endline stline))

      ;; Re-reverse the order of the backward arrays to compare with
      ;; the pass 1 results
      (setq idx 0			;Start at the beginning
	    idx2 (1- LineTotal))
      (while (< idx hLineTotal)
	(setq tmp (aref aRawData idx)) ;Swap values with end of buffer
	(aset aRawData idx (aref aRawData idx2))
	(aset aRawData idx2 tmp)
	(setq tmp (aref bRawData idx)) ;Swap values with end of buffer
	(aset bRawData idx (aref bRawData idx2))
	(aset bRawData idx2 tmp)
	(setq tmp (aref aPKSEL idx)) ;Swap values with end of buffer
	(aset aPKSEL idx (aref aPKSEL idx2))
	(aset aPKSEL idx2 tmp)
	(setq tmp (aref aPKMax idx)) ;Swap values with end of buffer
	(aset aPKMax idx (aref aPKMax idx2))
	(aset aPKMax idx2 tmp)
	(setq tmp (aref aPKNetHt idx)) ;Swap values with end of buffer
	(aset aPKNetHt idx (aref aPKNetHt idx2))
	(aset aPKNetHt idx2 tmp)
	(setq tmp (aref aPKWidth idx)) ;Swap values with end of buffer
	(aset aPKWidth idx (aref aPKWidth idx2))
	(aset aPKWidth idx2 tmp)
	(setq tmp (aref aPKMarker idx)) ;Swap values with end of buffer
	(aset aPKMarker idx (aref aPKMarker idx2))
	(aset aPKMarker idx2 tmp)
	(setq tmp (aref aBkgMean idx)) ;Swap values with end of buffer
	(aset aBkgMean idx (aref aBkgMean idx2))
	(aset aBkgMean idx2 tmp)
	(setq tmp (aref aBkgThresh idx)) ;Swap values with end of buffer
	(aset aBkgThresh idx (aref aBkgThresh idx2))
	(aset aBkgThresh idx2 tmp)
	(setq tmp (aref aParabFit-A idx))
	(aset aParabFit-A idx (aref aParabFit-A idx2))
	(aset aParabFit-A idx2 tmp)
	(setq tmp (aref aParabFit-B idx))
	(aset aParabFit-B idx (aref aParabFit-B idx2))
	(aset aParabFit-B idx2 tmp)
	(setq tmp (aref aParabFit-C idx))
	(aset aParabFit-C idx (aref aParabFit-C idx2))
	(aset aParabFit-C idx2 tmp)
	(setq tmp (aref aRMSParabFitErr idx))
	(aset aRMSParabFitErr idx (aref aRMSParabFitErr idx2))
	(aset aRMSParabFitErr idx2 tmp)
	(setq tmp (aref aAveUpslope idx))
	(aset aAveUpslope idx (aref aAveUpslope idx2))
	(aset aAveUpslope idx2 tmp)
	(setq tmp (aref aAveDownslope idx))
	(aset aAveDownslope idx (aref aAveDownslope idx2))
	(aset aAveDownslope idx2 tmp)
	(setq tmp (aref aAveCurvature idx))
	(aset aAveCurvature idx (aref aAveCurvature idx2))
	(aset aAveCurvature idx2 tmp)
	(setq tmp (aref aMaxCurvature idx))
	(aset aMaxCurvature idx (aref aMaxCurvature idx2))
	(aset aMaxCurvature idx2 tmp)

	;; And swap values back for aParabFit-x arrays
	(setq tmp (aref aParabFit-a idx))
	(aset aParabFit-a idx (aref aParabFit-a idx2))
	(aset aParabFit-a idx2 tmp)
	(setq tmp (aref aParabFit-b idx))
	(aset aParabFit-b idx (aref aParabFit-b idx2))
	(aset aParabFit-b idx2 tmp)
	(setq tmp (aref aParabFit-c idx))
	(aset aParabFit-c idx (aref aParabFit-c idx2))
	(aset aParabFit-c idx2 tmp)

	(setq idx (1+ idx)
	      idx2 (1- idx2)))		;End while

      ;; Write intermediate results to the peak buffer unless not debugging
      (if (/= debugsw 0)
	  (tcr-write-debugging-buffer-peaks "Pass 2: reverse time analysis"))

      ;; And add a message with stats in the minibuffer
      (message "Pass 2: %d lines read; %d pks found; %d short pks; %d long pks; %d tall pks; %d unparabolic; %d thresh adjs"
	       LineTotal nPK nShort nLong nTall nDiscrd nThAdj)

      ;; Then for pass 3, set the background mean and threshold values
      ;; to the minima of those from passes 1 and 2, and re-zero the
      ;; peak data arrays.
      (setq idx 0)
      (while (< idx LineTotal)
	(aset aBkgMean idx (min (aref aBkgMean idx)
				(aref bBkgMean idx)))
	(aset aBkgThresh idx (min (aref aBkgThresh idx)
				  (aref bBkgThresh idx)))

	(aset aPKSEL idx 0.0)
	(aset aPKMax idx 0.0)
	(aset aPKNetHt idx 0.0)
	(aset aPKWidth idx 0.0)
	(aset aPKMarker idx 0.0)
	(aset aParabFit-A idx 0.0)
	(aset aParabFit-B idx 0.0)
	(aset aParabFit-C idx 0.0)
	(aset aRMSParabFitErr idx 0.0)
	(aset aAveUpslope idx 0.0)
	(aset aAveDownslope idx 0.0)
	(aset aAveCurvature idx 0.0)
	(aset aMaxCurvature idx 0.0)

	(setq idx (1+ idx)))

      ;; Pass 3.a: Scan for background and peaks in the
      ;; averaged background data w/o computing new bkg stats.
      (setq idx 0		  ;init array indices
	    linecnt 0
	    bkgrndsw -1	  ;turn off bkg stats
	    stblock 0
	    endblock 0
	    stline 0
	    endline 0
	    nPK 0
	    nShort 0
	    nLong 0
	    nTall 0
	    nDiscrd 0
	    nThAdj 0
	    nInThAdj 0
	    medianamp (aref aBkgMean idx)  ;init bkg median value
	    PKthresh (aref aBkgThresh idx) ;init bkg detector
	    runamp medianamp)

      ;; Now scan through the raw data using the combined background
      ;; mean and thresholds to produce a new record of raw data
      ;; plus peaks in buffer PK-Markup

      ;; Start search for next block, peak or not. It may be a peak
      ;; if its starting amplitude >= PKthresh, its duration
      ;; > minDuration and < maxDuration
      (while (< endblock LineTotal)
	;; Here we set the background mean and threshold to the values
	;; found in passes 1 & 2, unless we are trying a background
	;; adjustment to fix a very long peak -- see
	;; set-median-by-histogram where the new medianamp and
	;; PKthresh values are set.
	(if (= nInThAdj 0)
	    (setq medianamp (aref aBkgMean endblock)
		  PKthresh (aref aBkgThresh endblock)))

	(if (< (aref aRawData endblock) PKthresh)
	    (setq endblock (tcr-dobkgndregion)) ;Start background block
	  (setq endblock (tcr-dopossiblePK))) ;Else start possible peak

	;; Now get ready for the next block
	(setq stblock endblock	;update start of next block
	      stline endblock
	      endline stline))

      ;; Write the intermediate results to the output buffers unless
      ;; not debugging
      (if (/= debugsw 0)
	  (tcr-write-debugging-buffer-peaks "Pass 3a: fixed background analysis"))

      ;; Pass 3.b - here we scan the detected peaks to see if any are
      ;; too close together or (eventually) the burst frequency ever
      ;; gets too high. If either condition is found, the smallest
      ;; peak in the group is eliminated.

      (setq nPairPrune 0		;Init some stuff
	    minPairLocDiff (/ minPairTimeDiff SampleInterval) ;Min dist ticks
	    idx1 (FindNextBigPeak 0.0 0) ;Index to 1st peak
	    idx2 idx1)			 ;End setq

      (while (/= idx2 -1)		;Loop through all peaks
	(if (= (setq idx2 (FindNextBigPeak 0.0 idx2)) -1) nil ;Quit if no pks
	  ;; Here we check to see if the inter-peak distance is too
	  ;; small. If so we remove the smaller of the peaks.
	  (if (> (- idx2 idx1) minPairLocDiff)
	      (setq idx1 idx2)		;OK, move up to next pair
	    (setq nPairPrune (1+ nPairPrune)) ;Count removals
	    (if (< (aref aPKNetHt idx2)
		   (aref aPKNetHt idx1))      ;Prune lower pk
		(tcr-remove-peak idx2)	      ;2nd one
	      (tcr-remove-peak idx1)	      ;1st one
	      (setq idx1 idx2)		      ;Move up to next pair
	      )		       ;End if ht2 < ht1
	    )		       ;End if diff
	  )		       ;End if last peak
	)		       ;End while

      ;; And finish up with stats in the minibuffer
      (message "Pass 3: %d lines read; %d pks found; %d short pks; %d long pks; %d tall pks; %d unparabolic; %d thresh adjs; %d too close (< %d sec)"
	       LineTotal nPK nShort nLong nTall nDiscrd nThAdj nPairPrune
	       minPairTimeDiff)


      ;; Pass 4: Next we check over the detected peaks to try to find
      ;; and separate multiplets. To do this, we use the  least squares
      ;; fit parabola to estmate the 1st derivative along the curve,
      ;; and look for signature zero crossings from a positive
      ;; derivative to a negative derivative with large amplitude
      ;; swings. We bound sub-peaks at the zero crossings and
      ;; recompute the peak maximum and total integrated energy for
      ;; each member of the multiplet. Multiplet members must still
      ;; satisfy the minimum peak duration requirement.

      ;; Then we produce the final output, merging original raw data
      ;; with resolved peak multiplets for which the maximum peak
      ;; values and integrated energy (SEL) is recomputed from the raw
      ;; data over the resolved peak limits.

      (setq nPK 0			;Init some variables
	    nShort 0			;Count peaklets that are too short
	    nMult 0
	    nAbortedPks 0	  ;Count aborted pks (too many pklets)
	    nMarginalPks 0)

      (setq nPK (tcr-merge-raw-data-and-resolve-multiplets))

      ;; First write the header and the complex noisy structure data to the
      ;; debugging buffer.

      (tcr-write-debugging-info-3 "Pass 4.a: Data for detecting noisy peak complexes (number of direction changes, average amplitude difference per change, & average location difference per change.)
" "%d\t%.1f\t%.1f"
				  LineTotal
				  bnumDirChanges
				  baveAmpChange
				  baveAmpChgDist)

      ;; Now write the header and the zero crossing data (raw and
      ;; pruned) to the debugging buffer.

      (tcr-write-debugging-info-2 "Pass 4.b: Raw & Pruned Zero Crossing Data for Detected Peaks
"
				  "%d\t%d"
				  LineTotal bZeroCrossings aZeroCrossings)

      ;; And always write the final results to the output buffers
      (setq hnfreqave (floor nfreqave 2)) ;Ready to compute peak frequencies
      (tcr-write-buffers "Pass 4.a: Full record")
      (tcr-write-freq "Pass 4.b: All Peaks-only list" 0.0)
      (tcr-write-freq "Pass 4.c: >= Threshold Peaks-only list" OPkThreshold)
      (tcr-write-peakless-bkgnd "Pass 4.d: Peaks removed")

      ;; And finish up with stats in the minibuffer
      (message "Pass 4: %d lines read; %d pks found; %d multiplets resolved; %d pks too noisy; %d short peaklets; %d marginal pks"
	       LineTotal nPK nMult nAbortedPks nShort nMarginalPks)
))))

(defun tcr-remove-peak (PkLoc)
"This function removes a peak detected at PkLoc and fills back in
the local background (in aPKSEL) with raw peak data"
(let (idx idxmin idxmax PkLen)
  (save-excursion
    (save-restriction

      ;; First locate the endpoints of the peak complex
      (setq PkLen (/ (aref aPKWidth PkLoc) SampleInterval)
	    idxmin (- PkLoc (floor (/ PkLen 2))) ;Set pk bounds
	    idxmax (+ idxmin PkLen)
	    idx idxmin)			;Init loop counter

      ;; Then fill in the raw values w/o the peak
      (while (< idx idxmax)
	(aset aPKSEL idx (aref bRawData idx))
	(setq idx (1+ idx))
	)				;End while

      ;; Clear out the peak marker data
      (aset aPKMax PkLoc 0)
      (aset aPKNetHt PkLoc 0)
      (aset aPKWidth PkLoc 0)
      (aset aPKMarker PkLoc 0)
      (setq tcr-remove-peak PkLoc)	;Return pk location

      )					;End save-restriction
    )					;End save-excursion
  )					;End let
) ;End defun


(defun tcr-write-debugging-info-1 (HdrStr formatstr dArrySize dArry)
"This function writes out debugging information into a debugging
buffer. It includes a header line, followed by dArrySize lines of
data from dArry in appropriate format."
(let (idx)
  (save-excursion
    (save-restriction

      (if (= debugsw 0) (setq tcr-write-debugging-info-1 debugsw)
	(set-buffer Data-Debug-bfr)	;Put output in debugging buffer
	(if (/= (point) (point-min)) (insert "
"))					;Blank line if needed

	(insert HdrStr)			;Write the header string
	(setq idx -1)			;Set to loop through whole array
	(while (< (setq idx (1+ idx)) dArrySize)
	  (insert (format formatstr (aref dArry idx)) "
")
	  )				;End while

	(set-buffer Data-bfr)		;Back to the data buffer
	(setq tcr-write-debugging-info-1 dArrySize)) ;Return array size
      ))))					;End defun

(defun tcr-write-debugging-info-2 (HdrStr formatstr dArrySize dArry1 dArry2)
"This function writes out debugging information into a debugging
buffer. It includes a header line, followed by dArrySize lines of
two values each in appropriate format."
(let (idx)
  (save-excursion
    (save-restriction

      (if (= debugsw 0) (setq tcr-write-debugging-info-2 debugsw)
	(set-buffer Data-Debug-bfr)	;Put output in debugging buffer
	(if (/= (point) (point-min)) (insert "
"))					;Blank line if needed

	(insert HdrStr)			;Write the header string
	(setq idx -1)			;Set to loop through whole array
	(while (< (setq idx (1+ idx)) dArrySize)
	  (insert (format formatstr
			  (aref dArry1 idx)
			  (aref dArry2 idx)) "
")
	  )				;End while

	(set-buffer Data-bfr)		;Back to the data buffer
	(setq tcr-write-debugging-info-2 dArrySize)) ;Return array size
      ))))					;End defun

(defun tcr-write-debugging-info-3 (HdrStr formatstr dArrySize dArry1 dArry2 dArry3)
"This function writes out debugging information into a debugging
buffer. It includes a header line, followed by dArrySize lines of
three values each in appropriate format."
(let (idx)
  (save-excursion
    (save-restriction

      (if (= debugsw 0) (setq tcr-write-debugging-info-3 debugsw)
	(set-buffer Data-Debug-bfr)	;Put output in debugging buffer
	(if (/= (point) (point-min)) (insert "
"))					;Blank line if needed

	(insert HdrStr)			;Write the header string
	(setq idx -1)			;Set to loop through whole array
	(while (< (setq idx (1+ idx)) dArrySize)
	  (insert (format formatstr
			  (aref dArry1 idx)
			  (aref dArry2 idx)
			  (aref dArry3 idx)) "
")
	  )				;End while

	(set-buffer Data-bfr)		;Back to the data buffer
	(setq tcr-write-debugging-info-3 dArrySize)) ;Return array size
      ))))					;End defun

(defun tcr-merge-raw-data-and-resolve-multiplets ()
"In areas outside of peaks, copy the raw initial peak values into
aPKSEL. Inside of peak areas, try to resolve multiplet subpeaks."
(let (idx idx0 idxtmp Cnt PkTime PkLen hPkLen idxmin idxmax PkGlobalMax
	  PkGlobalLoc nDirChg DirChgSw DirAmpChg DirChgInterval
	  aveDirAmpChg aveDirChgInterval Amp1 Amp2 LastExtremumLoc
	  LastExtremumAmp CurrAmp)
  (save-excursion
    (save-restriction

      (setq idx -1)			;Init some stuff

      (while (< (setq idx (1+ idx)) LineTotal) ;Do the whole array
	(if (/= (aref aPKMarker idx) 1)
	    (aset aPKSEL idx (aref bRawData idx)) ;Not peak, store raw data

	  ;; Here we found a previously detected peak. Check its shape
	  ;; characteristics and if OK, look for compound peak
	  ;; components
	  (setq idx0 idx
		PkTime (aref aPKWidth idx0)
		PkLen (/ PkTime SampleInterval) ;Pk len in samples
		hPkLen (floor PkLen 2)	      ;Peak half width
		idxmin (- idx0 hPkLen)	      ;idx to start of peak
		idxmax (1- (+ idxmin PkLen))) ;idx to end of peak

	  ;; Analyze noisiness of the peak
	  (setq idxtmp idxmin		;Start at the left end
		PkGlobalMax 0
		nDirChg 0		;Count of direction changes
		DirAmpChg 0		;Amplitude swing
		DirChgInterval 0	;Cumulative distance between changes
		Amp1 (aref aRawData idxmin)	;1st two data points
		Amp2 (aref aRawData (1+ idxmin))
		DirChgSw (if (>= Amp2 Amp1) 1
			   -1)
		LastExtremumLoc idxmin
		LastExtremumAmp (aref aRawData idxmin)) ;End setq

	  (while (<= idxtmp idxmax)
	    (if (> (setq CurrAmp (aref aRawData idxtmp)) PkGlobalMax)
		(setq PkGlobalMax CurrAmp
		      PkGlobalLoc idxtmp))	;End if

	    ;; Here we check on rapid change in peak amplitudes
	    (if (= Amp1 Amp2) nil		;If no change, move on
	      (if (or (and (> Amp2 Amp1)
			   (= DirChgSw -1))
		      (and (< Amp2 Amp1)
			   (= DirChgSw 1)))
		  ;;Here we have a change of direction, record the event		
		  (setq DirChgSw (- 0 DirChgSw) ;Chg from up to down
			nDirChg (1+ nDirChg)    ;Count changes
			DirAmpChg (+ DirAmpChg (abs (- Amp1 LastExtremumAmp)))
			DirChgInterval (+ DirChgInterval
					  (- (1- idxtmp) LastExtremumLoc))
			LastExtremumAmp Amp1
			LastExtremumLoc (1- idxtmp)) ;End setq
		)			;End if change of direction
	      )				;End master if

	    ;; Now make ready for the next pt in the complex
	    (setq idxtmp (1+ idxtmp)
		  Amp1 Amp2		;Remember last/next amplitude
		  Amp2 (if (= idxtmp idxmax) Amp2
			 (aref aRawData (1+ idxtmp)))) ;End setq
	    )					       ;End while

	  ;; Useful average noise properties of the peak complex
	  (setq aveDirAmpChg (/ (float DirAmpChg) (float nDirChg))
		aveDirChgInterval (/ (float DirChgInterval) (float nDirChg)))

      ;; Here we record the direction transition information for debugging
      (aset bnumDirChanges idx0 (* nDirChg (/ (float NoisyPkUnitInterval) PkTime))) ;Normalize dir changes
      (aset baveAmpChange idx0 aveDirAmpChg)
      (aset baveAmpChgDist idx0 aveDirChgInterval)

      ;; Now finish testing the peak complex properties and resolve
      ;; any multiplets. Leave out (or (>= PkGlobalMax AmplitudePass)
      ;; for now. Remove condition (<= nDirChg MaxChangesTot)
      (if (and (<= nDirChg (* MaxChangesPerPk (/ PkTime (float NoisyPkUnitInterval))))
	       (>= aveDirChgInterval MinChangeSep))
	  ;; Here things look OK, so look for multiplets
	  (setq Cnt (tcr-resolve-multiplet idx) ;Resolve pk, ret # peaklets
		nPK (+ nPK Cnt)		    ;Update total pk count
		nMult (if (> Cnt 1) (1+ nMult)
			nMult)		;Count multiplets resolved
		idx idxmax)		;Place to pick up processing

	;; Here the peak is too small and noisy, forget it
	(aset aPKMarker idx0 0)
	(aset aPKSEL idx0 (aref bRawData idx0))
	(setq idx idx0
	      nAbortedPks (1+ nAbortedPks) ;Peak too cluttered, count as aborted peak
	      )				   ;End setq
	)				;End if peak too noisy
      )					;End PKmarker if
	)				;while loop

      (setq tcr-merge-raw-data-and-resolve-multiplets nPK))))) ;Return # peaks

(defun tcr-resolve-multiplet (idx0)
"idx0 points to the midpoint of a peak. First, use the 1st
derivative buffer to search for local extrema that may represent
the morphology of subpeaks in a multiplet. Such subpeaks must
satisfy minimum width requirements and represent well-resolved
subpeaks, i.e., the maximum positive 1st derivative and the
succeeding minimum negative 1st derivatives must be big enough to
isolate a significant peak. Otherwise, leave the purported
subpeak merged with the preceeding subpeak. Function returns the
number of peaks found."
(let (idx idxtmp idxmin idxmax idxwrk idxl idxr DerivTry PkLen
	  hPkLen MultPkCnt BkgValue BkgThreshold PkletMax bPkletMax MaxLoc
	  PkletEnergy PkletLen PkletTime onslope Intensity bIntensity Cderiv
	  Clderiv nlderiv Crderiv nrderiv NewPkletDist
	  PkGlobalMax SignifAmpChg amppeaklet amppeakletLoc
	  nPKlets nPKletsToDo namppeaklet namppeakletLoc
	  ampPkletFlank idxloc PkGlobalLoc dipr diprLoc dipl
	  diplLoc ParabParamScale)
  (save-excursion
    (save-restriction

      (setq idx 0			;Init some stuff
	    MultPkCnt 0			;Count resolved peaks
	    PkLen (/ (aref aPKWidth idx0) SampleInterval) ;PkLen in samples
	    hPkLen (floor PkLen 2)	;Peak half width
	    idxmin (- idx0 hPkLen)	;idx to start of peak
	    idxmax (1- (+ idxmin PkLen))) ;idx to end of peak

      ;; Find the global maximum in the peak complex.
      (setq idxtmp idxmin		;Start at the left end
	    PkGlobalMax 0)		;End setq

      (while (<= idxtmp idxmax)
	(if (> (aref aRawData idxtmp) PkGlobalMax)
	    (setq PkGlobalMax (aref aRawData idxtmp)
		  PkGlobalLoc idxtmp))	;End if

	;; Now make ready for the next pt in the complex
	(setq idxtmp (1+ idxtmp)) ;End setq
	)				;End while

      ;; Set thresholds based on the main peak
      (setq BkgValue (aref aBkgMean idx0)
	    BkgThreshold (aref aBkgThresh idx0)
	    SignifAmpChg (+ (* MultipletThresh (- PkGlobalMax BkgThreshold))
			    BkgThreshold)
	    ParabParamScale (/ (- PkGlobalMax BkgValue) ParabRefAmp)
	    ampPkletFlank (* MultipletFlank BkgThreshold))

      ;; Find 1st derivative zero crossings (extrema) that occur in
      ;; the peak complex. The working crossing data is in
      ;; aZeroCrossings and a debugging copy of the original is in
      ;; bZeroCrossings.

      (setq idxtmp (1- idxmin))		;Again start at the left end
      (while (<= (setq idxtmp (1+ idxtmp)) idxmax)
	;; Assume no peak present
	(aset aZeroCrossings idxtmp 0)
	(aset bZeroCrossings idxtmp 0)

	;; Look for next peak, starting with the left side upslope
	(if (<= (setq Clderiv (aref aParabFit-b idxtmp)) 0) nil
	  (setq idx2 idxtmp
		nlderiv 0)
	  (while (and (<= (setq idx2 (1+ idx2)) idxmax)
		      (> (setq onslope (aref aParabFit-b idx2)) 0))
	    (setq Clderiv (max Clderiv onslope)
		  nlderiv (1+ nlderiv))) ;End while

	  ;; Max up slope must exceed MinPKslope within complex minNslopePklet
;;	  (if (or (>= idx2 idxmax)
;;		  (< Cderiv MinPKslope)) (setq idxtmp idx2) ;Nope, move on
;;	  (if (< Cderiv (* MinPKslope ParabParamScale))
;;	  (if (< Cderiv (* MinPKslope 1.0))

	  ;; First we must have long enough upslope
	  (if (< nlderiv minNslopePklet)
		 (setq idxtmp idx2) ;Nope, move on

	    ;; OK so far, now check down slope
	    (setq Crderiv (aref aParabFit-b idx2)
		  nrderiv 0)
	    (while (and (<= (setq idx2 (1+ idx2)) idxmax)
			(< (setq onslope (aref aParabFit-b idx2)) 0))
	      (setq Crderiv (min Crderiv onslope)
		    nrderiv (1+ nrderiv))) ;End while

	    ;; Max down slope magnitude must exceed MinPKslope as well
	    ;; within complex
;;	    (if (or (>= idx2 idxmax)
;;		    (< (abs Cderiv) MinPKslope)) (setq idxtmp idx2) ;Nope...
;;	    (if (< (abs Cderiv) (* MinPKslope ParabParamScale))
;;	    (if (< (abs Cderiv) (* MinPKslope 1.0))

	    ;; Then we must have long enough downslope
	    (if (< nrderiv minNslopePklet)
		(setq idxtmp idx2) ;Nope...

	      ;; Looks like a good peak, find the real maximum value
	      ;; and location
	      (setq amppeaklet 0
		    amppeakletLoc 0
		    idxl idxtmp)
	      (while (<= idxtmp idx2)
		(if (> (aref aRawData idxtmp) amppeaklet)
		    (setq amppeaklet (aref aRawData idxtmp)
			  amppeakletLoc idxtmp)) ;End if
		(setq idxtmp (1+ idxtmp))
		)			;End while

	      ;; Set pklet feature thresholds and make sure they
	      ;; satisfy them: height above flank; enough up/down
	      ;; slope; wide enough; top not too close to complex ends
	      (setq ParabParamScale (/ (- amppeaklet BkgValue) ParabRefAmp))

;; (if (> amppeakletLoc 616) (edebug))

	      (if (or (< amppeaklet ampPkletFlank)
		      (< Clderiv (* MinPKslope ParabParamScale))
		      (< (abs Crderiv) (* MinPKslope ParabParamScale))
		      (< (- idxtmp idxl -1) (* 2 minNslopePklet))
		      (< (min (- idxmax amppeakletLoc -1)
			      (- amppeakletLoc idxmin -1))
			 MinPKmax-EdgeDist))
		  nil	       ;No go, peaklet faulty so don't mark it
		(aset aZeroCrossings amppeakletLoc
		      (aref aRawData amppeakletLoc))
		(aset bZeroCrossings amppeakletLoc
		      (aref aRawData amppeakletLoc))
		)				;End save if
	      )					;End down slope if
	    )					;End up slope if
	  )					;End start zero cross search if
	)					;End outer loop while

      ;; Fix up end pts for the complex -- both marked as dips
      (aset aZeroCrossings idxmin (- 0 (aref aRawData idxmin)))
      (aset aZeroCrossings idxmax (- 0 (aref aRawData idxmax)))
      (aset bZeroCrossings idxmin (- 0 (aref aRawData idxmin)))
      (aset bZeroCrossings idxmax (- 0 (aref aRawData idxmax)))

      ;; Here we have the multiplet sub-maxima locations (if any).  We
      ;; begin at the maximal peak in this complex, and work out on
      ;; both sides looking for subsidiary peaklets that are separated
      ;; by about a minimum peak width from the main peak and each
      ;; other. Peaks that are too close are merged with the next
      ;; closest peak to the main peak. Further criteria are used to
      ;; test and edit peaks based on intervening dips. We want to
      ;; pick the lowest dip between multiplet subpeaks and test to
      ;; see if the subpeaks are really surrounded by significantly
      ;; lower areas to make them stand out as a separate peak. Once
      ;; an acceptable peaklet is found, the process continues outward
      ;; from the newly found peaklet.
      (setq NewPkletDist
	    (floor (/ minPkletSep SampleInterval))) ;Min sep

      (setq nPKlets (FilterPeakletTops idxmin PkGlobalLoc idxmax NewPkletDist)
	    MultPkCnt (if (> nPKlets 1) (1+ MultPkCnt)
		    MultPkCnt))		;Bump count if multiplet
;;(edebug)
      ;; Clear the current peak marker information.
      (aset aPKMax idx0 0)
      (aset aPKNetHt idx0 0)
      (aset aPKWidth idx0 0)
      (aset aPKMarker idx0 0)

      (if (<= nPKlets 0)		;If peak was too cluttered, quit
	  (setq nAbortedPks (1+ nAbortedPks)	;Count aborted peaks
		tcr-resolve-multiplet 0) ;Return 0 count

	;; Peak complex resolved, and for now fill in the PKSEL values
	;; inside the complex to the background mean.
	(setq idx 0)
	(while (<= (setq idxwrk (+ idxmin idx)) idxmax)
	  (aset aPKSEL idxwrk BkgValue)
	  (setq idx (1+ idx))
	  )				;End while

	;; Here we have the peaklet boundaries laid out so calculate the
	;; maximum peaklet amplitudes and peaklet energies to put into
	;; the aPKSEL, aPKMax, aPKWidth, and aPKMarker arrays.

	(setq idxtmp idxmin		;Place to start scanning
	      nPKletsToDo nPKlets
	      diplLoc idxmin)

	(while (> nPKletsToDo 0)		;Do all pklets
	  ;; Here find the next peaklet top and then the following dip
	  (setq amppeakletLoc (FindNextPeaklet idxtmp idxmax))
	  (if (= nPKletsToDo 1)
	      (setq diprLoc idxmax)	;If the last pklet, use right edge
	    (setq diprLoc (FindNextDip amppeakletLoc idxmax)))

	  ;; Error check
	  (if (or (> amppeakletLoc idxmax)
		  (> diprLoc idxmax))
	      (edebug))			;We should never get here

	  ;; Now tally up the integrated energy and the raw maximum
	  (setq idxwrk diplLoc
		PkletMax 0
		bPkletMax 0
		PkletEnergy 0)
	  (while (< idxwrk diprLoc)
	    (setq Intensity (aref aRawData idxwrk)
		  bIntensity (aref bRawData idxwrk)
		  PkletEnergy (+ PkletEnergy bIntensity))
	    (if (>= Intensity PkletMax)	     ;New highest intensity?
		(setq PkletMax Intensity	     ;Record value
		      MaxLoc idxwrk))	     ;and where it happened
	    (if (>= bIntensity bPkletMax)	     ;New highest intensity?
		(setq bPkletMax bIntensity))	     ;Record value
	    (setq idxwrk (1+ idxwrk))
	    )				;End while

	  ;; Special case for last peaklet in the complex - final edge pt
	  (if (/= nPKletsToDo 1) nil
	    (setq Intensity (aref aRawData diprLoc)
		  bIntensity (aref bRawData diprLoc)
		  PkletEnergy (+ PkletEnergy bIntensity))
	    (if (> Intensity PkletMax)	     ;New highest intensity?
		(setq PkletMax Intensity	     ;Record value
		      MaxLoc diprLoc)	     ;and where it happened
	      )				     ;End if
	    (if (> bIntensity bPkletMax)	     ;New highest intensity?
		(setq bPkletMax bIntensity)	     ;Record value
	      )
	    )				     ;End last peaklet if

	  (if (and (= nPKlets 1)		;If we have a singlet
		   (<= (* SampleInterval (- idxmax idxmin)) minDuration))
	      (setq nMarginalPks (1+ nMarginalPks)) ;Cnt marginal peaks
	    )					    ;End if

	  ;; OK, now save the peaklet data
	  (setq PkletMax (max PkletMax bPkletMax) ;Use the bigger pk ht estimate
		PkletLen (if (> nPKletsToDo 1)
			     (- diprLoc diplLoc)
			   (- diprLoc diplLoc -1)) ;Add 1 to len if last pklet
		PkletTime (* SampleInterval PkletLen)
		PkletEnergy (* SampleInterval PkletEnergy))
	  (if (< PkletTime minPKletDuration) ;Ignore very short peaks
	      (setq nPKlets (1- nPKlets)     ;Reduce peaklet count
		    nShort (1+ nShort))	     ;And count up short ones
	    ;; Otherwise, save the peaklet information
	    (aset aPKSEL MaxLoc (- PkletEnergy (* PkletLen (aref aBkgMean MaxLoc)))) ;Fill in the peaklet data
	    (aset aPKMax MaxLoc PkletMax)
	    (aset aPKNetHt MaxLoc (max 1 (- PkletMax (aref aBkgMean MaxLoc))))
	    (if (< (aref aPKNetHt MaxLoc) 0) (edebug))
	    (aset aPKWidth MaxLoc PkletTime)
	    (aset aPKMarker MaxLoc 1)
	    )				;End if

	  ;; Ready to do any more peaklets
	  (setq nPKletsToDo (1- nPKletsToDo) ;Decrement pklets to do
		idxtmp diprLoc		;Assume another peaklet coming
		diplLoc idxtmp)		;Start at last inter-pklet min
	  )				;End while to scan whole complex

	(setq tcr-resolve-multiplet nPKlets) ;Return # of resolved peaks
	)				     ;End if peak complex deleted
      ))))				 ;End defun

(defun FindLowestDip (LeftPkLoc RightPkLoc)
"Function to scan dips between two bounding peaklets and locate
the lowest one. It returns the location of the lowest dip."

(let (idxtmp mindip mindiploc dipvalue)
  (save-excursion 
    (save-restriction

      (setq idxtmp (1+ LeftPkLoc)	;Where to start the scan
	    mindip (min (aref aRawData LeftPkLoc) (aref aRawData RightPkLoc))
	    mindiploc -1)
      (while (< idxtmp RightPkLoc)	;Find the lowest dip
	(if (<= (setq dipvalue (aref aRawData idxtmp)) mindip)
	    (setq mindip dipvalue		;Better choice, record values
		  mindiploc idxtmp))
	(setq idxtmp (1+ idxtmp)))	;Look at all pts between peaks

;;      (if (= mindiploc -1) (edebug)	       ;This shouldn't happen
	(setq FindLowestDip mindiploc)

      ))))				;End defun

(defun FindNextPeaklet (StLoc EndLoc)
"Function to find the next recorded peaklet in the array
aZeroCrossings. StLoc is the starting peaklet, and EndLoc marks
the end of the peak complex area. Whether or not EndLoc is
greater or less than StLoc determines direction of the search to
be outward from the starting peaklet. The return value is the
location of the next peaklet, and -1 if there is none"
      
(let (idxtmp srchdir)
  (save-excursion 
    (save-restriction
      (if (> EndLoc StLoc) (setq srchdir 1)
	(setq srchdir -1))
      (setq idxtmp (+ StLoc srchdir))	;Where to start the scan
      (while (and (/= idxtmp EndLoc)	;Find the next peaklet
		  (<= (aref aZeroCrossings idxtmp) 0))
	(setq idxtmp (+ idxtmp srchdir)))

      (if (= idxtmp EndLoc) (setq FindNextPeaklet -1)
	(setq FindNextPeaklet idxtmp))
      ))))				;End defun

(defun FindNextDip (StLoc EndLoc)
"Function to find the next recorded dip between peaklets in the
array aZeroCrossings. StLoc is the starting peaklet, and EndLoc
marks the end of the peak complex area. Whether or not EndLoc is
greater or less than StLoc determines direction of the search to
be outward from the starting peaklet."
      
(let (idxtmp srchdir)
  (save-excursion 
    (save-restriction
      (if (> EndLoc StLoc) (setq srchdir 1)
	(setq srchdir -1))
      (setq idxtmp (+ StLoc srchdir))	;Where to start the scan
      (while (and (/= idxtmp EndLoc)	;Find the next peaklet
		  (>= (aref aZeroCrossings idxtmp) 0))
	(setq idxtmp (+ idxtmp srchdir)))

      (if (= idxtmp EndLoc) (setq FindNextDip EndLoc)
	(setq FindNextDip idxtmp))

      ))))				;End defun

(defun FilterPeakletTops (idxleft idxmain idxright InterPkletDist)
"This multiplet resolution helper function makes use of the
purported peaklet locations indicated in aZeroCrossings. It
starts at the main central peak and works outward to the right
and left. It finds neighboring peaklets and tests to see if they
are far enough from the previous peaklet (InterPkletDist), and
have a dip in between that is low enough to clearly make the two
peaks stand out as separate. If peaklet locations are encountered
that cannot indicate presence of a valid peaklet, they are
removed so that the peak area involved is joined with an earlier
peaklet neighbor."

(let (idxsrchleft idxsrchright idxtmp dipl diplLoc dipr diprLoc
		  diplMag diprMag nPklets)
  (save-excursion 
    (save-restriction
      ;; First, do the right half of the complex
      (setq nPklets 0			;At least count the main peak
	    idxsrchleft idxmain
	    idxsrchright idxright)

      (while (and (> idxsrchright idxsrchleft)
		  (/= (setq idxtmp
			    (FindNextPeaklet idxsrchleft idxsrchright)) -1))
	;; Soon we compute the parabola fit rms error here as a criterion
	(if (< (- idxtmp idxsrchleft) InterPkletDist)
	    (aset aZeroCrossings idxtmp 0) ;Too close, delete subpeak
	  (setq diprLoc (FindLowestDip idxsrchleft idxtmp) ;OK, find dip betw
		diprMag (min (aref aRawData idxsrchleft)
			     (aref aRawData idxtmp))) ;Ref peaklet height
	  (if (or (= diprLoc -1)
		  (< (* SampleInterval 2 (- idxtmp diprLoc)) minPKletDuration)
		  (> (aref aRawData diprLoc) (* MultipletThresh diprMag)))
	      (aset aZeroCrossings idxtmp 0) ;Too little, delete subpeak
	    (aset aZeroCrossings diprLoc (- 0 diprMag))	;pklet OK, record dip
	    (setq nPklets (1+ nPklets)			;Count new one
		  idxsrchleft idxtmp)	;move right to next pair
	    )				   ;End if
	  )				   ;End if
	)				   ;End while

      ;; Then, do the left half of the complex
      (setq idxsrchleft idxleft
	    idxsrchright idxmain)

      (while (and (< idxsrchleft idxsrchright)
		  (/= (setq idxtmp
		      (FindNextPeaklet idxsrchright idxsrchleft)) -1))
	;; Soon we compute the parabola fit rms error here as a criterion
	(if (< (- idxsrchright idxtmp) InterPkletDist)
	    (aset aZeroCrossings idxtmp 0) ;Too close, delete subpeak
	  (setq diplLoc (FindLowestDip idxtmp idxsrchright) ;OK, find dip betw
		diplMag (min (aref aRawData idxsrchright)
			     (aref aRawData idxtmp))) ;Ref peaklet height
	  (if (or (= diplLoc -1)
		  (< (* SampleInterval 2 (- diplLoc idxtmp)) minPKletDuration)
		  (> (aref aRawData diplLoc) (* MultipletThresh diplMag)))
	      (aset aZeroCrossings idxtmp 0) ;Too little, delete subpeak
	    (aset aZeroCrossings diplLoc (- 0 diplMag))	;pklet OK, record dip
	    (setq nPklets (1+ nPklets)			;Count new one
		  idxsrchright idxtmp)	;Move left to next pair
	    )				   ;End if
	  )				   ;End if
	)				   ;End while

      ;; Check if there are too many pklets
      (if (<= nPklets MaxPklets)
      ;; OK, return the count of pklets, including the main one
	  (setq FilterPeakletTops (1+ nPklets))
	;; Too many, go back over complex and delete all the peaks
	(setq idxtmp idxleft)
	(while (<= idxtmp idxright)
	  (if (> (aref aZeroCrossings idxtmp) 0)
	      (aset aZeroCrossings idxtmp 0))
	  (setq idxtmp (1+ idxtmp))
	  )				;End while
	(setq FilterPeakletTops 0)
	)				;End if
      ))))				;End of defun

(defun tcr-do-parabolic-fit (InArr Nelements nFit)
"Here we compute a running parabolic least squares fit (a + b*x +
c*x^2) of a data stream (of length Nelements in Inarr), over nFit
points. The parabola parameters are written to global arrays
aParabFit-a, aParabFit-b, and aParabFit-c. The end points of the
fit, within nFit/2 of the beginning and end of the data stream,
are set to 0"
(let (fitptr idx yave xyave x2yave fx2ave fx4ave fx4ave-fx2ave2
	     hnFit EndInnerElements)
  (save-excursion
    (save-restriction

      (setq idx 0		 ;Init array indices and params
	    hnFit (floor nFit 2)
	    fx2ave 0.0
	    fx4ave 0.0)

      ;; First compute the constant factors just involving x
      (while (< idx hnFit)
	(aset aParabFit-a idx 0.0) ;1st and last few output values are 0
	(aset aParabFit-b idx 0.0)
	(aset aParabFit-c idx 0.0)
	(aset aParabFit-a (- Nelements idx 1) 0.0)
	(aset aParabFit-b (- Nelements idx 1) 0.0)
	(aset aParabFit-c (- Nelements idx 1) 0.0)
	(setq fx2ave (+ fx2ave (expt (- hnFit idx) 2))
	      fx4ave (+ fx4ave (expt (- hnFit idx) 4))
	      idx (1+ idx)))

      (setq fx2ave (/ (* fx2ave 2) (float nFit)) ;Add 2nd half of fit area
	    fx4ave (/ (* fx4ave 2) (float nFit))
	    fx4ave-fx2ave2 (- fx4ave (expt fx2ave 2))
	    fitptr hnFit		;Start of inner elements
	    EndInnerElements (- Nelements hnFit 1))

      (while (<= fitptr EndInnerElements) ;Now we compute the parameters
	(setq idx 1			  ;Init the LSQ sums
	      yave (aref InArr fitptr)	  ;Include the x=0 pt
	      xyave 0.0
	      x2yave 0.0)
	;; Now compute the various sums
	(while (<= idx hnFit)
	  (setq yave (+ yave (aref InArr (+ fitptr idx))
			(aref InArr (- fitptr idx)))
		xyave (+ xyave (* idx (- (aref InArr (+ fitptr idx))
					 (aref InArr (- fitptr idx)))))
		x2yave (+ x2yave (* idx idx (+ (aref InArr (+ fitptr idx))
					       (aref InArr (- fitptr idx)))))
		idx (1+ idx))
	  )				;End while

	;; Finish computing averages
	(setq yave (/ yave (float nFit))
	      xyave (/ xyave (float nFit))
	      x2yave (/ x2yave (float nFit)))

	(aset aParabFit-a fitptr (/ (- (* yave fx4ave) (* fx2ave x2yave))
				    fx4ave-fx2ave2)) ;a parameter
	(aset aParabFit-b fitptr (/ xyave fx2ave))   ;b parameter
	(aset aParabFit-c fitptr (/ (- x2yave (* yave fx2ave))
				    fx4ave-fx2ave2)) ;c parameter
	(setq fitptr (1+ fitptr))
	)				;End outer while

	(setq tcr-do-parabolic-fit t)
      ))))

(defun tcr-do-box-filter (InArr Nelements Nfilterbox) 
"Here we prefilter the raw data with a box filter boxFilterWidth
wide. We keep a copy of the raw data in InArr, and the filtered
data in the global array aRawData. We also write the filtered
data to Data-LPF-bfr."
(let (locptr idx fvalue fwgt hNfilterbox Ninnerelements)
  (save-excursion
    (save-restriction

      (setq idx 0			;Init array indices and filter set-up
	    locptr 1
	    hNfilterbox (floor Nfilterbox 2)
	    fwgt hNfilterbox
	    fvalue (aref InArr 0))

      (while (<= locptr hNfilterbox)	;Init partially overlapping filter
	(setq fvalue (+ fvalue (aref InArr locptr))
	      locptr (1+ locptr)))

      (while (<= idx hNfilterbox)	;Start the 1st partial values
	(aset aRawData idx (/ fvalue (setq fwgt (1+ fwgt))))
	(setq idx (1+ idx)
	      fvalue (+ fvalue (aref InArr (+ hNfilterbox idx)))))
      (setq fvalue (- fvalue (aref InArr (- hNfilterbox idx -1))))

      (setq Ninnerelements (- Nelements hNfilterbox 1))
      (while (< idx Ninnerelements)	;Do the main part of data
	(aset aRawData idx (/ fvalue  fwgt))
	(setq fvalue (+ fvalue (aref InArr (+ idx hNfilterbox 1)))
	      fvalue (- fvalue (aref InArr (- idx hNfilterbox)))
	      idx (1+ idx)))

      (while (< idx Nelements)	;Do the ending overlap part
	(aset aRawData idx (/ fvalue fwgt))
	(setq fvalue (- fvalue (aref InArr (- idx hNfilterbox)))
	      fwgt (1- fwgt)
	      idx (1+ idx)))))))

(defun tcr-write-debugging-buffer-peaks (hdrstring)
"Here we output a complete set of peak data and background data from an analysis run"
(let (idx)

;; First, write the header in the debugging buffer. If not at the
;; beginning, space down a line to separate from the earlier output.
  (set-buffer Data-Debug-bfr)
  (if (/= (point) (point-min)) (insert "
"))					

  (insert hdrstring "
")
  (setq idx -1)
  (while (< (setq idx (1+ idx)) LineTotal) ;Loop through the whole array
    ;; Do the whole thing if this is a peak line
    (if (= (aref aPKMarker idx) 1)
	(insert (format "%.1f\t%.1f\t%.1f\t%d\tPEAK\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f
"
			(aref aPKSEL idx)
			(aref aPKMax idx)
			(aref aPKNetHt idx)
			(aref aPKWidth idx) 
			(aref aBkgMean idx)
			(aref aBkgThresh idx)
			(aref aParabFit-A idx)
			(aref aParabFit-B idx)
			(aref aParabFit-C idx)
			(aref aRMSParabFitErr idx)
			(aref aAveUpslope idx)
			(aref aAveDownslope idx)
			(aref aAveCurvature idx)
			(aref aMaxCurvature idx)
			))

      ;; Otherwise just write the data w/o peak info
      (insert (format "%.1f\t\t\t\t%.1f\t%.1f
"
		      (aref aPKSEL idx)
		      (aref aBkgMean idx)
		      (aref aBkgThresh idx)))
      )				;End if
    )				;End while
  (set-buffer Data-bfr)		;Back to the data buffer
  (setq tcr-write-debugging-buffer-peaks LineTotal)))

(defun tcr-write-buffers (hdrstring)
"Here we output a complete set of peak data and background data from an analysis run"
(let (idx)

;; First, write the header in the peak data buffer. If not at the
;; beginning, space down a line to separate from the earlier output.
  (set-buffer PK-bfr)
  (if (/= (point) (point-min)) (insert "
"))					

  (insert hdrstring "
")
  (setq idx -1)
  (while (< (setq idx (1+ idx)) LineTotal) ;Loop through the whole array
    ;; Do the whole thing if this is a peak line
    (if (= (aref aPKMarker idx) 1)
	(insert (format "%.1f\t%.1f\t%.1f\t%d\tPEAK\t%.1f\t%.1f
"
			(* 10 (log10 (aref aPKSEL idx)))
			(* 10 (log10 (aref aPKMax idx)))
			(* 10 (log10 (aref aPKNetHt idx)))
			(aref aPKWidth idx) 
			(* 10 (log10 (aref aBkgMean idx)))
			(* 10 (log10 (aref aBkgThresh idx)))))

      ;; Otherwise just write the data w/o peak info
      (insert (format "%.1f\t\t\t\t\t%.1f\t%.1f
"
		      (* 10 (log10 (aref aPKSEL idx)))
		      (* 10 (log10 (aref aBkgMean idx)))
		      (* 10 (log10 (aref aBkgThresh idx)))))
      )				;End if
    )				;End while
  (set-buffer Data-bfr)		;Back to the data buffer
  (setq tcr-write-buffers LineTotal)))

(defun tcr-write-freq (hdrstring outputthreshold)
"Here we output a selected set of peak and background data ready
for frequency analysis in Excel."
(let (idx ringidxst ringidxmid ringidxend ringinsert idxfreq tmp InPkPtr (npeaks 0) markerstr PkAveRing nToAve FreqValue)

  ;; First, write the header in the peak data buffer. If not at the
  ;; beginning, space down a line to separate from the earlier output.
  (set-buffer PK-bfr)
  (if (/= (point) (point-min)) (insert "
"))					

  (insert hdrstring "
")

  ;; Compute the peak entry marker text
  (if (= outputthreshold 0.0)
      (setq markerstr "PEAK")
    (setq markerstr (format "PK >=%d" (* 10 (log10 outputthreshold)))))

  ;; Set up array for computing interpeak distance average for frequence calculation
  (setq PkAveRing (make-vector nfreqave -1)) ;Set up primary data arrays, show empty

  ;; Fill array with first peak locations
  (setq ringinsert hnfreqave	    ;Only do half of the array to start
	nToAve 0	    ;Number in successive peak ring to average
	InPkPtr 0)		    ;Start looking for the first peak location in the raw data
  (while (and (< ringinsert nfreqave)
	      (>= nToAve 0))
    (setq InPkPtr (FindNextBigPeak outputthreshold InPkPtr)) ;Find next peak above outputthreshold
    (if (= InPkPtr -1)
	(setq nToAve -1)		;If not enough to start, abort
      (aset PkAveRing ringinsert InPkPtr) ;Got one, save the location
      (setq nToAve (1+ nToAve)	     ;# to average time intervals over
	    ringinsert (1+ ringinsert))	;Do another ring entry
      )				     ;End if
    )				 ;End while

  ;; Here we have filled the ring buffer with half+1 of the peak
  ;; average number, so we can begin filling in the output record
  ;; according to the templates below. At the ends, we use smaller
  ;; averages unless fully on the list of peaks

  (setq idx -1				;Init some stuff
	ringidxend (1- nfreqave)	;End of ring buffer ptr
	ringidxst hnfreqave		;Start of valid ring buffer data
	ringinsert 0			;Place to start inserting new data
	ringidxmid hnfreqave		;Ptr middle of the peak ptr ring -- needed???
	)				;End setq

  (while (> nToAve 0)	  ;Loop through all the peak groups
    ;; First compute the effective burst frequency at this point
    (setq FreqValue (/ (* (1- nToAve) (float LineTotal))
		       (* 24.0 (- (aref PkAveRing ringidxend) (aref PkAveRing ringidxst))))
	  idx (aref PkAveRing ringidxmid))

    ;; If zero threshold for peak selection, output stuff Excel expects:
    ;; Time, SEL, Pk Max, Pk Net Ht, Peak Width, Peak Mark, Freq/day
    (if (= outputthreshold 0.0)		
      (insert (format "%.14f\t%.1f\t%.1f\t%.1f\t%d\t"
		      (/ (float idx) (float LineTotal))
		      (* 10 (log10 (aref aPKSEL idx)))
		      (* 10 (log10 (aref aPKMax idx)))
		      (* 10 (log10 (aref aPKNetHt idx)))
		      (aref aPKWidth idx))
	      markerstr
	      (format "\t%.1f
"
		      FreqValue))

    ;; If non-zero threshold for peak selection, output stuff Excel expects:
    ;; Time, Pk Net Ht, Peak Mark, Freq/day
      (insert (format "%.14f\t%.1f\t"
		      (/ (float idx) (float LineTotal))
		      (* 10 (log10 (aref aPKNetHt idx))))
	      markerstr
	      (format "\t%.1f
"
		      FreqValue))
      )					;End if

;;(edebug)
    ;; Now prepare for the next line of output
      (setq npeaks (1+ npeaks))		;Count the peak just output

      ;; Find next peak above outputthreshold to add to the ring buffer
      (setq InPkPtr (FindNextBigPeak outputthreshold InPkPtr))
      (if (= InPkPtr -1)
	  ;; Here if no more peaks
	(setq nToAve (if (<= nToAve (1+ hnfreqave)) -1 ;If can't do more, abort
		       (1- nToAve)) ;Otherwise reduce # of peaks averaged
	      ringinsert (mod (1+ ringinsert) nfreqave)	;Place for next peak ptr
	      ringidxst (mod (1+ ringidxst) nfreqave) ;Bump ring idx grp start
	      ringidxmid (mod (1+ ringidxmid) nfreqave) ;Bump ring midpt idx
	      )						;End setq

	;; Here if we are still processing more good resolved peaks
	(aset PkAveRing ringinsert InPkPtr) ;Save the new location
	(setq nToAve (if (<= npeaks hnfreqave)
			 (1+ nToAve)
		       nToAve)	      ;Adjust average range, if needed
	      ringinsert (mod (1+ ringinsert) nfreqave)	;Place to put next good peak ptr
	      ringidxst (if (> npeaks hnfreqave)
			    (mod (1+ ringidxst) nfreqave)
			  ringidxst) ;Bump ring idx for grp start when all on data
	      ringidxmid (mod (1+ ringidxmid) nfreqave) ;Bump ring midpt index for group
	      ringidxend (mod (1+ ringidxend) nfreqave) ;Bump ring index for end of group
	      )						;End setq
	)						;End if
      )					;End nToAve while

  ;; Here we are all done
  (set-buffer Data-bfr)			;Back to the data buffer
  (setq tcr-write-freq npeaks)
  ))					;End defun

(defun FindNextBigPeak (PkHtThreshold StPtr)
"Here we look for the next resolved peak with net amplitude >=
PkHtThreshold, starting after location StPtr in the data
array. We return the data array index value of the peak. If we
run out of data in the array, return -1."

(let (idx sFlag)

  (setq idx StPtr	  ;Init local index
	sFlag 0)	  ;Init flag: 0 continue; >0 pk found; -1 fail

  (if (/= StPtr -1)
      (while (and (< (setq idx (1+ idx)) LineTotal) ;Loop through the whole array
		  (= sFlag 0))			;Until success or failure

	(if (and (= (aref aPKMarker idx) 1)	;Require resolved peak
		 (>= (aref aPKNetHt idx) PkHtThreshold)) ;That is tall enough
	    (setq sFlag idx)			;Success, record location
	  )					;End if
	)					;End while
  )					;End if

;;(edebug)
  (if (or (>= idx LineTotal)
	  (= StPtr -1))
      (setq FindNextBigPeak -1)		;Too far, show failure
    (setq FindNextBigPeak sFlag))	;Got one, return the index
))

(defun tcr-write-peakless-bkgnd (hdrstring)
"Here we output a complete set of peak data and background data from an analysis run"
(let (idx)

;; First, write the header in the peak data buffer. If not at the
;; beginning, space down a line to separate from the earlier output.
  (set-buffer PK-bfr)
  (if (/= (point) (point-min)) (insert "
"))					

  (insert hdrstring "
")
  (setq idx -1)
  (while (< (setq idx (1+ idx)) LineTotal) ;Loop through the whole array
    ;; Only do the background values, i.e., replace SEL value
    (if (/= (aref aPKMarker idx) 1)	;If no pk: Time <tab> <tab> SEL
	      (insert (format "%.14f\t\t%.1f
"
			      (/ (float idx) (float LineTotal))
			      (* 10 (log10 (aref aPKSEL idx)))))
      ;; If pk: Time <tab> Pk Mark <tab> Bkgnd
	(insert (format "%.14f\tPEAK\t%.1f
"
			(/ (float idx) (float LineTotal))
			(* 10 (log10 (aref aBkgMean idx)))))
	)				;End if
    )					;End while

  (set-buffer Data-bfr)		;Back to the data buffer
  (setq tcr-write-peakless-bkgnd LineTotal)))

(defun tcr-dopossiblePK ()
  "This may be an peak block -- first point above threshold. Find
the end and decide if it has an acceptable size and other
characteristics. If so, compute it's integrated energy and format
a new display of this block in the output buffer with all points
set to the background mean except the middle point which records
various peak parameters. If it is not a good peak, just copy the
raw data to the output buffer and update the background along the
way"

(let ((LtotPKenergy 0) LmeanPKenergy TtotPKenergy (LsqPKenergy 0)
		   LmeansqPKenergy LstdevPKenergy maxPKenergyLoc
		   (MaxUpslope 0) MaxUpslopeLoc (AveUpslope 0)
		   (MaxDownslope 0) MaxDownslopeLoc (AveDownslope 0)
		   (MaxCurvature 0) MaxCurvatureLoc (AveCurvature 0)
		   (nUpslope 0) (nDownslope 0) (nCurvature 0)
		   SlopeVal CurveVal ptmp pidx idxtmp ENTstline
		   ENTlinecnt ENTstblock ENTendblock leftblock
		   ParabParamScale RMSParabFitErr yFit yNorm
		   ParabFit-A  ; A param from form y = A + C*[x-B]^2
		   ParabFit-B  ; B param from form y = A + C*[x-B]^2
		   ParabFit-C  ; C param from form y = A + C*[x-B]^2
		   )
  (save-excursion
    ;; Now look to see where the end of the possible peak region
    ;; is. We enter with endblock set to the data index that
    ;; identified the 1st pt above threshold.
    (setq stline linecnt		;Note entry starting line #
	  ENTstline stline		;And actual entry values
	  ENTlinecnt linecnt
	  ENTstblock endblock)
;;	  tmp endblock)
;;	  endblock (1- endblock)	;Back up to get 1st data pt in block
;;	  maxPKenergy 0			;and find (global) max intensity
;;	  MaxCurvature 0		;and find max curvature
;;	  hCurveAveRng (floor CurveAveRng 2))

    ;; Find the leftmost start of this peak (start of positive
    ;; derivative or dropping below the mean/threshold value or end of
    ;; data)
    (setq idxtmp endblock)
    (while (and (>= (setq idxtmp (1- idxtmp)) 0)
		(> (aref aParabFit-b idxtmp) 0)
		(> (aref bRawData idxtmp) (aref aBkgMean idxtmp))))
;;		(> (aref aRawData idxtmp) (aref aBkgMean idxtmp))))
;;		(> (aref aRawData idxtmp) (aref aBkgThresh idxtmp))))

    ;; Save the new point starting this peak complex
    (setq linecnt (1+ idxtmp)
	  stline linecnt
	  endblock idxtmp		;last pt outside the block left end
	  leftblock idxtmp
	  nbackstep (- ENTstblock stline)) ;# prior output pts to back over

    ;; Find the rightmost end of this peak, first with PKthresh
    ;; criterion and then looking for the slope zero crossing
    (setq idxtmp ENTstblock)		;Back to where we entered
    (while (and (< (setq idxtmp (1+ idxtmp)) LineTotal)
		(>= (aref aRawData idxtmp) PKthresh)))

    (setq ENTendblock idxtmp)	      ;Save the threshold-based  end of block

    ;; Find the rightmost end of this peak (end of negative derivative
    ;; or dropping below the mean/threshold value or end of data)
    (while (and (< (setq idxtmp (1+ idxtmp)) LineTotal)
		(< (aref aParabFit-b idxtmp) 0)
		(> (aref bRawData idxtmp) (aref aBkgMean idxtmp))))
;;		(> (aref aRawData idxtmp) (aref aBkgMean idxtmp))))
;;		(> (aref aRawData idxtmp) (aref aBkgThresh idxtmp))))

    (setq endblock idxtmp)  ;New ptr to 1st pt outside block right end

    ;; Now scan the whole pk complex for extrema and other filtering
    ;; properties.
    (setq maxPKenergy 0			;and find (global) max intensities (filtered & unfiltered)
	  bmaxPKenergy 0
	  MaxCurvature 0		;and find max curvature
	  hCurveAveRng (floor CurveAveRng 2)
	  idxtmp leftblock)		;Place to start scan

    (while (< (setq idxtmp (1+ idxtmp)) endblock)
      (setq energyvalue (float (aref aRawData idxtmp))
	    benergyvalue (float (aref bRawData idxtmp)))
      (if (> energyvalue maxPKenergy)	;Find peak max/loc (filtered & unfiltered)
	  (setq maxPKenergy energyvalue
		maxPKenergyLoc idxtmp))
      (if (> benergyvalue bmaxPKenergy)	;Find peak max/loc (filtered & unfiltered)
	  (setq bmaxPKenergy benergyvalue))

      ;; Record the up/down slope extrema and averages
      (if (> (setq SlopeVal (aref aParabFit-b idxtmp)) 0)
	  (setq nUpslope (1+ nUpslope)
		AveUpslope (+ AveUpslope SlopeVal)
		MaxUpslope (if (<= SlopeVal MaxUpslope) MaxUpslope
			     (setq MaxUpslopeLoc idxtmp)
			     SlopeVal))	;End SlopeVal > 0 clause
	(if (< SlopeVal 0)
	  (setq nDownslope (1+ nDownslope)
		AveDownslope (+ AveDownslope SlopeVal)
		MaxDownslope (if (>= SlopeVal MaxDownslope) MaxDownslope
			     (setq MaxDownslopeLoc idxtmp)
			     SlopeVal))) ;End SlopeVal < 0 clause
	  )				 ;End if SlopeVal

      ;; Then record the max downward curvature extremum
      (if (< (setq CurveVal (aref aParabFit-c idxtmp)) MaxCurvature)
	  (setq MaxCurvature CurveVal
		MaxCurvatureLoc idxtmp)
	)				;End if CurveVal

      (setq LtotPKenergy (+ LtotPKenergy benergyvalue) ;Integrate tot pk energy
	    LsqPKenergy (+ LsqPKenergy (expt benergyvalue 2)) ;variance
	    linecnt (1+ linecnt))		      ;Bump line count
      )						      ;End while

    ;; Done with peak, compute parameters to see if it satisfies size
    ;; criteria
    (setq PKlength (- linecnt stline) ;Peak width in lines (2 sec each)
	  PKtime (* SampleInterval PKlength) ;Peak width in seconds
	  LmeanPKenergy (/ LtotPKenergy (float PKlength))
	  LmeansqPKenergy (/ LsqPKenergy (float PKlength))
;;	  LstdevPKenergy (sqrt (- LmeansqPKenergy (expt LmeanPKenergy 2)))
	  PKbaseenergy (* medianamp PKlength)  ;Area to median
	  PKthreshenergy (* PKthresh PKlength) ;Area to threshold
	  PKboxenergy (- (* maxPKenergy PKlength) PKbaseenergy) ;Pk box area
	  PKnetenergy (- LtotPKenergy PKbaseenergy) ;Net pk area
	  AveUpslope (if (> nUpslope 0)
			 (/ AveUpslope nUpslope)
		       0.0)
	  AveDownslope (if (> nDownslope 0)
			 (/ AveDownslope nDownslope)
			 0.0)
	  )				;End setq

    ;; Find the average downward curvature of the parabola near the max
    (if (= MaxCurvature 0) nil
      (setq AveCurvature MaxCurvature
	    pidx 0)
      (while (<= (setq pidx (1+ pidx)) hCurveAveRng)
	(setq AveCurvature (+ AveCurvature
			      (aref aParabFit-c (+ MaxCurvatureLoc pidx)) 
			      (aref aParabFit-c (- MaxCurvatureLoc pidx)))
	      )				;End setq
	)				;End while
      )					;End if
    (setq AveCurvature (/ AveCurvature CurveAveRng))

    ;; Find the RMS parabola fit error over the fit range
    (setq ParabFit-A 0
	  ParabFit-B 0
	  ParabFit-C 0
	  RMSParabFitErr 0
	  hNparabfit (floor Nparabfit 2)
	  pidx (- 0 hNparabfit))

    (if (or (< maxPKenergyLoc hNparabfit)
	    (>= maxPKenergyLoc (- LineTotal hNparabfit)))
	nil				;If out of bounds, move on

      ;; Otherwise, compute the rms fit error and the special form
      ;; parameters
      (while (<= pidx hNparabfit)
	(setq yFit (+ (aref aParabFit-a maxPKenergyLoc)
		      (* (aref aParabFit-b maxPKenergyLoc) pidx)
		      (* (aref aParabFit-c maxPKenergyLoc) (expt pidx 2)))
	      RMSParabFitErr (+ RMSParabFitErr
				(expt (- yFit
					 (aref aRawData (+ maxPKenergyLoc pidx)))
				      2))
	      pidx (1+ pidx)
	      )				;End setq
	)					;End while

      (setq RMSParabFitErr (sqrt (/ RMSParabFitErr (float Nparabfit))))

      ;; Compute the parameters of the form y = A + C*[x-B]^2 at
      ;; maxPKenergyLoc. Special case if totally flat.
      (if (= (aref aParabFit-c maxPKenergyLoc) 0)
	  (setq ParabFit-A (aref aParabFit-a maxPKenergyLoc)
		ParabFit-B 0.0
		ParabFit-C)
	;; No, curved so use the real formulas
	  (setq ParabFit-A (- (aref aParabFit-a maxPKenergyLoc)
			  (expt (/ (aref aParabFit-b maxPKenergyLoc)
				   2.0 (aref aParabFit-c maxPKenergyLoc))
				2))
	    ParabFit-B (- 0 (/ (aref aParabFit-b maxPKenergyLoc)
			       2.0 (aref aParabFit-c maxPKenergyLoc)))
	    ParabFit-C (aref aParabFit-c maxPKenergyLoc))
	  )				;End if no curvature
      )						;End if and ... maxPKenergyLoc

;;    (if (> linecnt 1017) (edebug))
;;    (if (and (> linecnt 16393)
;;	     (= bkgrndsw -1))
;;	(edebug))
;;	     (< linecnt 1150))
	

    ;; Check if peak too short 
    (if (< PKtime minDuration)
	(setq bkgrndsw (if (= bkgrndsw -1) -1
			 0) ;don't update bkgnd for short peaks,  may be spikes
	      nShort (1+ nShort)		  ;Bump short counter
	      stblock ENTstblock		  ;Start of copy
	      nbackstep 0			  ;No backstepping needed
	      stblock (tcr-copy-block-to-buffer)  ;Just copy if short
	      tcr-dopossiblePK stblock)

      ;; Check to see if peak too long, even with amplitude-based extension
      (if (<= PKtime (* maxDuration 
			(max 1.0 (/ (sqrt (log10 bmaxPKenergy)) 
				    ampwidthscale))))

      ;; This could be a real peak, check to see if it's too tall.
	  (if (> bmaxPKenergy TallPkLim)
	      (setq nTall (1+ nTall)	;Count too tall peaks
		    bkgrndsw (if (= bkgrndsw -1) -1
			       0)    ;No bkgnd update for too tall pks
		    stblock ENTstblock		  ;Start of copy
		    nbackstep 0			  ;No backstepping needed
		    ptmp (tcr-copy-block-to-buffer)
		    bkgrndsw (if (= bkgrndsw -1) -1
			       1)    ;Assume bkgnd update next time
		    tcr-dopossiblePK ptmp)

	    ;; Not too tall, see if parabola fit and other measures are OK
	    (setq ParabParamScale (/ (- maxPKenergy medianamp) ParabRefAmp))
	    (if (or (< AveUpslope (* ParabSlopeThresh ParabParamScale))
		    (< (abs AveDownslope) (* ParabSlopeThresh ParabParamScale))
		    (> AveCurvature 0)
		    (< (abs AveCurvature) (* ParabCurveThresh ParabParamScale))
		    (< (abs MaxCurvature) (* ParabCurveMin ParabParamScale))
		    (> (abs MaxCurvature) (* ParabCurveMax ParabParamScale))
		    (< (abs (- maxPKenergyLoc endblock))
		       MinPKmax-EdgeDist)
		    (< (abs (- maxPKenergyLoc stblock))
		       MinPKmax-EdgeDist)
		    )			;End (or) property checks
		(setq nDiscrd (1+ nDiscrd) ;Bump unparabolic pk count
		      bkgrndsw (if (= bkgrndsw -1) -1
				 0)    ;Don't do  bkgnd update for funny pks
		      stblock ENTstblock ;Start of copy
		      nbackstep 0	 ;No backstepping needed
		      ptmp (tcr-copy-block-to-buffer)
		      bkgrndsw (if (= bkgrndsw -1) -1
			       1)    ;Assume bkgnd update next time
		      tcr-dopossiblePK ptmp)

	      ;;Height and parabola params OK, see if very small, or
	      ;;too boxy, if so ignore it
	      (if (and (> (- maxPKenergy medianamp)
			  (* BigPKfactor (- PKthresh medianamp)))
		       (< (/ PKnetenergy PKboxenergy) excessPKareafraction))

;;		     (>= (/ PKnetenergy PKboxenergy) expctdPKareafraction)))

		  ;; Real peak here, convert energy to account for 2 sec
		  ;; sample time
		  (setq TtotPKenergy (* SampleInterval LtotPKenergy)
			nPK (1+ nPK)	;Update # of peaks found
			bkgrndsw (if (= bkgrndsw -1) -1
				   1)  ;Default update bkgnd next time
			stblock (1+ leftblock) ;Use new peak start
			yNorm ParabParamScale  ;Just shorten name for now
			tcr-dopossiblePK (tcr-write-PK TtotPKenergy
						       (/ ParabFit-A yNorm)
						       ParabFit-B
						       (/ ParabFit-C yNorm)
						       (/ RMSParabFitErr yNorm)
						       (/ AveUpslope yNorm)
						       (/ AveDownslope yNorm)
						       (/ AveCurvature yNorm)
						       (/ MaxCurvature yNorm)
						       ) ;End write-PK call
			)				 ;End setq
		(setq stblock ENTstblock ;Start of copy
		      nbackstep 0	 ;No backstepping needed
		      tcr-dopossiblePK (tcr-copy-block-to-buffer)))))

	;; Peak too long, see if we need a threshold calculation
	;; reset, i.e., if peak doesn't fill enough space or peak is
	;; mega-long.
	(if (or (> nInThAdj nInThAdjMax)
		(and (<= PKtime (* megaPKfactor maxDuration))
		     (>= (/ PKnetenergy PKboxenergy) expctdPKareafraction)))
	    (setq bkgrndsw (if (= bkgrndsw -1) -1
			 1)		;Yes, still update background
		  nLong (1+ nLong)	;Count another long peak
		  stblock ENTstblock	;Start of copy
		  nbackstep 0		;No backstepping needed
		  tcr-dopossiblePK (tcr-copy-block-to-buffer) ;Just copy it
		  )			;End if (or...) t

	;; Here the peak complex is very wide, but it's more than can
	;; be explained by a single peak. Try to adjust the background
	;; mean & threshold settings based on the histogram of
	;; amplitudes during the long peak complex. Set the new median
	;; to a point of the amplitude histogram determined by
	;; HistogramMedian and the detection threshold to a higher
	;; level determined by Histogramthresh.
	  (setq lmedianamp medianamp	;Save old median just in case
		runamp (set-median-by-histogram ENTstblock ENTendblock)
		runvar (expt (- iPKthresh medianamp) 2)
		runnorm 1.0
		bkgrndsw (if (= bkgrndsw -1) -1
			   1)
		nLong (1+ nLong)      ;Bump long counter
		nThAdj (1+ nThAdj)    ;Bump threshold adjustment count
		nInThAdj (1+ nInThAdj)	;Count loops
		stline ENTstline	;Reset a bunch of stuff
		linecnt ENTstline
		stblock ENTstblock	;Back to start of prev block
		endblock ENTstblock)
	  (if (< medianamp 0) (edebug))
	  (setq tcr-dopossiblePK ENTstblock)
	  )				;End if (long peak chk thresh reset)
	)				;End if (check if peak long)
      )					;End if (check if peak short)
    )))					;End intro and return

(defun set-median-by-histogram (stpt endpt)
"Here we have to adjust the background mean and threshold, which
we do by constructing a logarithmic histogram of the amplitudes
in the region (stpt endpt). We then find the new median such that
it is greater than HistogramMedian of the histogram content and
the new threshold such that it is greater than Histogramthresh of
the histogram content. The function update the global variables
medianamp and PKthresh"

(let (idx logidx evalue nAmps meanCnt meanHt threshCnt threshHt
	  runCnt AmpHist HistLen (HistMaxDba 150) (HistDecimalPts 1)
	  HistResFactor HistResNorm)
  (save-excursion
    (setq HistResFactor (expt 10 HistDecimalPts)
	  HistResNorm (float (* 10 HistResFactor))
	  HistLen (* HistMaxDba HistResFactor)
	  AmpHist (make-vector HistLen 0)
	  idx stpt)

    ;; Loop through the peak amplitudes and tally the count
    (while (< idx endpt)
      (setq evalue (* 10 HistResFactor (log10 (aref aRawData idx)))
	    logidx (floor evalue))
      (aset AmpHist logidx (1+ (aref AmpHist logidx)))
      (setq idx (1+ idx))
      )					;End while

    ;; Now we have to find the point in the histogram where the new
    ;; median and threshold are set.
    (setq idx 0
	  runCnt 0
	  nAmps (- endpt stpt)		;Init some stuff
	  meanCnt (* HistogramMedian nAmps)	;Count to median
	  threshCnt (* HistogramThresh nAmps)	;Count to threshold
	  meanHt -1 
	  threshHt -1)					;End setq

    ;; Now scan the amplitude histogram to find the amplitude levels
    ;; for the new background median and threshold
    (while (and (< (setq idx (1+ idx)) HistLen)
		(or (= meanHt -1)
		    (= threshHt -1)))
      (setq runCnt (+ runCnt (aref AmpHist idx)))
      (if (and (= meanHt -1)		;Look for median level
	       (>= runCnt meanCnt))
	  (setq meanHt idx))
      (if (and (= threshHt -1)		;Look for thresh level
	       (>= runCnt threshCnt))
	  (setq threshHt idx))
      )					;End while

    ;; All done, return the values
    (setq medianamp (expt 10 (/ (float meanHt) HistResNorm))
	  iPKthresh (expt 10 (/ (float threshHt) HistResNorm))
	  PKthresh (max (* ithrshoffset medianamp)
			iPKthresh)
	  set-median-by-histogram medianamp)
    )))					;End defun


(defun tcr-copy-block-to-buffer ()
"Here we have what was a possible peak but it did not meet size
constraints. Copy the relevant data to the output buffers. If
bkgrndsw = 0, don't update the background stats and just output
the previous values. If bkgrndsw = 1, update the background stats
during the output. If bkgrndsw = -1, use current values in
aBkgmean and aBkgThresh."
(let (zenergyvalue)
  (save-excursion
    ;; Loop through the short/long peak
    (setq locptr stblock)
    (while (< locptr endblock)
      (setq zenergyvalue (aref aRawData locptr))
      (tcr-copy-non-pk-line-to-buffer locptr zenergyvalue) ;Output line of data
      (setq locptr (1+ locptr)))	      ;Move to next line

    ;; End non-peak block, prepare to check for possible new peak
    ;; block.
    (setq nInThAdj 0				;In case this was a loop
	  tcr-copy-block-to-buffer endblock))))	;Note where we start next line


(defun tcr-dobkgndregion ()
  "Start of a background block. Update background statistics as
needed and copy the block of data into the output buffers. Enter
with pointers set to 1st line of the background block (<
PKthresh)"
(let (yenergyvalue)
  (save-excursion
    (if (/= bkgrndsw -1)(setq bkgrndsw  1)) ;Update bkgnd stats appropriately
    (setq endblock (1- endblock)) ;And back up to get the 1st pt of blk

    ;; Loop until end of data or possible start of a peak
    (while (and (< (setq endblock (1+ endblock)) LineTotal)
		(< (setq yenergyvalue (aref aRawData endblock))
		   PKthresh))
      (tcr-copy-non-pk-line-to-buffer endblock yenergyvalue) ;Output data pt
      (setq linecnt (1+ linecnt)))

    ;; End non-peak block, prepare to check for possible new peak block.
    (setq nInThAdj 0			 ;In case this was a loop
	  tcr-dobkgndregion endblock)))) ;Note where we start next block

(defun tcr-copy-non-pk-line-to-buffer (arrptr xenergyvalue)
  "Update the background mean and threshold stats and copy the
related raw data into the output peak buffers"
  (save-excursion
    (aset aPKSEL arrptr xenergyvalue)	;Write the peak analysis data
    (aset aPKMax arrptr 0)
    (aset aPKNetHt arrptr 0)
    (aset aPKWidth arrptr 0)
    (aset aPKMarker arrptr 0)

    ;; Update background threshold for detecting peaks if appropriate
    (if (= bkgrndsw 0) nil	;Don't update background stats if switch off
      (if (= bkgrndsw -1)
	  (if (= nInThAdj 0)		;Updata bkgnd unless using histogram
	      (setq medianamp (aref aBkgMean arrptr)
		    PKthresh (aref aBkgThresh arrptr))
	    (aset aBkgMean arrptr medianamp) ;If histogram, use that value
	    (aset aBkgThresh arrptr PKthresh)
	    )				;End if nInThAdj

	;; Here we need to update the background parameters
	(setq runnorm (+ cmpavewgt (* avewgt runnorm)) ;Update normalization
	      runamp (+ (* cmpavewgt xenergyvalue) 
			(* avewgt runamp)) ;Update amp average
	      medianamp (/ runamp runnorm) ;Update smoothed bkgnd
	      runvar (+ (* cmpavewgt (expt (- xenergyvalue medianamp) 2))
			(* avewgt runvar))
	      errbar (* sdevthrshfact (sqrt (/ runvar runnorm)))
	      iPKthresh (+ medianamp errbar)
	      PKthresh (max (* ithrshoffset medianamp)
			    iPKthresh))
	)				;End if bkgrndsw = -1
      )					;End if bkgrndsw = 0

    ;; Write background analysis data
    (if (= bkgrndsw -1) nil
;;      (if (< medianamp 0) (edebug))
      (aset aBkgMean arrptr medianamp)
      (aset aBkgThresh arrptr PKthresh))

    ;; Note that we were successful
    (setq tcr-copy-non-pk-line-to-buffer t)))

(defun tcr-write-PK (atotPKenergy qParabFit-A qParabFit-B qParabFit-C qRMSParabFitErr qAveUpslope qAveDownslope qAveCurvature qMaxCurvature)
  "write a real peak into the output buffers"
    (save-excursion
    (setq locptr 0			   ;Init loop counter
	  PK1sthalf (floor (/ PKlength 2)) ;Divide up peak range for output
	  PK2ndhalf (1- (- PKlength PK1sthalf)))

    ;; Now do the first half of the buffer
    (while (< locptr PK1sthalf)
      (setq idx (+ locptr stblock))
      (if (= bkgrndsw -1) nil
;;	(if (< medianamp 0) (edebug))
	(aset aBkgMean idx medianamp)
	(aset aBkgThresh idx PKthresh))
      (aset aPKSEL idx (aref aBkgMean idx))	;Write the bkgnd median
      (aset aPKMax idx 0)
      (aset aPKNetHt idx 0)
      (aset aPKWidth idx 0)
      (aset aPKMarker idx 0)
      (aset aParabFit-A idx 0)
      (aset aParabFit-B idx 0)
      (aset aParabFit-C idx 0)
      (aset aRMSParabFitErr idx 0)
      (aset aAveUpslope idx 0)
      (aset aAveDownslope idx 0)
      (aset aAveCurvature idx 0)
      (aset aMaxCurvature idx 0)
      (setq locptr (1+ locptr)))    ;Finish 1st half

    ;; Then output peak data on middle line and mark it in next column
    (setq idx (+ locptr stblock))
    (if (= bkgrndsw -1) nil
;;      (if (< medianamp 0) (edebug))
      (aset aBkgMean idx medianamp)
      (aset aBkgThresh idx PKthresh))
    (aset aPKSEL idx (- atotPKenergy (* (aref aBkgMean idx) PKlength))) ;Write the peak param summary
    (aset aPKMax idx bmaxPKenergy)
    (aset aPKNetHt idx (- bmaxPKenergy (aref aBkgMean idx)))
    (if (< (aref aPKNetHt idx) 0) (edebug))
    (aset aPKWidth idx PKtime)
    (aset aPKMarker idx 1)
    (aset aParabFit-A idx qParabFit-A)
    (aset aParabFit-B idx qParabFit-B)
    (aset aParabFit-C idx qParabFit-C)
    (aset aRMSParabFitErr idx qRMSParabFitErr)
    (aset aAveUpslope idx qAveUpslope)
    (aset aAveDownslope idx qAveDownslope)
    (aset aAveCurvature idx qAveCurvature)
    (aset aMaxCurvature idx qMaxCurvature)

    ;; Then do the 2nd half
    (setq locptr 0)
    (while (< locptr PK2ndhalf)
      (setq idx (1+ idx))
      (if (= bkgrndsw -1) nil
;;	(if (< medianamp 0) (edebug))
	(aset aBkgMean idx medianamp)
	(aset aBkgThresh idx PKthresh))
      (aset aPKSEL idx (aref aBkgMean idx))	;Write the bkgnd median
      (aset aPKMax idx 0)
      (aset aPKNetHt idx 0)
      (aset aPKWidth idx 0)
      (aset aPKMarker idx 0)
      (aset aParabFit-A idx 0)
      (aset aParabFit-B idx 0)
      (aset aParabFit-C idx 0)
      (aset aRMSParabFitErr idx 0)
      (aset aAveUpslope idx 0)
      (aset aAveDownslope idx 0)
      (aset aAveCurvature idx 0)
      (aset aMaxCurvature idx 0)
      (setq locptr (1+ locptr)))	;Finish 2nd half

    (setq nInThAdj 0			;In case we got here in a loop
	  tcr-write-PK endblock)))	;Show that we made it
