# Model noise vs distance

library(lme4)
library(rstanarm)
library(tidyverse)

source('ExploreReedMichaelM.R')

d = nearby %>% rename(Type=`AC Type`, Level=Value, Altitude=Alt) %>% 
  select(-time_str, -tip) %>% 
  mutate(Distance=ifelse(Notes=='at home', Altitude, sqrt(Altitude^2+4400^2)))
d$Location=factor(d$Notes, levels=c('at home', 'Park/Franklin'),
                         labels=c('Overhead', 'Nearby'))

source('ExploreReedDataWing.R')
wing = bind_rows(mar18_flights_m, mar21_flights) %>% 
  filter(Notes %in% c('at house', 'Locust/MVP')) %>% 
  rename(Type=`AC Type`, Level=Value) %>% 
  select(-time_str, -tip) %>% 
  mutate(Distance=ifelse(Notes=='at house', Altitude, sqrt(Altitude^2+3800^2)))

d2 = bind_rows(d, wing)

# Data from Wing @ Locust/MVP is anomalous - many readings are as loud as Wing @ at house
ggplot(d2, aes(Distance, Level)) + geom_point(aes(color=Notes))+stat_smooth(method=lm)

small = d %>% count(Type) %>% arrange(desc(n)) %>% filter(n<3)
d$Type_ = ifelse(d$Type %in% small$Type, 'Other', d$Type)
  
show_prediction = function(model, predictor=predict) {
  dp = d
  dp$pred = predictor(model)
  call = if (isS4(model)) model@call else model$call
  ggplot(dp, aes(log10(Distance), Level, color=Type_)) + geom_point(size=1) + 
    geom_line(aes(y=pred), linetype=2) + guides(color=FALSE) +
    labs(y='Sound level (dBA)',
         title='Sound level vs Distance to aircraft',
         subtitle=call) + facet_wrap(~Type_)
}

# Linear model with fixed slope and intercept (complete pooling)
model_lm_ff = lm(Level ~ log10(Distance) + Distance, data=d)
summary(model_lm_ff)
show_prediction(model_lm_ff)

# Linear model with fixed slope, varying intercept (no pooling)
model_lm_fv = lm(Level ~ log10(Distance) + Distance + Type_ - 1, 
            data=d)
summary(model_lm_fv)
show_prediction(model_lm_fv)

model_lmer = lmer(Level ~ log10(Distance) + Distance + (1|Type), data=d)
summary(model_lmer)
show_prediction(model_lmer)

options(mc.cores = parallel::detectCores())
model_stan_lmer = stan_lmer(Level ~ log10(Distance) + Distance + (1|Type), 
                            data=d, prior=normal(location=c(-20, 0), scale=c(0.1, 1)))
summary(model_stan_lmer)
#show_prediction(model_stan_lmer, posterior_predict)

library(shinystan)
launch_shinystan(model_stan_lmer)
