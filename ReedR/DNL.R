# How many flights would it take to reach a given DNL?

secs = 24 * 60 * 60 # seconds in a day
N = 300 # Number of flights
SEL = 80 # Sound exposure level for a single flight
10 * log10(N * 10^(SEL/10) / secs) # DNL

# What is N given DNL?
DNL = 55
secs * 10^ ((DNL-SEL)/10) # N
