library(ggplot2)
library(lubridate)
library(scales)
library(xts)

files = list.files('~/Google Drive/Fair Skies/Reed/2016-03-12 dBC/', full.names=TRUE)
r = lapply(files, read.delim, stringsAsFactors=FALSE)
r = do.call(rbind, r)
r$time = ymd_hms(paste(r$Date, r$Time), tz="America/New_York")
p = qplot(time, Value, data=r, geom='line')
p + scale_x_datetime(labels = date_format("%b %d %H:%M", tz="America/New_York"), 
                     breaks=date_breaks('4 hour'), minor_breaks = date_breaks("1 hour"),
                     limits=date_breaks('1 hour')(range(r$time))[c(1, 18)])


library(dygraphs)
xt = xts(r$Value, order.by=r$time)
dygraph(xt) %>% dyRangeSelector()

# After sourcing NoiseFuncs.R
wensnPath = "~/Google Drive/Raspberry Pi/logs/"
w = read_daily_log(wensnPath, '2016-03-12')

r$tc = as.character(r$time)
w$tc = as.character(w$time)
xt2 = merge(r[,c('tc', 'Value')], w[,c('tc', 'avg')], all=FALSE)
names(xt2) = c('time', 'dBC', 'dBA')

# Figure out the offset between the two series
lags=ccf(xt2$dBC, xt2$dBA, lag.max=5)
offset=lags$lag[which.max(lags$acf)]
ccf(tail(xt2$dBC, -offset), head(xt2$dBA, -offset), lag.max=5)

# Correct by the offset
xt2$dBC = c(tail(xt2$dBC, -offset), rep(NA, offset))
wxt = xts(xt2[,2:3], order.by=as.POSIXct(xt2$time))
dygraph(wxt) %>% dyRangeSelector()

# Smooth with triangle filter
triangle = c(1:6, 5:1)
triangle = triangle/sum(triangle)
width = length(triangle)
smoothed = rollapply(wxt, width, function(x) sum(x*triangle), align='center')
smoothed$Difference = smoothed$dBC-smoothed$dBA

# The WENSN has a floor about 48 dB which distorts the difference. Restrict
# the data to the time period where it is above the floor.
smoothed = smoothed['2016-03-12 06:30::2016-03-12 23:00']
dygraph(smoothed, main='Comparison of "A" and "C" weighted sound level measurements, 2016-03-12', xlab='Time', ylab='Sound level (dBA or dBC) and difference') %>% dyRangeSelector()

# Mean-difference plots
theme_set(theme_bw())
qplot((dBA+dBC)/2, Difference, data=smoothed, size=I(0.5), alpha=I(0.1)) + stat_smooth(se=FALSE) + labs(x='Average sound level (dBA+dBC)/2', y='Difference (dBC-dBA)', title='Difference between "A" and "C" weighted sound levels')

qplot(dBC, Difference, data=smoothed, size=I(0.5), alpha=I(0.1)) + stat_smooth(se=FALSE) + labs(x='dBC sound level', y='Difference (dBC-dBA)', title='Difference between "A" and "C" weighted sound levels')
