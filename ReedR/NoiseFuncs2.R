# Noise analysis functions - not done
library(tidyverse)
library(lubridate)

# Read Reed data file, return a data.frame
read_reed = function(path)
{
  r = read_tsv(path, col_types='cccnc')
  r$time = ymd_hms(paste(r$Date, r$Time), tz="America/New_York")
  r %>% select(-Place, -Unit) %>% filter(!is.na(time)) # Take out rows from extra headers
}

# http://stackoverflow.com/a/6836924/80626
# This finds too many peaks on raw data
localMaxima <- function(x) {
  y <- diff(c(-Inf, x)) > 0L
  rle(y)$lengths
  y <- cumsum(rle(y)$lengths)
  y <- y[seq.int(1L, length(y), 2L)]
  if (x[[1]] == x[[2]]) {
    y <- y[-1]
  }
  y
}

# Find the indices of local maxima in each segment >= lowlimit, where the peak is >= hilimit
# lowlimit should be lower than the artificial dips within peaks, but high enough to
# separate actual peaks. Only peaks >= hilimit are returned.
# Returns a data frame of
# ix - index in original data of the peak
# value - height of the peak
# start - index of the start of the peak
# end - index of the end of the peak
findPeaksAbove <- function(x, lowlimit, hilimit) {
  # First find the peaks
  value = data_frame()
  runs = rle(x>=lowlimit)
  start_ix = 1
  for (i in seq_along(runs$lengths)) {
    run_len = runs$lengths[i]
    if (runs$values[i]) {
      end_ix = start_ix + run_len -1
      run_max = max(x[start_ix:end_ix])
      run_max_ix = which.max(x[start_ix:end_ix]) + start_ix - 1
      value = rbind(value, 
                    data_frame(ix=run_max_ix, start=start_ix, end=end_ix, value=run_max))
    }
    start_ix = start_ix + run_len
  }
  value %>% filter(value>=hilimit)
}

# Filter a time series the way Rene Vega does
filter_vega = function(n)
{
  n = filter_spikes(n)
  splmean = find_initial_mean(n)
}

# Filter spikes > 95 dB that are also 20 dB above their two
# neighbors. Replace them with the previous measurement.
filter_spikes = function(n)
{
  # Where are the peaks?
  peaks_ix = which(n>95)
  peaks_ix = peaks[peaks_ix>1 & peaks_ix<length(n)]
  peaks = n[peaks_ix]
  
  # Previous an next values
  prev = n[peaks_ix-1]
  nxt = n[peaks_ix+1]
  
  # Location of single-value peaks
  spikes = peaks_ix[peaks>prev+20 & peaks>nxt+20]
  n[spikes] = n[spikes-1]
  n
}

# Find the initial starting point for the exponential filter
find_initial_mean = function(n)
{
  splmean = from_dB(n[1])
  for (i in 2:min(300, length(n)))
  {
    spl = from_dB(n[i])
		spldiff = spl - splmean
		if (spl <= splmean) splwt = 0.5
		else splwt = 0.5 / 4^abs(spldiff)
		splmean = splmean * (1.0 - splwt) + (spl * splwt)
  }
  splmean
}

# Smooth n with a triangle filter of the given width
smooth = function(n, width=9)
{
  # Make a kernel
  if (width %% 2 == 0) width = width + 1
  half_width = width %/% 2
  triangle = c(1:(half_width+1), half_width:1)
  triangle = triangle/sum(triangle)
  
  # Pad the data at front and back, filter
  t = c(rep(n[1], half_width), n, rep(tail(n, 1), half_width))
  t = stats::filter(t, triangle, method='convolution')
  
  # Remove the padding
  t %>% head(-half_width) %>% tail(-half_width)
}

from_dB = function(x) 10^(x/10)
to_dB = function(x) 10*log10(x)
