library(tidyverse)
library(ggiraph)
library(kjutil)
library(lubridate)
library(readxl)
library(scales)
library(xts)

theme_reports()

source('NoiseFuncs2.R')

# Data from Anita Gryan, Burch St, Arlington
files = list.files('~/Google Drive/Fair Skies/Reed Anita Burch/', pattern='SLA*', full.names=TRUE)
r = lapply(files, read_reed)
r = bind_rows(r)

#unique(r$Date)
 # [1] "2016/12/03" "2016/12/04" "2016/12/09" "2016/12/28" "2016/12/30" "2017/01/01" "2017/01/02"
 # [8] "2017/01/15" "2017/01/16" "2017/01/19" "2017/01/21" "2017/01/27"

# library(dygraphs)
# xt = xts(r$Value, order.by=r$time)
# dygraph(xt['2016-12-03']) %>% dyRangeSelector()
# dygraph(xt['2016-12-04']) %>% dyRangeSelector()
# dygraph(xt['2016-12-09']) %>% dyRangeSelector()
# 
# dygraph(xt['2016-12-28 15/2016-12-29']) %>% dyRangeSelector() # Afternoon is good ~ 43 N70 in 5 hours
# dygraph(xt['2017-01-01']) %>% dyRangeSelector() # 49 N70 over 7 hours
# dygraph(xt['2017-01-15']) %>% dyRangeSelector() # 20 N70 in 6 hours
# dygraph(xt['2017-01-19']) %>% dyRangeSelector() # 4 hours
# dygraph(xt['2017-01-27 14/2017-01-28']) %>% dyRangeSelector() # 54 N70 in 8 hours

# Offset needed to synchronize the noise data to to the flight data
offset = -(4*60+11)

jan27_data = r %>% 
  filter(time>=as.POSIXct('2017-01-27 13:40:00'), time<as.POSIXct('2017-01-27 21:10:00')) %>% 
  mutate(time=time+offset, time_str=as.character(time))

jan27_peaks = findPeaksAbove(jan27_data$Value, 60, 65) %>% 
  mutate(time=jan27_data$time[ix]) %>% rename(Value=value)
jan27_peaks$interval = interval(jan27_data$time[jan27_peaks$start], jan27_data$time[jan27_peaks$end])

# Plot the 3/21 data with flight number annotations and local maxima
jan27_flights = 
  read_excel('~/Google Drive/Fair Skies/Reed Anita Burch/Planes 2017 January 27.xlsx',
             sheet=2) %>% 
  `[`(-1, 1:5) %>% 
  mutate(Time=Time %>% gsub('1899-12-30', '01-27-17', .)) %>% 
  mutate(Time = mdy_hms(Time, tz="America/New_York"),
         time_str=as.character(Time)) %>% 
  filter(Track!='Winch/Everett', Time > mdy_hms('01-27-2017 12:00:00')) %>% 
  rename(Altitude=`Alt.`, Flight=`Flight #`)

# Fix the time zone
#jan27_flights$Time = force_tz(jan27_flights$Time, "America/New_York")

# First row is empty; only five columns have data
#jan27_flights = jan27_flights[-(1),1:5] %>% mutate(time_str=as.character(Time))

# Merge in noise levels so we can draw nice lines
jan27_flights = merge(jan27_flights, jan27_data[,c('time_str', 'Value')], 
                      by.x='time_str', by.y='time_str', all.x=TRUE)
jan27_flights$Value[is.na(jan27_flights$Value)] = 60

# Look up flights in the peaks array to try to get better values
jan27_flights = jan27_flights %>% rowwise %>% 
  mutate(Value = max(Value, jan27_peaks$Value[Time %within% jan27_peaks$interval][1], na.rm=TRUE))

# Make tooltips
jan27_flights$tip = paste0(jan27_flights$Flight, '<br>', 
                           format(jan27_flights$Time, '%l:%M %p', tz="America/New_York"), '<br>',
                           jan27_flights$`AC Type`, '<br>',
                           jan27_flights$Value, ' dB<br>',
                           jan27_flights$Altitude, 'ft')

# Take out flights that are just visual noise
#jan27_flights = jan27_flights %>% filter(!(Track %in% c('Winch/93/South', 'Winch/93N', '??Fresh Pond', 'Camb/Central Sq', 'not from R33L')))
jan27_flights = jan27_flights %>% filter(Value>=65) %>% droplevels

p = ggplot(jan27_data, aes(time, Value)) + 
  geom_segment(aes(x=Time, xend=Time, y=Value, yend=75), 
               data=jan27_flights, color='blue', alpha=0.3, size=0.3) +
  geom_line(color='grey40', size=0.3) + 
  geom_point(data=jan27_peaks, color='red', size=0.5) +
  labs(x='Time', y='Measured sound level (dBA)', 
       title='Sound levels, Burch St, Arlington, 2017-01-27') +
  scale_x_datetime(date_breaks='30 min', labels=date_format('%l:%M %p', tz="America/New_York"))

p = p + geom_text_interactive(aes(x=Time, y=75, label=Flight, tooltip=tip, data_id=Time), 
            data=jan27_flights, angle=90, size=2, hjust=-0.1) +
  geom_segment(aes(x=x, xend=xend, y=y, yend=yend), 
               data=data.frame(x=as.POSIXct("2017-01-27 13:32:00 EDT"),
               xend=as.POSIXct("2017-01-27 16:02:00 EDT"),
               y=60, yend=60), 
            color='red', inherit.aes = FALSE,
            arrow=arrow(length=unit(0.03, 'npc'), angle=90, ends='both')) +
  geom_label(aes(label=label), 
             data=data.frame(time=as.POSIXct("2017-01-27 14:47:00 EDT"), 
                             Value=60, label='Video'), color='red')

tooltip_css <- "background-color:#ffcccc;font-size:12px;font-family: sans-serif;padding:10px;border-radius:5px;line-height:100%"
gg_jan27 = ggiraph(code = {print(p)}, width=1, width_svg=10, zoom_max=5,
        hover_css = "fill:#FF4C3B;",
        tooltip_offy=-10,
        tooltip_extra_css=tooltip_css, tooltip_opacity=1)

