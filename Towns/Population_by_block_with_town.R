# Census data for Arlington and Belmond

library(tidyverse)
library(tidycensus)
library(sf)

census_api_key('223cfc10cecf0faf2d4e5085dd6d977498e6ceac')
v10 <- load_variables(2010, "sf1", cache = TRUE)
df = get_decennial(geography='block', 
                   variables=c('H001001', 'P001001'),
                   state='MA',
                   county='Middlesex',
                   geometry=TRUE)

df = df %>% spread('variable', 'value')
dfc = st_centroid(df)

# Get municipal boundaries
towns = st_read(here::here('Towns/CENSUS2010TOWNS_SHP/CENSUS2010TOWNS_POLY.shp')) %>% 
  st_transform(st_crs(dfc))

dfc = st_join(dfc, towns %>% select(TOWN2)) 
st_write(dfc %>% select(-NAME), here::here('Towns/Population_by_block_with_town.csv'), layer_options='GEOMETRY=AS_XY')
