# Calculations for Stage 3 noise levels
library(tidyverse)

base_calc = function(min_wt, min_noise, max_wt, max_noise, decrease) {
  function(wt) {
    case_when(
      wt <= min_wt ~ min_noise,
      wt >= max_wt ~ max_noise,
      TRUE ~ min_noise + decrease * log2(wt/min_wt)
    )
  }
}

takeoff = function(wt, engines) {
  case_when(
    engines > 3 ~ base_calc(44673, 89, 850000, 106, 4)(wt),
    engines == 3 ~ base_calc(63177, 89, 850000, 104, 4)(wt),
    engines < 3 ~ base_calc(106250, 89, 850000, 101, 4)(wt)
  )
}
sideline = base_calc(77200, 94, 882000, 103, 2.56)
approach = base_calc(77200, 98, 617300, 105, 2.33)

# FAR 36 Appendix C:
# (3) Stage 3 noise limits are as follows:
# (i) For takeoff.
# (A) For airplanes with more than 3 engines. 106 EPNdB for maximum weights of
# 850,000 pounds or more, reduced by 4 EPNdB perhalving of the 850,000 pounds
# maximum weight down to 89 EPNdB for maximum weights of 44,673 pounds or less;
# (B) For airplanes with 3 engines—104 EPNdB for maximum weights of 850,000
# pounds or more, reduced by 4 EPNdB per halving of the 850,000 pounds maximum
# weight down to 89 EPNdB for maximum weights of 63,177 pounds and less; and
# (C) For airplanes with fewer than 3 engines— 101 EPNdB for maximum weights of
# 850,000 pounds or more, reduced by 4 EPNdB per halving of the 850,000 pounds
# maximum weight down to 89 EPNdB for maximum weights of 106,250 pounds and
# less.
# (ii) For sideline, regardless of the number of engines. 103 EPNdB for maximum
# weights of 882,000 pounds or more, reduced by 2.56 EPNdB per halving of the
# 882,000 pounds maximum weight down to 94 EPNdB for maximum weights of 77,200
# pounds or less.
# (iii) For approach, regardless of the number of engines—105 EPNdB for maximum
# weights of 617,300 pounds or more, reduced by 2.33 EPNdB per halving of the
# 617,300 pounds weight down to 98 EPNdB for maximum weights of 77,200 pounds or
# less.
