# Read waypoints file, parse and save in DB

library(tidyverse)
library(sf)
source('DB.R')

w = read_csv('MassachusettsWaypoints.csv', comment='#', skip=1,
             col_names=c('Name', 'Desc'), col_types='c-c') 

# Split out degrees minutes seconds
dms = w$Desc %>% 
  str_match('(\\d{2})-(\\d{2})-(\\d{2}.\\d{4})N (\\d{3})-(\\d{2})-(\\d{2}.\\d{4})W')

to_decimal = function(d) {
  as.numeric(d[,1]) + as.numeric(d[,2])/60 + as.numeric(d[,3])/3600
}

w$lat = to_decimal(dms[,2:4])
w$lon = -to_decimal(dms[,5:7])

ways = w %>% select(name=Name, lon, lat) %>% st_as_sf(coords=c('lon', 'lat'), crs=4326)
with_db_connection(list(con=connect()), st_write_db(con, ways, table=c('tracks', 'waypoints'), geom_name='location'))
