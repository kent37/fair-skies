-- PostgreSQL / PostGIS notes

create database tracks;
\c tracks
create schema tracks;
create schema staging;
alter database tracks set search_path to public, tracks;
create extension postgis;

-- Create the main tracks table
CREATE TABLE tracks.tracks
(
    id serial PRIMARY KEY,
    flight text COLLATE pg_catalog."default",
    hex text COLLATE pg_catalog."default",
    squawk integer,
    mlat integer,
    delta double precision,
    "time" timestamp with time zone,
    date date,
    month double precision,
    hour integer,
    operation text COLLATE pg_catalog."default",
    track geometry(LineStringZM,4326)
)

-- st_write_db doesn't like to make the name of the geometry
-- column the same as the name of the source geometry
-- so rename it afterwards
alter table staging.tracks rename column wkb_geometry to track;

-- Copy from staging to main table
INSERT INTO tracks.tracks
    (flight, hex, squawk, mlat, delta, "time", date, month, hour, operation, track)
  SELECT 
    flight, hex, squawk, mlat, delta, "time", date, month, hour, operation, track
  FROM staging.tracks;

-- Remove the staging table
DROP TABLE staging.tracks;

-- Create a view for 33L departures
CREATE VIEW tracks.dep_33l AS
select * from tracks.tracks 
where operation='departure' 
and ST_Intersects(track, ST_GeomFromEWKT('SRID=4326;POLYGON ((-71.1153027777778 42.3960694444444, -71.0819694444444 42.3960694444444, -71.0819694444444 42.4294027777778, -71.1153027777778 42.4294027777778, -71.1153027777778 42.3960694444444))'));

-- FOIA version
CREATE VIEW tracks.dep_33l_faa AS
select * from tracks.foia 
where operation='departure' 
and ST_Intersects(track, ST_GeomFromEWKT('SRID=4326;POLYGON ((-71.1153027777778 42.3960694444444, -71.0819694444444 42.3960694444444, -71.0819694444444 42.4294027777778, -71.1153027777778 42.4294027777778, -71.1153027777778 42.3960694444444))'));

-- Create an geometry index for tracks
CREATE INDEX idx_tracks_track ON tracks.tracks USING gist(track);
CREATE INDEX idx_tracks_month ON tracks.tracks (month);
CREATE INDEX idx_tracks_year ON tracks.tracks (year);

-- OpenJump doesn't plot XYZM data
-- OpenJump WMS layers are in EPSG:3857
-- This reads tracks, strips ZM and converts to 3857
select st_transform(st_force_2d(track), 3857) from tracks.dep_33l

-- FOIA table
CREATE TABLE tracks.foia
(
    id serial PRIMARY KEY,
    flight text COLLATE pg_catalog."default",
    squawk text COLLATE pg_catalog."default",
    delta double precision,
    "time" timestamp with time zone,
    date date,
    year double precision,
    month double precision,
    hour integer,
    operation text COLLATE pg_catalog."default",
    equip text COLLATE pg_catalog."default",
    track geometry(LineStringZM,4326)
)

-- Create a geometry index for foia
CREATE INDEX idx_foia_track ON tracks.foia USING gist(track);
CREATE INDEX idx_foia_month ON tracks.foia (month);
CREATE INDEX idx_foia_year ON tracks.foia (year);

-- Gates table
CREATE TABLE tracks.gates
(
    id serial PRIMARY KEY,
    name text COLLATE pg_catalog."default",
    gate geometry(LineString,4326)
)

-- Get TEKKK tracks for a specific day
-- Including month and year conditions allows index search
select * from foia where 
  operation='departure' 
  and st_intersects(track, (select track from gates where name='TEKKK')) 
  and month=4 
  and extract(year from date)=2016 
  and date='2016-04-01' and operation='departure'
  