---
title: "Boston Logan 2015 Runway Usage"
author: "Kent S Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.height=15, fig.width=10, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

This document shows all 2015 arrivals and departures at Boston Logan Airport. Operations are grouped by wind speed and direction so the plots show the predominant wind patterns for each runway and operation type. The size and intensity of the dots reflects the number of flights at each point of the compass and wind speed.

```{r}
library(dplyr)
library(formattable)
library(ggplot2)
library(lubridate)
#library(pander)
library(readr)
library(stringr)
library(tidyr)
#library(viridis)

#panderOptions('table.split.table', Inf)

# Read CountOps data
# Time is local time, verified by comparing with PublicVue for 1/1/2015
d = read_csv('CountOps_flts_BOS_CY15.csv', col_types='ccccccc') %>% 
  mutate(date=ymd(YYYYMMDD), month=month(date), hour=as.integer(str_sub(HHMM, 1, 2)))

# There are many duplicate entries, take them out
d = d[!duplicated(d),]

# All arrivals and departures by hour
by_hour = d %>% filter(`Operation Type` %in% c('Arrival', 'Departure'), !is.na(Runway)) %>% group_by(date, hour, Runway, `Operation Type`) %>% summarize(by_hour=n()) %>% filter(by_hour>0)

# Get wind data
knots_per_ms = 1.94384 # 1 meter/second in knots
weather = read_csv('HourlyWeather/29794_Logan_International_Airport__Hourly_2015.csv') %>% 
  transmute(date=ymd(paste(`Year Local`, `Month Local`, `Day Local`, sep='-')), 
            hour=`Hour Local`, wind=`Wind Speed (m/s)` * knots_per_ms, 
            direction = `Wind Direction (degrees)`) %>% 
  filter(!is.na(direction), !is.na(wind))

# Merge, make a merged operation column, consolidate 
by_hour_merged = left_join(by_hour, weather) %>% 
  mutate(operation = paste(Runway, `Operation Type`)) %>% 
  group_by(Runway, `Operation Type`, operation, wind, direction) %>% 
  summarize(by_hour=sum(by_hour)) %>% 
  mutate(lt10=wind<=10)

# Not every hour has a wind entry, creating missing values
# Fill them in with defaults
missing = is.na(by_hour_merged$wind)
by_hour_merged$wind[missing] = 0
by_hour_merged$direction[missing] = 0
by_hour_merged$lt10[missing] = TRUE

# Filter out entries with less than one operation per day
#by_runway = by_hour_merged %>% group_by(operation) %>% summarize(count=n()) %>% filter(count>365)

#by_hour_merged %>% filter(operation %in% by_runway$operation) -> by_hour_merged

# Runway directions
variation = -14 # degrees from true north
runways = data_frame(operation=unique(by_hour_merged$operation)) %>% 
  mutate(number=as.numeric(str_extract(operation, '\\d+')),
         deg=number*10+variation)
```

The plots show operations on all runways. 
Operations are colored red where the wind speed is greater 
than 10 knots. At these higher wind speeds the airport has less discretion about runway use. Wind speeds less than 10 knots are green.

Runway direction is shown as a blue line in each plot.

```{r}
#high_wind = by_hour_merged %>% filter(wind>10)
max_wind = max(by_hour_merged$wind)
ggplot(by_hour_merged) + 
#  geom_point(aes(color=wind), alpha=0.3) + 
  geom_point(aes(direction, wind, size=sqrt(by_hour), color=lt10), alpha=0.15) + 
  geom_segment(aes(x=deg, y=0, xend=deg, yend=max_wind), 
               data=runways, color='blue', alpha=0.5) +
  coord_polar() + 
  facet_wrap(~operation, ncol=4) +
  scale_x_continuous(breaks=c(0, 90, 180, 270), limits=c(0, 360), labels=NULL) +
  scale_y_continuous(limits=c(0, NA)) +
  scale_size_area(guide=FALSE) + theme_bw() + 
  scale_color_manual(values=c('red', 'green'), labels=c('>10', '<=10'), 
                     name='Wind speed') +
  #scale_color_viridis('Knots', option='A', direction=-1) +
  labs(x='', y='Wind speed (knots)', 
       title='2015 Operations by Runway, Wind direction and Wind speed\nHigh wind operations') + 
  theme(strip.text = element_text(size=10), panel.margin.x=unit(0, 'pt'),
        panel.margin.y=unit(0, 'pt'))
```

This table shows the number of operations per runway end as a count and as a percent of total operations.

```{r results='asis'}

stats = by_hour_merged %>% ungroup %>% 
  group_by(Runway, `Operation Type`) %>% 
  summarize(count=sum(by_hour), lt10=sum(by_hour*lt10), gt10=count-lt10) %>% # Get totals
  gather('count_type', 'count', count, lt10, gt10) %>% # Gather, paste, spread
  mutate(heading=paste(`Operation Type`, count_type, sep='_')) %>% 
  select(-`Operation Type`, -count_type) %>% 
  spread(heading, count) %>% ungroup
  
stats = stats %>% rbind(c(0, colSums(stats[,-1]))) %>% 
  mutate(  # Add percents. We have a total row so adjust the sums by 2
    arr_pct = percent(Arrival_count/sum(Arrival_count)*2, digits=1),
    arr_lt10_pct = percent(Arrival_lt10/sum(Arrival_lt10)*2, digits=1),
    arr_gt10_pct = percent(Arrival_gt10/sum(Arrival_gt10)*2, digits=1),
    arr_delta = percent(arr_gt10_pct-arr_lt10_pct, digits=1),
    dep_pct = percent(Departure_count/sum(Departure_count)*2, digits=1),
    dep_lt10_pct = percent(Departure_lt10/sum(Departure_lt10)*2, digits=1),
    dep_gt10_pct = percent(Departure_gt10/sum(Departure_gt10)*2, digits=1),
    dep_delta = percent(dep_gt10_pct-dep_lt10_pct, digits=1)
  ) %>% 
  transmute( # Rename and re-order
    Runway=Runway,
    'Arrivals'=Arrival_count,
    'Arrival pct'=arr_pct,
    'Arrivals <10 knots'=Arrival_lt10,
    'Arrivals >10 knots'=Arrival_gt10,
    'Arrivals <10 pct'=arr_lt10_pct,
    'Arrivals >10 pct'=arr_gt10_pct,
    'Arrivals delta pct'=arr_delta,
    'Departures'=Departure_count,
    'Departure pct'=dep_pct,
    'Departures <10 knots'=Departure_lt10,
    'Departures >10 knots'=Departure_gt10,
    'Departures <10 pct'=dep_lt10_pct,
    'Departures >10 pct'=dep_gt10_pct,
    'Departures delta pct'=dep_delta
  )

stats$Runway[nrow(stats)] = 'Total'
non_totals = nrow(stats) - 1
cat('<small>\n')
formattable(stats, list(
#            area(1:non_totals, `Arrivals <10 pct`) ~ proportion_bar('yellow', x~percent(x)),
#            `Arrivals >10 pct` = color_bar('yellow'),
            `Arrivals delta pct` = formatter("span",
                style = x ~ ifelse(x>0.01, 'color:lime', 
                                   ifelse(x< -0.01, "color:red", 'color:black'))),
            # `Departures <10 pct` = color_bar('yellow'),
            # `Departures >10 pct` = color_bar('yellow'),
            `Departures delta pct` = formatter("span",
                style = x ~ ifelse(x>0.01, 'color:lime', 
                                   ifelse(x< -0.01, "color:red", 'color:black')))
            ))
cat('</small>\n')
#pander(stats, justify='right')
```

**Updated** 10/21/2016 to correct small (3-4%) errors in the operations counts.

Runway use data is from the FAA via FOIA request. Weather data from WeatherSpark. Visualization by Kent S Johnson.

<small>Copyright 2016 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>2016-10-21</span></small>
