dep2015 = use %>% filter(Year==2015, Type=='dep') %>% group_by(RWbase, deg, `Wind Speed`) %>% summarise(count=sum(Count)) %>% ungroup %>% filter(count>100)

arr2015 = use %>% filter(Year==2015, Type=='arr') %>% group_by(RWbase, deg, `Wind Speed`) %>% summarise(count=sum(Count)) %>% ungroup %>% mutate(deg=180+deg) %>% filter(count>100)

count_max = max(dep2015$count, arr2015$count)

dep2015 = dep2015 %>% mutate(scaled=500*count/count_max)
arr2015 = arr2015 %>% mutate(scaled=500*count/count_max)

dep = offset_convert(dep2015, 5, 20) %>% segs_to_lines
arr = offset_convert(arr2015, -5, 20) %>% segs_to_lines
dep_end = dep %>% filter(end=='end', y>50)
arr_end = arr %>% filter(end=='end', y>100)

xlim = c(min(0, dep$x, arr$x), max(360, dep$x, arr$x))
p = ggplot(dep, aes(x, y, group=group)) + geom_line(color='red') + geom_line(data=arr, color='green') + coord_radar() + scale_y_continuous(limits=c(0, NA)) + scale_x_continuous(limits=xlim, breaks=seq(45, 360, 45))

p = p + geom_point(data=subset(dep, end=='end'), shape=20, color='red') + geom_point(data=subset(arr, end=='start'), shape=20, color='green')


p + geom_label(data=cbind(dep_end %>% select(-x, -y), offset(dep_end$x, dep_end$y, xoff=40, yoff=0)), aes(label=RWbase), size=3, label.size=0.05, color='red') + 
  geom_label(data=cbind(arr_end %>% select(-x, -y), offset(arr_end$x, arr_end$y, xoff=-40, yoff=0)), aes(label=RWbase), size=3, label.size=0.05, color='green')
