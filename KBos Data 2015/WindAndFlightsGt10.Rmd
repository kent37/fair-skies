---
title: "Logan Airport Wind Direction and Runway Use, Strong Winds Only"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

This report shows wind direction and speed and the number of jet arrivals and departures on each runway at Boston Logan Airport in 2015. This report shows only strong winds (&ge; 10 knots).

Wind data is downloaded from [https://weatherspark.com](WeatherSpark). Runway use data is from [http://www.massport.com/environment/environmental-reporting/noise-abatement/runway-use/](Massport Runway Use) reports.

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)

# How are day of week, wind speed and number of flights related?
library(descr)
library(dplyr)
library(ggplot2)
library(lubridate)
library(readr)
library(readxl)
library(tidyr)
theme_set(theme_bw())

ten_knots = 5.14444 # meters / second
variation = -14 # degrees from true north

flight_counts = read_excel('33L Dept Daily Counts.xlsx', sheet=2)

weather = read_csv('HourlyWeather/29794_Logan_International_Airport__Hourly_2015.csv') %>%
  mutate(wday=wday(ISOdate(`Year Local`, `Month Local`, `Day Local`), label=TRUE)) %>% 
  mutate(Month = factor(`Month Local`, levels=1:12, 
                        labels=c('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
                        ordered=TRUE)
        ) %>% filter(`Wind Speed (m/s)` >= ten_knots)

# Runway use
use = read_excel('YearlyRunwayUse.xlsx') %>% 
  gather(Month_type, Count, Jan_arr:Dec_dep) %>% 
  separate(Month_type, c('Month', 'Type')) %>% 
  mutate(
    Runway =factor(Runway),
    Month = factor(Month, levels=c('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'), ordered=TRUE),
    Year = factor(Year, ordered=TRUE),
    RWbase = as.numeric(sub('R|L', '', Runway)),
    deg = RWbase*10+variation)

dep2015 = use %>% filter(Year==2015, Type=='dep') %>% group_by(RWbase, deg) %>% summarise(count=sum(Count)) %>% ungroup %>%  mutate(scaled=200*count/max(count)) %>% filter(count>100)

dep2015_by_month = use %>% filter(Year==2015, Type=='dep') %>% group_by(Month, RWbase, deg) %>% summarise(count=sum(Count)) %>% ungroup %>%  mutate(scaled=40*count/max(count)) %>% filter(count>100)

arr2015 = use %>% filter(Year==2015, Type=='arr') %>% group_by(RWbase, deg) %>% summarise(count=sum(Count)) %>% ungroup %>%  mutate(scaled=200*count/max(count), arr_deg = (deg + 180) %% 360) %>% filter(count>100)

arr2015_by_month = use %>% filter(Year==2015, Type=='arr') %>% group_by(Month, RWbase, deg) %>% summarise(count=sum(Count)) %>% ungroup %>%  mutate(scaled=40*count/max(count), arr_deg = (deg + 180) %% 360) %>% filter(count>100)
```

### Yearly wind and runway use for 2015

The first four graphs cover all of 2015, with yearly and monthly totals. 
The  bars show the cumulative number of hours of wind &ge; 10 knots from each direction, in 10° increments. You can clearly see that for the whole year, the wind is predominantly from the southwest and west-northwest. The monthly graphs show the yearly variation with strong north-west winds in January-March.

The red lines show the directions of the principal arrival departure runways. The length of the line is proportional to the number of flights using that runway. Departure runways are drawn leaving the center; arrival runways are drawn towards the center.

#### Departures

```{r}
# Base plot for yearly plots
p = ggplot(weather, aes(`Wind Direction (degrees)`)) + 
  geom_histogram(binwidth=10, alpha=0.8, fill='#377eb8') + 
  coord_polar(start=5*pi/180) +
  scale_x_continuous(breaks=seq(45, 360, 45), limits=c(5, 365)) +
  labs(y='Number of Hours')

# Departures
p + geom_segment(aes(x=deg, y=0, xend=deg, yend=scaled), data=dep2015, color='red') +
  geom_label(aes(x=deg, y=scaled+10, label=RWbase), data=dep2015, color='red', show.legend=FALSE) +
  ggtitle(expression(atop('Strong wind direction at Logan Airport 2015',
                     atop(italic('Red lines show relative departures by runway')))))

# By month
p + geom_segment(aes(x=deg, y=0, xend=deg, yend=scaled), data=dep2015_by_month, color='red') +
  ggtitle(expression(atop('Strong wind direction at Logan Airport 2015, by month',
                     atop(italic('Red lines show relative departures by runway'))))) + facet_wrap(~Month)
```

#### Arrivals

```{r}
# Arrivals
p + geom_segment(aes(x=arr_deg, y=0, xend=arr_deg, yend=scaled), data=arr2015, color='red') +
  geom_label(aes(x=arr_deg, y=scaled+10, label=RWbase), data=arr2015, color='red', show.legend=FALSE) + 
  ggtitle(expression(atop('Strong wind direction at Logan Airport 2015',
                     atop(italic('Red lines show relative arrivals by runway')))))

p + geom_segment(aes(x=arr_deg, y=0, xend=arr_deg, yend=scaled), data=arr2015_by_month, color='red') +
  ggtitle(expression(atop('Strong wind direction at Logan Airport 2015, by month',
                     atop(italic('Red lines show relative arrivals by runway'))))) + facet_wrap(~Month)
```

Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>
