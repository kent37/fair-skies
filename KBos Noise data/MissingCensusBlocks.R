# Find census blocks which are missing from the 33L EA (and thus from the EDRs)
library(tidyverse)
library(mapview)
library(readxl)
library(sf)
library(sp)
library(tidycensus)
library(tigris)

options(tigris_use_cache=TRUE)

# Town outlines
tn = read_sf('/Users/kent/Downloads/townssurvey_shp/TOWNSSURVEY_POLYM.shp') %>% 
  st_transform(crs=4326)

# 33L data
path = '/Users/kent/Downloads/data from all files TO SHARE.xlsx'
d = read_xlsx(path, sheet="33l ea all census blocks")
sf_33l = st_as_sf(d, coords = c('LONG', 'LAT'), crs=4326)

# Find towns containing 33L data
tn_33l = tn[st_contains(tn, sf_33l) %>% map_lgl(~length(.) > 0),]
tn_33l_outline = tn_33l %>% st_union

# Get the census blocks
all_blocks = blocks(state='MA', county=c('Middlesex', 'Essex', 'Suffolk', 'Norfolk', 'Plymouth'), year=2010)

# Mansfield and Easton are in the 33L area but not in these five counties; just skip them
tn_33l = tn_33l %>% filter(FIPS_STCO!=25005)
tn_33l_outline = tn_33l %>% st_union
# We need population data to go with the blocks
# Total population, total population in households
# See https://www.census.gov/prod/cen2010/doc/sf1.pdf
pop = get_decennial(geography='block', 
                    variables=c('P0010001', 'P0160001', 'PLACE'), 
                    state='MA', 
                    county=c('Middlesex', 'Essex', 'Suffolk', 'Norfolk', 'Plymouth'), 
                    geometry=FALSE, output='wide')

blocks_pop = all_blocks %>% st_as_sf %>% 
  left_join(pop, by=c(GEOID10='GEOID')) %>% 
  st_transform(crs=4326)

# Find the blocks that are not in the 33L data
# It looks like this is just blocks outside some DNL threshold
# They are all in the 128-495 ring
blocks_missing = blocks_pop %>% filter(!(GEOID10 %in% sf_33l$POINT_ID), P0010001>0)
blocks_missing_in_towns = st_contains(tn_33l_outline, blocks_missing)
mapview(blocks_missing[blocks_missing_in_towns[[1]],])

# Try again with 2014 EDR data (which is rectangular, unlike the 2015 data)
by_year = readRDS('NoiseByYear.RDS')
edr14 = by_year[['2014']] %>% mutate(POINT_ID = as.character(POINT_ID)) %>% 
  st_as_sf(coords = c('LONG', 'LAT'), crs=4326)

st_as_sfc.bbox = function(bb) {
  box = st_polygon(list(matrix(bb[c(1, 2, 3, 2, 3, 4, 1, 4, 1, 2)], ncol=2, byrow=TRUE)))
  st_sfc(box, crs=st_crs(bb))
}

b14 = st_as_sfc(st_bbox(edr14))

# This finds blocks completely contained in b14
blocks_in_14 = st_contains(b14, blocks_pop)
blocks_missing = blocks_pop[blocks_in_14[[1]],] %>% 
  filter(!(GEOID10 %in% edr14$POINT_ID), P0010001>0)

# This finds blocks that intersect b14, a larger set
# It gets a lot of edge blocks that we don't want
blocks_in_14 = st_intersects(blocks_pop, b14, sparse=FALSE)
blocks_missing = blocks_pop[blocks_in_14[,1],] %>% 
  filter(!(GEOID10 %in% edr14$POINT_ID), P0010001>0)

mapview(blocks_missing, zcol='P0010001')
sum(blocks_missing$P0010001) # 26578 !
sum(blocks_missing$P0160001) # 0

