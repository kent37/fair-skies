---
title: "Missing Census Blocks - 2014 EDR"
author: "Kent S Johnson"
date: '`r Sys.Date()`'
output: kjutil::small_format
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, comment=NA, warning=FALSE, message=FALSE)

library(tidyverse)
library(leaflet)
library(sf)
library(tidycensus)
library(tigris)

options(tigris_use_cache=TRUE)
```

<style>
table {
  width: auto !important;
}
</style>

```{r cache=TRUE}
# Town outlines
tn = read_sf('/Users/kent/Downloads/townssurvey_shp/TOWNSSURVEY_POLYM.shp') %>% 
  st_transform(crs=4326)

# Get the census blocks
all_blocks = blocks(state='MA', county=c('Middlesex', 'Essex', 'Suffolk', 'Norfolk', 'Plymouth'), year=2010)

# We need population data to go with the blocks
# Total population, total population in households
# See https://www.census.gov/prod/cen2010/doc/sf1.pdf
pop = get_decennial(geography='block', 
                    variables=c('P0010001', 'P0160001', 'PLACE'), 
                    state='MA', 
                    county=c('Middlesex', 'Essex', 'Suffolk', 'Norfolk', 'Plymouth'), 
                    geometry=FALSE, output='wide')

blocks_pop = all_blocks %>% st_as_sf %>% 
  left_join(pop, by=c(GEOID10='GEOID')) %>% 
  st_transform(crs=4326)
```

```{r}
by_year = readRDS('NoiseByYear.RDS')
edr14 = by_year[['2014']] %>% mutate(POINT_ID = as.character(POINT_ID)) %>% 
  st_as_sf(coords = c('LONG', 'LAT'), crs=4326)
rm(by_year)

st_as_sfc.bbox = function(bb) {
  box = st_polygon(list(matrix(bb[c(1, 2, 3, 2, 3, 4, 1, 4, 1, 2)], ncol=2, byrow=TRUE)))
  st_sfc(box, crs=st_crs(bb))
}

b14 = st_as_sfc(st_bbox(edr14))

# This finds blocks completely contained in b14
blocks_in_14 = st_contains(b14, blocks_pop)
blocks_missing = blocks_pop[blocks_in_14[[1]],] %>% 
  filter(!(GEOID10 %in% edr14$POINT_ID), P0010001>0)
```

This map shows census blocks within the bounds of the 
[Massport 2014 Environmental Data Report](https://www.massport.com/media/342451/2014_EDR_Web_Version_file1.pdf) (PDF)
which are not included in the noise analysis of the report. 

Mouse over a block to see the population of the block.

```{r map}
load('NearbyTowns.RData') # Contains nearby_towns
places = read_delim('st25_ma_places.txt', delim='|',
  col_names=c('STATE', 'STATEFP', 'PLACEFP', 'PLACENAME', 
              'CDP', 'TYPE', 'FUNCSTAT', 'COUNTY'),
  col_types='cnnccccc') %>% 
  mutate(place=sub(' city| CDP| town', '', PLACENAME, ignore.case=TRUE)) %>% 
  select(PLACEFP, place) %>% 
  distinct()

blocks_missing = left_join(blocks_missing, places, by=c(PLACE='PLACEFP'))
labels = paste0(blocks_missing$place,
                ' - ', blocks_missing$P0010001)
pal = colorQuantile('viridis', blocks_missing$P0010001)
towns_in_14 = nearby_towns %>% st_as_sf %>% st_intersection(b14)

leaflet() %>% 
  setView(-71.128184, 42.3769824, zoom = 11)  %>% 
  addTiles(urlTemplate='http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
           	attribution = '&copy; Kent S Johnson &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
           options=tileOptions(maxZoom=19, subdomains='abcd')) %>% 
  addPolylines(data=towns_in_14, color='darkred', weight=2) %>% 
  addPolygons(data=b14, color='darkred', weight=2, fill=FALSE) %>% 
  addPolygons(data=blocks_missing, color='black', fillColor=~pal(P0010001),
              label=labels,
              weight=1, dashArray=1,           # Line weight
              opacity=0.7, fillOpacity=0.5    # Transparency
 )
```

This table shows the number of census blocks and the 
population count omitted from each municipality. 
A total of `r scales::comma(sum(blocks_missing$P0010001))` people were omitted.

```{r}
blocks_missing[, c('place', 'P0010001')] %>% as_data_frame %>% group_by(place) %>% 
  summarize(Blocks=n(), Total=sum(P0010001)) %>% 
  arrange(-Total) %>% 
  mutate(Total=scales::comma(Total)) %>% 
  rename(Place=place, 'Census Blocks'=Blocks, 'Omitted Population'=Total) %>% 
  knitr::kable(align='lrr')

```

Census block outlines from US Census TIGER files. Massport noise modeling data
from the 
[BLANS Document Library](http://www.bostonoverflight.com/phase3_documents.aspx).

<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>