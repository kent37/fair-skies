# Has the number of 33L departures increasesed significantly?

library(tidyverse)
library(lubridate)
library(readxl)

adsb_flights = 
  read_csv('~/Dev/Fair Skies-TEKKK-ADSB/all_flights.csv') %>% 
  with_tz('US/Eastern')

# Daily totals are a bit too much
adsb_by_date = adsb_flights %>% 
  mutate(date=date(time)) %>% 
  group_by(date) %>% 
  summarize(count=n())

faa = read_csv('~/Dev/Fair Skies-TEKKK/all_flights.csv')%>% 
  with_tz('US/Eastern') %>% 
  mutate(date=date(time)) %>% 
  group_by(date) %>% 
  summarize(count=n())

# October 2016 data is bad
#faa$Total[faa$Month=='2016-10-01'] = NA

co_path = '~/Dev/CountOps 2010-2019/'
co_files = list.files(co_path, full.names=TRUE)
co_raw = map_dfr(co_files, read_csv)

co_data = co_raw %>%
  filter(operation_type=='Departure',
         Runway=='33L', Equipment!='HELO') %>%
  mutate(date=ymd(LCL_YYYYMMDD)) %>% 
  group_by(date) %>% 
  summarize(count=n())

all_by_date = bind_rows(faa %>% mutate(Source='FAA tracks'),
                    #co_data %>% mutate(Source='CountOps'),
                    adsb_by_date %>% mutate(Source='ADS-B')) %>% 
  filter(year(date) %in% 2014:2019)

ggplot(all_by_date, aes(date, count, color=Source)) +
  geom_line() +
  facet_wrap(~Source, ncol=1)


# Number of days per month with >= n_flights flights
n_flights = 125
manipulate::manipulate({
over_n = all_by_date %>% 
  mutate(month=floor_date(date, unit='months')) %>% 
  group_by(Source, month) %>% 
  summarize(over_n=sum(count>=n_flights)) %>% 
  ungroup()

ggplot(over_n, aes(month, over_n)) + 
  geom_line(aes(color=Source)) +
  scale_color_brewer(palette='Dark2') +
  # scale_y_continuous(breaks=seq(0, 10, 2), minor_breaks=NULL,
  #                    limits=c(0, 12)) +
  labs(x='', 
       y=str_glue('Number of days with {n_flights} 33L departures'),
       title='Busy departure days on Logan 33L',
       subtitle=str_glue('Monthly count of days with {n_flights} or more 33L departures'),
       caption='Data: FAA FOIA request, ADS-B collection. Chart © 2020 Kent Johnson') +
  silgelib::theme_plex()  +
    theme(axis.line=element_line(),
        legend.position='bottom',
        legend.margin=margin(-10, 0, 0, 0))
}, n_flights=manipulate::slider(25, 400, 200, step=25, label='Monthly flights'))

# Compare with Massport monthly numbers
mp33l = read_excel(here::here('KBos Data 2015/YearlyRunwayUse.xlsx')) %>% 
  gather(Month_type, Count, Jan_arr:Dec_dep) %>% 
  separate(Month_type, c('Month', 'Type')) %>% 
  filter(Runway=='33L', Type=='dep', Year>=2016) %>% 
  mutate(date=dmy(paste('1', Month, Year)))

ggplot(mp33l, aes(date, Count)) +
  geom_line()
