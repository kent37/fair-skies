# Read and prepare CountOps data

library(tidyverse)
library(lubridate)
co_path = '~/Dev/CountOps 2010-2019/'

#' Read original CountOps files, do some basic cleaning and 
#' summarizing, and save the results to files.
prepare = function() {
  source(here::here('AltitudeAtTEKKK/EquipmentTypes.R'))
  
  co_files = list.files(co_path, 'CountOps_flts_BOS', full.names=TRUE)
  
  # Read files, basic munging
  co_raw = vroom::vroom(co_files) %>% 
    filter(LOCID=='BOS', operation_type != 'Overflight') %>% 
    mutate(Date=ymd(LCL_YYYYMMDD)) %>% 
    rename(Hour=HR_LCL, Operation=operation_type, 
           Count=operations) %>% 
    select(-LCL_YYYYMMDD, -LOCID)
  
  # Add columns for equipment categories
  countops_cooked = co_raw %>% 
    mutate(Category=equip_category(Equipment),
           Jet=!Category %in% c('GA', 'Helo', 'Other', 'Regional prop'))
  
  vroom::vroom_write(countops_cooked, 
                     file.path(co_path, 'countops.tsv'))
  
  # Hourly summary
  countops_hourly = countops_cooked %>% 
    group_by(Date, Hour, Runway, Operation, Category, Jet) %>% 
    summarize(Count=sum(Count)) %>% 
    mutate(Month=floor_date(Date, unit='months'))
  
  vroom::vroom_write(countops_hourly, 
                     file.path(co_path, 'countops_hourly.tsv'))
  
  # Hourly jets only
  countops_hourly_jets = countops_hourly %>% 
    filter(Jet) %>% 
    group_by(Date, Month, Hour, Runway, Operation) %>% 
    summarize(Count=sum(Count))
  
  vroom::vroom_write(countops_hourly_jets, 
                     file.path(co_path, 'countops_hourly_jets.tsv'))
}

#' Read the entire cleaned CountOps data.
#' Returns a data.frame with columns
#' - Hour - Hour of the day, 0-23
#' - Runway
#' - Operation - Arrival, Departure or Overflight
#' - Equipment - CountOps code for the type of aircraft
#' - Count - Number of operations
#' - Date
#' - Category - Wide body, single aisle, etc
#' - Jet - true / false
read_countops = function() {
  vroom::vroom(file.path(co_path, 'countops.tsv'))
}

#' CountOps data summarized by hour, runway and equipment category.
#' Returns a data.frame with columns
#' - Date
#' - Hour - Hour of the day, 0-23
#' - Runway
#' - Operation - Arrival, Departure or Overflight
#' - Category - Wide body, single aisle, etc
#' - Count - Number of operations
#' - Month - Date of the first of the month containing the event
#' - Jet - true / false
read_countops_hourly = function() {
  vroom::vroom(file.path(co_path, 'countops_hourly.tsv'))
}

#' CountOps data, jets only, summarized by hour and runway.
#' Returns a data.frame with columns
#' - Date
#' - Month - Date of the first of the month containing the event
#' - Hour - Hour of the day, 0-23
#' - Runway
#' - Operation - Arrival, Departure or Overflight
#' - Count - Number of operations
read_countops_hourly_jets = function() {
  vroom::vroom(file.path(co_path, 'countops_hourly_jets.tsv'))
}
