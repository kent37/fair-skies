# Busy mornings in October 2019
library(tidyverse)
library(lubridate)
library(readxl)
library(sf)
source(here::here('PostGIS/DB.R'))

adsb = read_csv('~/Dev/Fair Skies-TEKKK-ADSB/all_flights.csv') %>% 
  with_tz('US/Eastern')%>% 
  filter(month(time)==10, year(time)==2019) %>% 
  group_by(day(time)) %>% 
  summarize(Total=n()) %>% 
  arrange(desc(Total))

# October 18, 2019 had 399 flights
# How many by hour?
oct_18 = read_csv('~/Dev/Fair Skies-TEKKK-ADSB/all_flights.csv') %>% 
  with_tz('US/Eastern')%>% 
  filter(date(time)=='2019-10-18') %>% 
  group_by(hour) %>% 
  summarize(Total=n())
# Are there days that had many 33L departures and also others?
aspm = read_excel('~/Downloads/October2019-BOSFlightsbyDaybyHour.xlsx', skip=3)

aspm %>% group_by(Date) %>% 
  summarize(Departures=sum(Departures))

# 10/18 looks like a good day to try
con = connect()
tracks = st_read(con, query=
  "select * from tracks.tracks 
    where date='2019-10-18' and operation='departure'")

mapview::mapview(tracks, alpha=0.1, lwd=1)

tracks %>% as.data.frame() %>% count(hour, sort=TRUE)

morning = tracks %>% filter(hour %in% 8:9) %>% 
  mutate(minute=time - ymd_hm('2019-10-18 8:00', 
                              tz='US/Eastern'))
# %>% 
#   mutate(minute=as.double(minute, units='mins'))
mapview::mapview(morning, 
                 alpha=0.3, lwd=2, label=morning$flight)

