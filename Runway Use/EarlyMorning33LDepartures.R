# Look for busy hours when 33L / 27 are being used for departures
library(tidyverse)
source(here::here('Runway Use/CountOps.R'))

# Total arrivals by month
co_hourly = read_countops_hourly()
co_hourly_jets = read_countops_hourly_jets()

# This consolidates 27 and 33L departure runways in use per hour
hourly = co_hourly_jets %>% 
  filter(Operation == 'Departure',
         !is.na(Runway),
         Runway %in% c('27', '33L')) %>% 
  group_by(Date, Hour, Runway) %>% 
  summarize(Count=sum(Count)) %>% 
  ungroup() %>% 
  pivot_wider(names_from='Runway', values_from='Count', 
              values_fill=list(Count=0)) %>% 
  mutate(Total=`27` + `33L`)

range(hourly$Total)
qplot(hourly$Total)
ggplot(hourly, aes(Date, Total)) +
  geom_point(size=0.5, alpha=0.5) +
  stat_smooth(se=FALSE)

to_plot = hourly %>% 
  select(-`27`, -Total) %>% 
  rename(Total=`33L`) %>% 
  filter(between(Hour, 5, 7)) %>% 
  mutate(Hour=paste0(Hour, '-', Hour+1, 'am'))

to_plot = to_plot %>% bind_rows(
  to_plot %>% 
    group_by(Date) %>% 
    summarize(Total=sum(Total)) %>% 
    mutate(Hour='5-8am')
)

to_plot %>% 
  ggplot(aes(Date, Total)) +
  geom_point(size=0.5) +
  facet_wrap(~Hour, ncol=1, 
             scales='free_y') +
  scale_x_date(date_breaks='1 year', minor_breaks=NULL,
               date_labels='%Y') +
  silgelib::theme_plex() +
  labs(title='Logan Runway 33L hourly departures, 5am-8am',
       x='', y='Departures per hour',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')

# Just the daily total
to_plot %>% 
  filter(Hour=='5-8am') %>% 
  ggplot(aes(Date, Total)) +
  geom_point(aes(color=Total)) +
  scale_color_gradientn(colors=pals::kovesi.rainbow(100), guide='none') +
  scale_x_date(date_breaks='1 year', minor_breaks=NULL,
               date_labels='%Y') +
  silgelib::theme_plex() +
  labs(title='Logan Runway 33L total departures, 5am-8am',
       x='', y='Departure count',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')

# How many days per month with more than 10 flights?
morning_33 = co_hourly_jets %>% 
  filter(Operation == 'Departure',
         !is.na(Runway),
         Runway == '33L',
         between(Hour, 5, 7))

morning_33 %>% 
  group_by(Month, Date) %>% 
  summarize(Count=sum(Count)) %>% 
  summarize(Over10=sum(Count>10)) %>% 
  ggplot(aes(Month, Over10)) +
  stat_smooth(method=lm, se=FALSE, color='grey') +
  geom_point(aes(color=Over10)) +
  scale_color_gradientn(colors=pals::kovesi.rainbow(100),
                        guide='none') +
  scale_x_date(date_breaks='1 year', minor_breaks=NULL,
               date_labels='%Y') +
  silgelib::theme_plex() +
  labs(title='Logan Runway 33L early morning departures have increased',
       subtitle='Days per month with over ten 33L departures between 5-8am',
       x='', y='Days with over 10 early am departures',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')
  
# What does this look like on a monthly basis?
monthly = morning_33 %>% 
  group_by(Month) %>% 
  summarize(Count=sum(Count)) %>% 
  ungroup()

monthly %>% 
  ggplot(aes(Month, Count)) +
  geom_point(aes(color=Count)) +
  scale_color_gradientn(colors=pals::kovesi.rainbow(100)) +
  scale_x_date(date_breaks='1 year', minor_breaks=NULL,
               date_labels='%Y') +
  silgelib::theme_plex() +
  labs(title='Logan Runway 33L monthly departures, 5am-8am',
       x='', y='Departures per month',
       color='Departures',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')

# What about all departures?
morning_all = co_hourly_jets %>% 
  filter(Operation == 'Departure',
         between(Hour, 5, 7))

morning_all %>% 
  group_by(Date) %>% 
  summarize(Count=sum(Count)) %>% 
  filter(Count<150) %>% # 2015-04-01 is nuts
  ggplot(aes(Date, Count)) +
  geom_point(aes(color=Count), size=1) +
  stat_smooth(method=lm, color='grey50', se=FALSE) +
  scale_color_gradientn(colors=pals::kovesi.rainbow(100), guide='none') +
  scale_x_date(date_breaks='1 year', minor_breaks=NULL,
               date_labels='%Y') +
  silgelib::theme_plex() +
  labs(title='Logan early morning departures have steadily increased since 2012',
       subtitle='Total daily departures between 5-8am',
       x='', y='Departure count',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')

# 2015-04-01 was insane
crazy_day = co_hourly_jets %>% 
  filter(Operation=='Departure', Date=='2015-04-01') %>% 
  group_by(Hour) %>% 
  summarize(Count=sum(Count))

sum(crazy_day$Count >=50) #11
ggplot(crazy_day, aes(Hour, Count,)) + 
  geom_point(color='darkred', size=2) +
  scale_x_continuous(minor_breaks=NULL) +
  silgelib::theme_plex() +
  labs(title='944 departures from Logan Airport on April 1, 2015',
       subtitle='Hourly departures as high as 80',
       x='Hour', y='Departure count',
       caption='Data from FAA FOIA request\nChart © 2020 Kent Johnson')
