library(dplyr)
library(ggplot2)
library(lubridate)

load('BelmontNoiseData.RData')

d = as.tbl(belmont_noise_data)
d = d %>% mutate( 
  hour=hour(`Start Time`), 
  sec = as.integer(`Start Time`) %% 86400,
  date=as.Date(`Start Time`))

events = as.tbl(belmont_events)
events = mutate(events, date=as.Date(EVENTLMAXTIME), sec = as.integer(EVENTLMAXTIME) %% 86400)
event_counts = events  %>% group_by(date) %>% summarise(count=n())
labels = as.list(apply(event_counts, 1, function(r) paste0(r[1], ' (', r[2], ' flights)')))
names(labels)=event_counts$date

# Labeller function for facets
date_labeller <- function(variable,value){
  return(labels[as.character(value)])
}

# Set dplyr to print 27 days
options(dplyr.print_max=27)

# What time are the flights?
qplot(d$LAeq)
qplot(hour(events$POCATIME))
night_events = events  %>% filter(hour(POCATIME)< 4)

# Look at two nighttime events on 2/22
d_sample = d %>% filter(date=='2014-02-22', hour==0)
ne_sample = night_events[1:2,]
p = qplot(`Start Time`, LAeq, data=d_sample, size=I(1), alpha=I(0.3))
p + geom_point(aes(x=EVENTLMAXTIME, y=LMAX), data=ne_sample, color='blue') + theme_minimal()

# All of 2/22
p = qplot(`Start Time`, LAeq, data=subset(d, date=='2014-02-22'), size=I(1), alpha=I(0.2))
p = p + theme_minimal()
p + geom_point(aes(x=EVENTLMAXTIME, y=LMAX), 
               data=subset(events, date=='2014-02-22'), color='blue')

# Two days, to get the facetting right
d_sample = d %>% filter(date=='2014-02-22' | date=='2014-02-23')
ev_sample = events %>% filter(date=='2014-02-22' | date=='2014-02-23')
p = qplot(sec/3600, LAeq, data=d_sample, size=I(1), alpha=I(0.2))
p = p + theme_minimal() + facet_grid(date~., labeller=date_labeller) + labs(x='Hour')
p = p + geom_point(aes(y=LMAX), data=ev_sample, color='blue')
p + scale_x_continuous(limits=c(0, 24), breaks=0:24)

# Everything
p = qplot(sec/3600, LAeq, data=d, size=I(1), alpha=I(0.2))
p = p + theme_minimal() + facet_grid(date~., labeller=date_labeller) + labs(x='Hour')
p = p + geom_point(aes(y=LMAX), data=events, color='blue')
p = p + scale_x_continuous(limits=c(0, 24), breaks=0:24)
ggsave(filename='All flights.pdf', plot=p, width=8.5, height=74, units='in', limitsize=FALSE)

# Try to remove some values
# Try simple thinning
# Every other value
d2 = subset(d, sec%%2==0)

# Every fourth value if lower
d2 = subset(d2, sec%%4==0 | LAeq>=50)
d2 = subset(d2, sec%%8==0 | LAeq>=40)

# Look at two days
d_sample = d2 %>% filter(date=='2014-02-22' | date=='2014-03-20')
ev_sample = events %>% filter(date=='2014-02-22' | date=='2014-03-20')
p = qplot(sec/3600, LAeq, data=d_sample, size=I(1), alpha=I(0.2))
p = p + theme_minimal() + facet_grid(date~., labeller=date_labeller) + labs(x='Hour')
p = p + geom_point(aes(y=LMAX), data=ev_sample, color='blue')
p + scale_x_continuous(limits=c(0, 24), breaks=0:24)

# Everything
p = qplot(sec/3600, LAeq, data=d2, size=I(1), alpha=I(0.2))
p = p + theme_minimal() + facet_grid(date~., labeller=date_labeller) + labs(x='Hour')
p = p + geom_point(aes(y=LMAX), data=events, color='blue')
p = p + scale_x_continuous(limits=c(0, 24), breaks=0:24)
ggsave(filename='All flights2.pdf', plot=p, width=8.5, height=74, units='in', limitsize=FALSE)

# What is the duration between flights?
events %>% arrange(EVENTLMAXTIME) %>% group_by(date) %>% 
  summarize(min=min(diff(sec/60)), median=median(diff(sec/60)), count=n()) %>% 
  mutate(min=round(min, 2), median=round(median, 2)) %>% 
  arrange(desc(count)) %>% print(n=27)
