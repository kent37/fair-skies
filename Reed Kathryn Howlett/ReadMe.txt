100 block of Scituate St, Arlington

10/28 11:54 PM - start in yard
10/29 1:39 PM - helicopter
10/29 2:03 PM - IN (moved meter onto covered porch)
10/30 12:32 AM - OUT (moved meter back into yard)
10/30 2:37 PM - IN
10/31 8:21 AM - OUT
10/31 4:43 PM - basketball game next door
11/3 8:43 AM - IN
11/3 11AM - (still IN) helicopter
11/3 ~11:45 AM - (still IN) trash trucks
11/4 ~12:45 PM - OUT (but accidentally knocked plug out of the wall)
11/4 8:30 PM - plugged in and reset
11/5 8:20 AM - leaf blower
11/5 ~12:00 PM - unplugged
11/5 ~4:00 PM - plugged in and reset
11/6 ~5:15 or 6:15 AM - IN
11/6 (still IN) - basketball
11/6 8:53 PM - OUT
11/7 ~7:05 PM - stopped to collect data, then plugged in and reset at 7:15pm

Third batch:
Monday 14th 10:15pm moved meter on to porch due to rain in forecast.
(during some plane activity)
Wednesday the 16th, moved meter back into yard.  Some airplane activity.  Left out in yard until tonight at about 10:45pm.