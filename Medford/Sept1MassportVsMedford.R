library(tidyverse)
library(dygraphs)
library(lubridate)
library(readxl)
library(xts)

# mp = read_excel('NM22 and NS RT compared for Sep 1.xlsx', sheet=1) %>% 
#   mutate(date=ymd_hms(paste0('2017-09-01 ', HH, ':', MM, ':', SS), tz='US/Eastern'))

mp = read_excel('Site 22 Sept 1 2017 data shred with Medford.xlsx', skip=8)
mp$date = mp$event_time
date(mp$date) = '2017-09-01'
mp$date = force_tz(mp$date, 'US/Eastern')

med = read_excel('NM22 and NS RT compared for Sep 1.xlsx', sheet=2) %>% 
  mutate(hh = case_when(
    AMPM == 'PM' ~ HH + 12,
    AMPM == 'AM' & HH==12 ~ 0,
    TRUE ~ HH
  ),
    date=ymd_hms(paste0('2017-09-01 ', hh, ':', MM, ':', SS), tz='US/Eastern'))

adsb = read_csv('2017_Sep_tekkk.csv') %>% 
  mutate(date=ymd_hms(time)) %>% 
  filter(date(date)=='2017-09-01')

xt_mp = xts(mp$event_lmax, order.by=mp$date)
xt_med = xts(med$LMax, order.by=med$date)
xt = merge(xt_med, xt_mp)
dygraph(xt) %>% 
  dySeries('xt_med', color='green', drawPoints=FALSE) %>% 
  dySeries('xt_mp', color='red', drawPoints=TRUE, strokeWidth=0, pointSize=2) %>%  
  dyRangeSelector()

