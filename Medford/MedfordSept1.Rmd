---
title: "Airplane Noise, Medford, Sept 1, 2017"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: 
  kjutil::small_format:
    df_print: kable
---

<style>
table {
  width: auto !important;
}
</style>

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=7, fig.height=6, comment=NA, warning=FALSE, message=FALSE)
options(width=100)

library(tidyverse)
library(ggiraph)
library(lubridate)
library(mapview)
library(readxl)
library(scales)
library(sf)
source(rprojroot::find_rstudio_root_file('PostGIS', 'DB.R'))
source(rprojroot::find_rstudio_root_file('ReedR', 'NoiseFuncs2.R'))
```

### Analysis of Massport noise data for runway 33L departures on Sept 1, 2017.

```{r}
noise = read_excel('Site 22 Sept 1 2017 data shred with Medford.xlsx', skip=8)
date(noise$event_time) = '2017-09-01'
date(noise$flight_time) = '2017-09-01'
all = read_excel('Sept 1 2017 flight data shared with Medford.xlsx')
date(all$Time) = '2017-09-01'

# 33L departures, jet only
dep33l = all %>% filter(Opr=='D', RW=='33L', Prop=='J')

# Add noise measurements to departure data when available. Match by time and flight.
# Fix the date and time
joined = dep33l %>% 
  left_join((noise %>% select(flight_time, flight_id, event_time, event_lmax)),
            by=c(Time='flight_time', `Flight ID`='flight_id'))
tz(joined$Time) = 'US/Eastern'
tz(joined$event_time) = 'US/Eastern'

#sum(!is.na(joined$event_lmax)) # 160

# Estimate event time for flights that do not have it
#qplot(na.omit(joined$event_time-joined$Time)) # event_time is about 2 minutes after flight time
joined$event_time[is.na(joined$event_time)] = joined$Time[is.na(joined$event_time)] + 120

no_noise = joined %>% filter(is.na(event_lmax)) # 270 flights, more than in noise!!
```

Massport reported noise for `r nrow(noise)` flights on Sept. 1 at noise monitor 22 in
Medford. The Massport flight log shows
`r nrow(dep33l)` runway 33L jet departures on the same date. 
`r nrow(dep33l)-nrow(noise)` jet departures from the log are not represented in the Massport 
noise report.

### Missing flights by time and airline

The flights for which noise is not reported are distributed across many airlines and much of the day.

```{r}
qplot(Time, forcats::fct_rev(factor(Airline)), data=no_noise) + 
  labs(title="33L jet departures missing from Massport noise data",
      subtitle="Sept 1, 2017, Medford", y='Airline') + 
  theme_light() + scale_x_datetime(date_labels='%H:%M')
      
```

```{r}
# Read ADSB tracks
tracks = with_db_connection(list(con=connect()),
                            st_read_db(con, query="select * from dep_33L where date='2017-09-01'"))
#no_noise_tracks = tracks %>% filter(flight %in% no_noise$`Flight ID`)
no_noise_tracks = tracks %>% inner_join(no_noise, by=c(flight='Flight ID'))
```

### Tracks of missing flights

The missing flights seem to be predominantly ones which turned south-west from TEKKK.
This map shows ADS-B flight tracks for `r nrow(no_noise_tracks)` flights which are not represented
in the noise data.  

```{r}
mapview(no_noise_tracks, zcol='flight', alpha=0.5, lwd=2,
        popup=popupTable(as_data_frame(no_noise_tracks), c('flight', 'AC Type', 'Time')))
```

### Noise monitor data

Data from a Medford noise monitor at the Andrews Middle School clearly shows many of 
the flights missing from the Massport report. In the figure below, flight numbers shown
in red are not in the Massport report. (Click on the magnifying glass to enable zoom and pan.)

```{r noise_plot,fig.width=9, fig.height=6}
# Match measured noise with Massport runway use data
med = read_excel('NM22 and NS RT compared for Sep 1.xlsx', sheet=2) %>% 
  mutate(hh = case_when(
    AMPM == 'PM' ~ HH + 12,
    AMPM == 'AM' & HH==12 ~ 0,
    TRUE ~ HH
  ),
    date=ymd_hms(paste0('2017-09-01 ', hh, ':', MM, ':', SS), tz='US/Eastern'))

# Focus on 1-4pm
afternoon = med %>% 
  filter(date>=as.POSIXct('2017/09/01 13:00:00'), date<as.POSIXct('2017/09/01 16:00:00')) %>% 
  mutate(time=date, time_str=as.character(time))

peaks = findPeaksAbove(afternoon$LMax, 60, 70) %>% 
  mutate(time=afternoon$time[ix]) %>% rename(Value=value)
peaks$interval = interval(afternoon$time[peaks$start], afternoon$time[peaks$end])

flights = joined %>% 
  filter(Time>=as.POSIXct('2017/09/01 13:00:00'), Time<as.POSIXct('2017/09/01 16:00:00'))

# ggplot(afternoon) + geom_line(aes(date, LMax)) + geom_point(data=peaks, aes(time, Value), color='red') + geom_point(data=flights, aes(Time+120, 80), color=c('blue', 'red')[is.na(flights$event_lmax)+1])
# 
# ggplot(afternoon) + geom_line(aes(date, LMax)) + geom_point(data=peaks, aes(time, Value), color='red') + geom_point(data=flights, aes(event_time, 80), color='green')

# Merge in noise levels so we can draw nice lines
flights = merge(flights, afternoon[,c('time', 'LMax')], 
                      by.x='event_time', by.y='time', all.x=TRUE)
flights$LMax[is.na(flights$LMax)] = 60

# Look up flights in the peaks array to try to get better values
flights = flights %>% rowwise %>% 
  mutate(LMax = max(LMax, peaks$Value[Time %within% peaks$interval][1], na.rm=TRUE))

# Make tooltips
flights$tip = paste0(flights$`Flight ID`, '<br>', 
                           format(flights$Time, '%l:%M %p', tz="America/New_York"), '<br>',
                           flights$`AC Type`, '<br>',
                           round(flights$LMax, 1), ' dB')

p = ggplot(afternoon) + 
  geom_segment(aes(x=Time, xend=Time, y=LMax, yend=80), 
               data=flights, color='blue', alpha=0.3, size=0.3) +
  geom_line(aes(time, LMax), color='grey40') + 
#  geom_point(data=peaks, aes(time, Value), color='red', size=0.5) +
  labs(x='Time', y='Measured sound level (dBA)', 
       title='Sound levels and flights, Andrews Middle School, Medford 2017-09-01, 1-4pm',
       subtitle='Flight names in red are missing from Massport noise report') +
  scale_x_datetime(date_breaks='30 min', labels=date_format('%l:%M %p', tz="America/New_York"))

p = p + geom_text_interactive(aes(x=Time, y=80, label=`Flight ID`, tooltip=tip, data_id=Time), 
            data=flights, angle=90, size=2.5, hjust=-0.1,
            color=ifelse(is.na(flights$event_lmax), 'red', 'black')) +
  ylim(NA, 85)

tooltip_css <- "background-color:#ffcccc;font-size:12px;font-family: sans-serif;padding:10px;border-radius:5px;line-height:100%"
ggiraph(code = {print(p)}, width=1, width_svg=10, zoom_max=5,
        hover_css = "fill:#FF4C3B;",
        tooltip_offy=-10,
        tooltip_extra_css=tooltip_css, tooltip_opacity=1)
```

### Airlines and aircraft types

These tables show the top ten airlines and aircraft types represented in the missing data.

```{r}
knitr::kable(list(no_noise %>% group_by(Airline) %>% summarize(count=n()) %>% 
  arrange(desc(count)) %>% top_n(n=10, wt=count),
no_noise %>% group_by(`AC Type`) %>% summarize(count=n()) %>% 
  arrange(desc(count)) %>% top_n(n=10, wt=count)))
```

<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>
  
