# Get monthly tracks and weather for Luke

library(tidyverse)
library(glue)
library(leaflet)
library(lubridate)
library(mapview)
library(RPostgreSQL)
library(rnoaa)
library(sf)
source(rprojroot::find_rstudio_root_file('TrackR', 'DistanceAndLocation.R'))

year_num=2018
month_num=11
month_str = month.abb[month_num]
# Flights
# Location of the meter (estimating the height)
school = st_point(c(-71.098405, 42.411445, 50)) %>% 
  st_sfc(crs=4326)
school_wkt = school %>% st_as_text(EWKT=TRUE)

con=dbConnect(PostgreSQL(), dbname='tracks', user='postgres', password='postgres', host='localhost')

# Using PostGIS st_3dclosestpoint gave some very strange results
# This seems to work better
tr = st_read(con, query=glue('select flight, time, track
                from dep_33l 
                where month={month_num}
                and extract(year from date)={year_num}
                order by time'))

mapview(tr, zcol='flight', alpha=0.3, maxpoints=500000,
        highlight=highlightOptions(color='red', weight=4))

near = nearest_point_and_altitude(tr, st_zm(school)) %>% 
  select(min_dist, altitude_at_min, min_lon, min_lat, time) %>% 
  mutate(time=format(as.POSIXct(time,origin = "1970-01-01", tz = "America/New_York"), usetz=TRUE))

as_tibble(tr) %>% select(flight) %>% bind_cols(near) %>% 
  filter(min_dist <=2500) %>% # 2018-05 filter out some junk
   write_csv(glue('{year_num}_{month_str}_tekkk.csv'))

arr = st_read(con, query=glue("select flight, time, track
              from arr_15r
              where month={month_num}
              and extract(year from date)={year_num}
              order by time"))

near_arr = nearest_point_and_altitude(arr, st_zm(school)) %>% 
  select(min_dist, altitude_at_min, min_lon, min_lat, time) %>% 
  mutate(time=format(as.POSIXct(time,origin = "1970-01-01", tz = "America/New_York"), usetz=TRUE))

# Many non-15R flights fly over TEKKK; filter by altitude
arr_15r = bind_cols(arr, near_arr) %>% filter(altitude_at_min <= 2000)

mapview(arr_15r, zcol='flight', alpha=0.3, maxpoints=500000,
        highlight=highlightOptions(color='red', weight=4))

as_tibble(arr_15r) %>% 
  select(flight, min_dist, altitude_at_min, min_lon, min_lat, time=time1) %>% 
   write_csv(glue('{year_num}_{month_str}_arr_tekkk.csv'))

dbDisconnect(con)

# Weather
# We have to get a new copy to get the new month
kbos_usaf='725090'
kbos_wban='14739'
force = TRUE
kbos=isd(kbos_usaf, kbos_wban, year=year_num, progress=TRUE, additional=FALSE, force=force)

# date_flag == 7 and wind_direction_quality == 5 seems to be the majority
# of observations and to reduce duplication
# Maybe make a ranking system and pick the best of each hourly value?
knots_per_meter_per_second=1.94384
kbos_month = kbos %>% 
  select(date, time, wind_direction, wind_speed, temperature) %>% 
  mutate(datetime=with_tz(ymd_hm(paste(date, time)), 'US/Eastern'),
         date=date(datetime), year=year(datetime), month=month(datetime),
         hour=hour(datetime + minutes(30)), # round to nearest hour; breaks at spring DST transition!
         hour_date=date(datetime+minutes(30)),
         valid = wind_direction!='999' & 
           wind_speed!='9999' & temperature!='+9999') %>% 
  filter(valid) %>% 
  mutate(
    wind_direction=as.numeric(wind_direction),
    wind_speed=as.numeric(wind_speed)*knots_per_meter_per_second/10,
    temperature=round(as.numeric(temperature)*9/50+32), 2) %>% 
  filter(.data$year==year_num, month==month_num) %>%
  select(hour_date, hour, datetime, wind_direction, wind_speed, temperature) %>%
  group_by(hour_date, hour) %>% slice(n())

# Check
qplot(hour_date, hour, data=kbos_month, geom='jitter', shape=I('.'), width=0.25, height=0.25)

kbos_month %>% 
  write_csv(glue::glue('{year_num}_{month_str}_weather.csv'))
