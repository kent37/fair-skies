---
title: "Logan 33L and 15R Flight Tracks over Medford"
author: "Kent Johnson"
output: kjutil::small_format
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=6, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

```{r read_data,results='hide'}
library(tidyverse)
library(leaflet)
library(RPostgreSQL)
library(scales)
library(sf)

con=dbConnect(PostgreSQL(), dbname='tracks', user='postgres', 
              password='postgres', host='localhost')

# Arrivals 6/16 has 260
# Departures 6/21 has 258
dep6_21 = st_read_db(con, query="select flight, time, track
                from dep_33l 
                where date_trunc('day', time) = '2017-06-21 00:00:00'
                order by time")
arr_6_16 = st_read_db(con, query="select flight, time, track
                from arr_15r
                where date_trunc('day', time) = '2017-06-16 00:00:00'
                order by time")

# Convert LINESTRING to line segments
to_lines <- function(tr) { 
    g = as.matrix(tr)
    hd = head(g, -1)
    tl = tail(g, -1)
    lines = lapply(seq_len(nrow(hd)), function(i) st_linestring(rbind(hd[i,], tl[i,])))
    alt = map_dbl(lines, ~.x[1,3])
    tibble(alt = alt, geom=st_sfc(lines, crs=4326))
}

dep6_21$segments = map(dep6_21$track, to_lines)
arr_6_16$segments = map(arr_6_16$track, to_lines)

dep_seg = dep6_21 %>% as_data_frame %>% select(flight, segments) %>% unnest %>% 
  filter(alt<=5000)
dep_seg$geom = st_sfc(dep_seg$geom, crs=4326)
dep_seg = st_sf(dep_seg)

arr_seg = arr_6_16 %>% as_data_frame %>% select(flight, segments) %>% unnest %>% 
  filter(alt<=2500)
arr_seg$geom = st_sfc(arr_seg$geom, crs=4326)
arr_seg = st_sf(arr_seg)
```

This map shows representative tracks of flights departing from Logan Airport runway 33L and
arriving on runway 15R. Data is from ASD-B recording and does not show all flights on the indicated days.

The map shows `r nrow(dep6_21)` departures on June 21, 2017 and `r nrow(arr_6_16)` arrivals on June 16, 2017. 

Departure tracks are shown to 5,000 feet altitude. Arrival tracks are shown from 2,500 feet 
altitude.

Visualization by Kent Johnson.


```{r map}
base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('Esri.WorldTopoMap') %>% 
  setView(-71.0986361111111, 42.4127361111111, zoom = 13)

alts = c(arr_seg$alt, dep_seg$alt)
pal = colorNumeric('plasma', alts)

base_map %>% addPolylines(data=st_zm(dep_seg), color=~pal(alt),
                          label=~as.character(alt), popup=~flight, 
                          weight=1,
                          group='Departures') %>% 
  addPolylines(data=(arr_seg %>% filter(alt<=2500) %>% st_zm), color=~pal(alt),
                          label=~as.character(alt), popup=~flight, 
                          weight=1,
                          group='Arrivals') %>% 
  addLegend(pal=pal, values=alts, title='Altitude') %>% 
  addLayersControl(overlayGroups=c('Arrivals', 'Departures'))
```

<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style="float:right;font-style: italic;">`r Sys.Date()`</span></small>
