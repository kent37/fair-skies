---
title: "Logan 33L Flight Tracks, 2013 vs 2015"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

This map shows the tracks of all flights departing from Logan Airport runway 33L on January 17, 22 and 27 2013, January 5, 10 and 16, 2015 and November 3, 2015. Also shown (in blue-gray) are the waypoints and nominal paths for the RNAV routes from 33L.

Data from Massport.

```{r read_data,results='hide'}
library(ggplot2)
library(leaflet)
library(scales)
source('ReadTracks.R')
source('Waypoints.R')
source('Towns.R')

jan5 = read_tracks('../TrackData/Jan5 2015 33L dep.kml', home)
jan10 = read_tracks('../TrackData/Jan10 2015 33L dep.kml', home)
jan16 = read_tracks('../TrackData/Jan16 2015 33L dep.kml', home)
nov3 = read_tracks('../TrackData/Belmont req nov 3 2015 33Ldep.kml', home)
all2015 = rbind(jan5, jan10, jan16, nov3)
all2015$month = '2015'

jan17 = read_tracks('../TrackData/33L Dep Jan 17 2013.kml', home)
jan22 = read_tracks('../TrackData/33L dept Jan 22 2013.kml', home)

# The original 33L Dept Jan 27 2013.kml has tracks with no coordinates. readOGR doesn't like this;
# here read a modified version with the empty tracks removed
jan27 = read_tracks('../TrackData/33L Dept Jan 27 2013 no empty tracks.kml', home)

all2013 = rbind(jan17, jan22, jan27)
all2013$month = '2013'
all = rbind(all2013, all2015)
all$hour = hour(all$time)

```

```{r map}
# The base map
base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.128184, 42.3769824, zoom = 12) %>% add_towns() %>%    
  add_waypoints_to_map(color='darkslateblue') %>% 
  addMeasure(position='bottomright', primaryLengthUnit='miles',
             secondaryLengthUnit='feet', completedColor='#0000FF')
```

```{r add_lines}
# Add lines colored by year and grouped by date
# Modifies global base_map and groups
groups = NULL
add_lines_for_date = function(tracks, date_str, color)
{
  # Remember group names for the layers control
  groups <<- c(groups, date_str)
  
  base_map <<- addPolylines(base_map, data=tracks, group=date_str,
                            color=color, weight=1, opacity=0.3)
}

add_lines_for_date(jan17, 'Jan 17 2013', 'green')
add_lines_for_date(jan22, 'Jan 22 2013', 'green')
add_lines_for_date(jan27, 'Jan 27 2013', 'green')
add_lines_for_date(jan5, 'Jan 5 2015', 'red')
add_lines_for_date(jan10, 'Jan 10 2015', 'red')
add_lines_for_date(jan16, 'Jan 16 2015', 'red')
add_lines_for_date(nov3, 'Nov 3 2015', 'red')

base_map %>% 
  #addCircles(LON, LAT, opacity=1) %>% 
   # Layers control
  addLayersControl(
    overlayGroups = groups,
    options = layersControlOptions(collapsed = FALSE)
  ) 
```

### Altitude at closest approach

The track data includes the altitude of the flights at each location. For each flight, I found the point of closest approach to my house in North Cambridge and the altitude of the flight at that point. The first plot compares the distance and altitude at closest approach in 2013 to those in 2015. The second plot shows just the distance at closest approach.

```{r}
p = qplot(min_dist, altitude_at_min, data=all@data, color=factor(month), size=I(2), xlim=c(0, 15000))
p = p + labs(x='Minimum distance (feet)', y='Altitude at min distance (feet)') + theme_bw() + facet_wrap(~month, ncol=1)
p + labs(title='Altitude vs distance at nearest approach, 2013 vs 2015') + guides(color=FALSE)

p = qplot(min_dist, data=all@data, fill=factor(month), xlim=c(0, 15000), binwidth=200)
p = p + labs(x='Minimum distance (feet)', y='Number of flights', title='Distribution of minimum distance') + theme_bw()
p + labs(title='Distance at nearest approach, 2013 vs 2015') + guides(fill=FALSE) + facet_wrap(~month, ncol=1)
```

### Time of day

This histogram shows the number of flights per hour for each day.

```{r}
all$wdate = paste(weekdays(all$date), all$date)
p = qplot(hour, data=all@data, fill=factor(month), binwidth=1)
p = p + labs(x='Hour', y='Number of flights') + theme_bw()
p + labs(title='Flights per hour per day') + guides(fill=FALSE) + facet_wrap(~wdate, ncol=1)
```

### Location of closest approach

The final map shows the ground location of the point of closest approach, colored by year.

```{r}
# Need a new base map without the tracks
base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(LON, LAT, zoom = 13) %>% add_towns() %>%    
  add_waypoints_to_map(color='darkslateblue')

base_map %>% 
  addCircles(all$min_lon, all$min_lat, color=c('green', 'red')[as.numeric(factor(all$month))], opacity=0.2) %>% 
  addCircles(LON, LAT, opacity=1)
```

Copyright 2016 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>