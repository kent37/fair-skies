# Help with dump1090 tracks
library(geosphere)
#library(plyr) # Not sure we need this, try to avoid conflicts with dplyr
library(dplyr)
library(purrr)

feet_per_meter = 3.28084
miles_per_meter = feet_per_meter / 5280
nm_per_meter = 1 / 1852 
nm_per_foot = nm_per_meter/feet_per_meter

home = c(-71.131526, 42.396116)
apoole = c(-71.1655453290529,42.3909620891245)

# Default distance function. distGeo is implemented in C and accurate
distfun = distGeo

# Find the point within a track where it is closest to a given location.
# Returns a data frame containing 
# min_dist - the distance on the ground to the point of closest approach
# altitude_at_min - the altitude at POCA
# min_lon, min_lat - the location of POCA
nearest_point_and_altitude = function(track, location)
{
  UseMethod("nearest_point_and_altitude")
}

nearest_point_and_altitude.sf = function(track, location) {
  nearest_point_and_altitude(st_geometry(track), location)
}

nearest_point_and_altitude.sfc = function(track, location) {
  map_df(track, ~nearest_point_and_altitude(., location))
}

# For XYZM, preserve the time attribute
nearest_point_and_altitude.XYZM = function(track, location) {
  nearest_point_and_altitude(
    as.matrix(track)[, 1:4] %>% `colnames<-`(c('lon', 'lat', 'altitude', 'time')), location)
}

nearest_point_and_altitude.sfg = function(track, location) {
  nearest_point_and_altitude(
    as.matrix(track)[, 1:3] %>% `colnames<-`(c('lon', 'lat', 'altitude')), location)
}

nearest_point_and_altitude.SpatialMultiPointsDataFrame = function(track, location)
{
  nearest_point_and_altitude(SpatialMultiPoints(track@coords), location)
}

nearest_point_and_altitude.SpatialMultiPoints = function(track, location)
{
  ldply(track@coords, nearest_point_and_altitude, location=location)
}

nearest_point_and_altitude.tracks = function(tracks, location)
{
  tracks %>% rowwise %>% do(nearest_point_and_altitude(.$track, location))
}

nearest_point_and_altitude.data.frame = function(track, location)
{
  cols = c('lat', 'lon', 'altitude')
  stopifnot(all(cols %in% names(track)))
  if ('time' %in% names(track))
    cols = c(cols, 'time')
  nearest_point_and_altitude(as.matrix(track[,cols]), location)
}

nearest_point_and_altitude.matrix = function(track, location)
{
  stopifnot(all(c('lat', 'lon', 'altitude') %in% colnames(track)))
  
  locs = track[, c('lon', 'lat')]
  
  if (nrow(track) == 1)
  {
    # Only one point, it must be the closest
    distance = distfun(locs, location) * feet_per_meter
    value = data.frame(min_dist=distance, 
                      altitude_at_min=track[1, 'altitude'], 
                      min_lon=track[1, 'lon'], 
                      min_lat=track[1, 'lat'])
    if (dim(track)[2]==4 && colnames(track)[4]=='time')
      value$time=track[1, 'time']
    return(value)
  }
  
  # dist2Line finds the closest point on the track, interpolating between points
  if (inherits(location, 'sf') || inherits(location, 'sfc'))
    location = as(location, 'Spatial')
  d = dist2Line(location, locs, distfun = distfun)
  min_dist = d[1] * feet_per_meter
  min_loc = d[2:3]
  # Find the altitude at the nearest point. Don't interpolate, it is close enough
  ix = which.min(distfun(min_loc, locs))
  alt = track[ix, 'altitude']
  result = data.frame(min_dist=min_dist, altitude_at_min=alt, min_lon=min_loc[1], min_lat=min_loc[2], 
                      as.data.frame(track)[ix,])
  if ('time' %in% colnames(track))
    result$time = track[ix, 'time']
  result
}

# Find the nearest point to a track
# wps should be a SpatialPointsDataFrame whose @data has row names, e.g. waypoints data.
# Returns the row name of the nearest point
nearest_waypoint = function(track, wps)
{
  UseMethod('nearest_waypoint')
}

nearest_waypoint.sf = function(track, location) {
  nearest_waypoint(st_geometry(track), location)
}

nearest_waypoint.sfc = function(track, location) {
  map_chr(track, ~nearest_waypoint(., location))
}

# For XYZM, preserve the time attribute
nearest_waypoint.XYZM = function(track, location) {
  nearest_waypoint(
    as.matrix(track)[, 1:4] %>% `colnames<-`(c('lon', 'lat', 'altitude', 'time')), location)
}

nearest_waypoint.sfg = function(track, location) {
  nearest_waypoint(
    as.matrix(track)[, 1:3] %>% `colnames<-`(c('lon', 'lat', 'altitude')), location)
}

nearest_waypoint.SpatialMultiPointsDataFrame = function(track, location)
{
  nearest_waypoint(SpatialMultiPoints(track@coords), location)
}

nearest_waypoint.SpatialMultiPoints = function(track, location)
{
  sapply(track@coords, nearest_waypoint, location)
}

nearest_waypoint.tracks = function(tracks, wps)
{
  sapply(tracks$track, function(tr) nearest_waypoint(tr, wps))
}

nearest_waypoint.data.frame = function(track, wps)
{
  stopifnot(all(c('lat', 'lon') %in% names(track)))
  nearest_waypoint(as.matrix(track[, c('lon', 'lat')]), wps)
}

nearest_waypoint.matrix = function(track, wps)
{
  stopifnot(all(c('lat', 'lon') %in% colnames(track)))
  d = dist2Line(wps, track[, c('lon', 'lat')], distfun = distfun)
  rownames(wps@data)[which.min(d[,'distance'])]
}

# Functions to work with Spatial* objects
# These are defined as S4 generic methods

# Filter a Spatial* by distance
setGeneric("filter_by_distance", function(poly, p, miles) standardGeneric("filter_by_distance"))

setMethod("filter_by_distance", signature(poly="SpatialPolygons", p='SpatialPoints', miles='numeric'), 
  function(poly, p, miles)
{
  d=sapply(poly@polygons, function(l) dist2Line(p, l@Polygons[[1]]@coords, distfun = distfun))
  d = d[1,] * miles_per_meter
  poly[d<miles,]
})

setMethod("filter_by_distance", signature(poly="SpatialLines", p='SpatialPoints', miles='numeric'), 
  function(poly, p, miles)
{
  d=sapply(poly@lines, function(l) dist2Line(p, l@Lines[[1]]@coords, distfun = distfun))
  d = d[1,] * miles_per_meter
  poly[d<miles,]
})

# Methods to make leaflet::addPolygons work for SpatialMultiPointsDataFrame()
# Now in dev leaflet
# polygonData.SpatialMultiPoints = function(obj)
# {
#   lapply(obj@coords, function(points) list(list(lng=points[,'lon'], lat=points[,'lat']))) %>% 
#     structure(bbox = obj@bbox)
# }
# 
# polygonData.SpatialMultiPointsDataFrame = function(obj)
# {
#   leaflet:::polygonData(sp::SpatialMultiPoints(obj@coords))
# }

# rbind methods for SpatialMultiPoints*
rbind.SpatialMultiPoints <- function(...) {
	dots = list(...)
	names(dots) <- NULL
	stopifnot(identicalCRS(dots))
	SpatialMultiPoints(do.call(c, lapply(dots, slot, name='coords')),
		CRS(proj4string(dots[[1]])))
}

rbind.SpatialMultiPointsDataFrame <- function(...) {
	dots = list(...)
	names(dots) <- NULL
	sp = do.call(rbind, lapply(dots, function(x) as(x, "SpatialMultiPoints")))
	df = do.call(rbind, lapply(dots, function(x) x@data))
	SpatialMultiPointsDataFrame(sp, df)
}

topn = function(d, n=10)
{
  t = table(d)
  head(sort(t, decreasing = TRUE),n = n)
}

# Compute incremental and cumulative distance between points in a data frame,
# in feet. If start is provided, the first distance is from start; otherwise
# it is 0
cumulative_distance = function(locs, start=NA)
{
  locs = as.matrix(locs[, c('lon', 'lat')])
  if (!anyNA(start))
    locs = rbind(start, locs)
  starts = tail(locs, -1)
  ends = head(locs, -1)
  dists = distfun(starts, ends) * feet_per_meter
  if (anyNA(start)) dists = c(0, dists)
  data_frame(dist=dists, cum_dist=cumsum(dists))
}

feet_per_nm_climb = function(track, lag=1)
{
  climb = c(rep(NA, lag), diff(track$altitude, lag=lag))
  dist = c(rep(NA, lag), diff(track$cum_dist, lag=lag))
  climb / (dist * nm_per_foot)
}