---
title: "Logan 33L Flight Tracks"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

This map shows the tracks of all flights from Logan Airport runway 33L on January 5, 10 and 16, 2015.
The tracks are colored by altitude in feet.

Data from Massport.

```{r read_data,results='hide'}
library(leaflet)
library(scales)
source('ReadTracks.R')

jan5 = getKMLcoordinatesFixed('Jan5 2015 33L dep.kml', ignoreAltitude=FALSE)
jan10 = getKMLcoordinatesFixed('Jan10 2015 33L dep.kml', ignoreAltitude=FALSE)
jan16 = getKMLcoordinatesFixed('Jan16 2015 33L dep.kml', ignoreAltitude=FALSE)
jan17 = getKMLcoordinatesFixed('33L Dep Jan 17 2013.kml', ignoreAltitude=FALSE)
jan22 = getKMLcoordinatesFixed('33L dept Jan 22 2013.kml', ignoreAltitude=FALSE)

# The original 33L Dept Jan 27 2013.kml has tracks with no coordinates. readOGR doesn't like this;
# here read a modified version with the empty tracks removed
jan27 = getKMLcoordinatesFixed('33L Dept Jan 27 2013 no empty tracks.kml', ignoreAltitude=FALSE)
all = c(jan5, jan10, jan16, jan17, jan22, jan27)


towns = readOGR('townssurvey_shp', 'TOWNSSURVEY_ARC', stringsAsFactors=FALSE)
towns = subset(towns, OUTLINE==17)
towns = spTransform(towns, CRS("+init=EPSG:4326"))

# We want to add every line segment to the map, colored by it's altitude.
# There is too much overhead in formatting to add each segment individually.
# Instead we build a SpatialLines object where each Lines component consists
# of segments at the same altitude. Altitude is binned to 100-foot increments.
all_altitudes = unlist(sapply(all, function(t) t[,3])) * feet_per_meter
binned_altitudes = as.integer(all_altitudes/100) + 1
max_bin = max(binned_altitudes)
```

```{r map}
# The base map
nColor = 20
colors = col_quantile(colorRamp(c('red', 'dimgray', 'blue')), binned_altitudes, n=nColor)

base_map = leaflet(width='95%', height=800) %>% 
  addTiles(urlTemplate='https://{s}.tiles.mapbox.com/v3/kent37.npinlfln/{z}/{x}/{y}.png',
           attribution='<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>') %>% 
  setView(-71.128184, 42.3769824, zoom = 12) %>% 
  addPolylines(data=towns, color='slateblue', weight=2)
```

```{r add_lines}
# Add lines colored by altitude and grouped by date
# Modifies global base_map and groups
groups = NULL
add_lines_for_date = function(tracks, date_str)
{
  # Remember group names for the layers control
  groups <<- c(groups, date_str)
  
  # Split tracks into separate lists per binned altitude
  lines_list = vector('list', max_bin)
  for (i in 1:max_bin) lines_list[[i]] = list()
  
  for (track in tracks)
    for (i in 1:(nrow(track)-1))
    {
      line = Line(track[i:(i+1), 1:2])
      alt = as.integer(track[i,3] * feet_per_meter / 100) + 1
      lines_list[[alt]] = c(lines_list[[alt]], line)
    }
  
  for (i in 1:max_bin)
  {
    ll = lines_list[[i]]
    if (length(ll)==0) next
    lines = mapply(Lines, ll, 1:length(ll))
    spl = SpatialLines(lines, CRS("+proj=longlat +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +no_defs"))
    base_map <<- addPolylines(base_map, data=spl, group=date_str,
                            color=colors(i), weight=1, opacity=0.5)
  }
}

add_lines_for_date(jan5, 'Jan 5 2015')
add_lines_for_date(jan10, 'Jan 10 2015')
add_lines_for_date(jan16, 'Jan 16 2015')
add_lines_for_date(jan17, 'Jan 17 2013')
add_lines_for_date(jan22, 'Jan 22 2013')
add_lines_for_date(jan27, 'Jan 27 2013')

base_map %>% 
   # Layers control
  addLayersControl(
    overlayGroups = groups,
    options = layersControlOptions(collapsed = FALSE)
  ) %>% 
  addLegend('bottomright', colors, binned_altitudes, title='Altitude',
                       labFormat = function(type, cuts, p) as.character(cuts*100))
```
