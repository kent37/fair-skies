library(classInt)
library(leaflet)
library(RColorBrewer)
library(rgdal)
source('../ADSB/TrackUtils.R')

d=readOGR('/Users/kent/Downloads/CENSUS2010_BLK_BG_TRCT_SHP/', 'CENSUS2010BLOCKGROUPS_POLY')
d=spTransform(d, CRS("+init=EPSG:4326"))

m=leaflet() %>% addProviderTiles('CartoDB.Positron')

#m  %>%  addPolygons(data=d2)

d$density = d$POP100_RE / d$AREA_ACRES * 640
natural.interval = classIntervals(d$density, n = 9, style = 'jenks')$brks

#d$density_cut = cut(d$density, breaks=quantile(d$density, 0:9/9), include.lowest=TRUE)
d$density_cut = cut(d$density, breaks=natural.interval, include.lowest=TRUE)

pal = brewer.pal(n=9, name='YlOrRd')

for (i in 1:9)
{
  l = levels(d$density_cut)[i]
  dcut = d[d$density_cut==l,]
  m = m %>% addPolygons(data=dcut, stroke=FALSE, fillColor=pal[i], fillOpacity=0.4, group=l)
}
  
m %>%
  addPolylines(data=towns, color='slateblue', weight=2) %>% 
  add_waypoints() %>% 
  addLegend(title='Pop density / sq mi', labels=levels(d$density_cut), colors=pal)


