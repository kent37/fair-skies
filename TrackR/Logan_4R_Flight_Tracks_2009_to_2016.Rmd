---
title: "Logan 4R Flight Tracks, 2009 to 2016"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

This map shows the tracks of flights arriving on Logan Airport runway 4R on
two days in April for each year 2009-2011 and 2014-2016 (twelve days total). 
Tracks are shown from an altitude of 5,000 feet until landing at Logan.

```{r read_data,results='hide'}
library(ggplot2)
library(knitr)
library(leaflet)
library(lubridate)
library(scales)
library(sf)
#source('ReadTracks.R')
source('DistanceAndLocation.R')
source('Towns.R')
source('parse_kml.R')
source('st_clip_z.R')
```

```{r data,results='hide'}
feet_per_meter = 3.28084
locations = read_sf('/Users/kent/Downloads/RWY04/Locations.kml', 'Untitled layer') %>% st_zm

parse_kml = function(path) {
  cat(path, '\n')
  layer = basename(path)
  kml = read_sf(path, layer)
  meta = parse_extended_data(path)
  bind_cols(kml, meta)
}

# If we have saved data with tracks and nearest approach, use it
d_near_path = 'Runway_4R_Arrivals_Near.RData'
if (file.exists(d_near_path)) {
  load(d_near_path)
} else {
  # Do we have saved tracks
  d4r_path = 'Runway_4R_Arrivals.RData'
  if (file.exists(d4r_path)) {
    load(d4r_path)
  } else {
    # Don't use map_df or bind_rows, they break the geometry
    d = list.files('/Users/kent/Downloads/RWY04 selected dates/', pattern='\\.kml$', full.names=TRUE) %>% 
      map(parse_kml)
    d = do.call(rbind, d)
    d4r = d[d$Runway=='4R',]
    d4r$year = year(ymd_hms(d4r$`Track Start Time`))
    save(d4r, file=d4r_path)
  }
  # Find just the tracks that are close to location 1
  da = nearest_point_and_altitude(d4r, locations[1,])
  d_near = bind_cols(d4r, 
                     da[, c('min_dist', 'altitude_at_min', 'min_lon', 'min_lat')]) %>% 
    filter(min_dist<5280, altitude_at_min<1000)
  save(d_near, file=d_near_path)
  
}
d_near$date = ymd_hms(d_near$`Track End Time`) %>% date
d_near$hour = ymd_hms(d_near$`Track End Time`) %>% hour
```

```{r map}
# Add lines colored by year and grouped by date
# Modifies global base_map and groups
add_lines_for_date = function(tracks, date_str, color)
{
  # Remember group names for the layers control
  groups <<- c(groups, date_str)
  
  base_map <<- addPolylines(base_map, data=st_zm(tracks), group=date_str,
                            color=color, weight=1, opacity=0.15)
}

# The base map
base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.048, 42.28, zoom = 11) %>% add_towns() %>% 
  addCircles(data=locations[1,], label='Fontbonne Academy') %>% 
  addMeasure(position='bottomright', primaryLengthUnit='miles',
             secondaryLengthUnit='feet', completedColor='#0000FF')

# Clip the tracks to 5000 feet to reduce the size a little
d_map = st_clip_z(d_near, max_z=5000/feet_per_meter)
groups = NULL
add_lines_for_date(d_map %>% filter(year==2009), '2009', 'green')
add_lines_for_date(d_map %>% filter(year==2010), '2010', 'green')
add_lines_for_date(d_map %>% filter(year==2011), '2011', 'green')

add_lines_for_date(d_map %>% filter(year==2014), '2014', 'red')
add_lines_for_date(d_map %>% filter(year==2014), '2015', 'red')
add_lines_for_date(d_map %>% filter(year==2016), '2016', 'red')

base_map %>% 
  addLayersControl(
    overlayGroups = groups,
    # overlayGroups = c(groups, "Locations"),
    options = layersControlOptions(collapsed = FALSE)
  ) 
```
  
The actual dates, and the number of flights on each date (a total of `r nrow(d_near)` flights):  
`r as.data.frame(table(d_near$date)) %>% 
   {paste0(.$Var1, ' - ', .$Freq, ' flights', collapse='  \n')}`.

### Altitude at closest approach

The track data includes the altitude of the flights at each location. For each flight, I found the point of closest approach to Fontbonne Academy (930 Brook Rd, blue circle on map) and the altitude of the flight at that point. The first plot compares the distance and altitude at closest approach in 2009-2011 to those in 2014-2016. The second plot shows just the altitude at closest approach.

```{r}
ggplot(aes(min_dist, altitude_at_min*feet_per_meter, color=factor(year)), data=d_near) +
  geom_point(size=1, alpha=0.5) + xlim(c(500, 2000)) +
  labs(x='Minimum distance (feet)', y='Altitude at nearest approach (feet)') + theme_bw() +
  facet_wrap(~year, ncol=1) +
  labs(title='Altitude vs distance at nearest approach, 2009-2011 vs 2014-2016') + 
  guides(color=FALSE) +
  scale_color_manual(values=c('2009'='green4', '2010'='green4', '2011'='green4', 
                              '2014'='red', '2015'='red', '2016'='red'))

qplot(altitude_at_min*feet_per_meter, data=d_near, fill=factor(year), binwidth=20) +
  labs(x='Altitude at nearest approach (feet)', y='Number of flights', 
       title='Distribution of Altitude at Nearest Approach, 2009-2011 vs 2014-2016') + 
  theme_bw() + guides(fill=FALSE) + 
  facet_wrap(~year, ncol=1) + xlim(c(1750, 2250)) +
  scale_fill_manual(values=c('2009'='green4', '2010'='green4', '2011'='green4', 
                             '2014'='red', '2015'='red', '2016'='red'))

```

### Time of day

This histogram shows the number of flights per hour for each day.

```{r}
qplot(hour, data=d_near, fill=factor(year), binwidth=1) +
  labs(x='Hour', y='Number of flights') + theme_bw() +
  labs(title='Flights per hour per day') + guides(fill=FALSE) + facet_wrap(~year, ncol=1) +
  scale_fill_manual(values=c('2009'='green4', '2010'='green4', '2011'='green4', 
                             '2014'='red', '2015'='red', '2016'='red'))
```


### Location of closest approach

The final map shows the ground location of the point of closest approach, colored by year.

```{r}
add_circles_for_date = function(tracks, date_str, color)
{
  # Remember group names for the layers control
  groups <<- c(groups, date_str)
  
  base_map <<- addCircles(base_map, lng=tracks$min_lon, lat=tracks$min_lat, group=date_str,
                            color=color, opacity=0.15)
}

# Need a new base map
base_map = leaflet(width='95%', height=400) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.0594803, 42.2595406, zoom = 14) %>% add_towns() %>% 
  addCircles(data=locations[1,], label='Fontbonne Academy') %>% 
  addMeasure(position='bottomright', primaryLengthUnit='miles',
             secondaryLengthUnit='feet', completedColor='#0000FF')

groups = NULL
add_circles_for_date(d_map %>% filter(year==2009), '2009', 'green')
add_circles_for_date(d_map %>% filter(year==2010), '2010', 'green')
add_circles_for_date(d_map %>% filter(year==2011), '2011', 'green')

add_circles_for_date(d_map %>% filter(year==2014), '2014', 'red')
add_circles_for_date(d_map %>% filter(year==2014), '2015', 'red')
add_circles_for_date(d_map %>% filter(year==2016), '2016', 'red')

base_map %>% 
  addLayersControl(
    overlayGroups = groups,
    # overlayGroups = c(groups, "Locations"),
    options = layersControlOptions(collapsed = FALSE)
  ) 
```

Data from Massport, visualization by Kent Johnson.

Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>