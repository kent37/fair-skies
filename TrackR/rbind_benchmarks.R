library(sf)
library(microbenchmark)

# Make some test data - a list of single st_sf objects, each
# containing a single row with a LINESTRING and metadata
storms <- st_read(system.file("shape/storms_xyzm.shp", package="sf"))
storms$id = seq_len(nrow(storms))
n = 4000
data = vector('list', n)
for (i in 1:n) data[[i]] = storms[i %/% 71 + 1,]

# Benchmark; just run each one time
microbenchmark(do.call(rbind, data), 
               st_sf(plyr::rbind.fill(data), crs=st_crs(storms)),
               times=1)
tracks = do.call(rbind, data)
tracks = dplyr::bind_rows(data)
tracks = plyr::rbind.fill(data)
tracks = st_sf(tracks, crs=st_crs(storms))
