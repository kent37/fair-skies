library(classInt)
library(geosphere)
library(here)
library(rgdal)
library(tidyverse)
#source('DistanceAndLocation.R')

cache = here('Towns/towns.RData')
if (file.exists(cache))
    load(cache)

if (!exists('towns'))
{
  towns = readOGR(here('Towns/townssurvey_shp'), 'TOWNSSURVEY_ARC', stringsAsFactors=FALSE)
  towns = subset(towns, OUTLINE==17)
  towns = spTransform(towns, CRS("+init=EPSG:4326"))

  nearby_towns = filter_by_distance(towns, kbos, 40)
}

if (!exists('block_groups'))
{
  block_groups = readOGR('/Users/kent/Dev/CENSUS2010_BLK_BG_TRCT_SHP/', 'CENSUS2010BLOCKGROUPS_POLY')
  block_groups=spTransform(block_groups, wgs84)
  block_groups$density = block_groups$POP100_RE / block_groups$AREA_ACRES * 640

  nearby_block_groups = filter_by_distance(block_groups, kbos, 40)
  nb = 9
  natural.interval = classIntervals(nearby_block_groups$density, n = nb, style = 'jenks')$brks

  s = signif(natural.interval, 2)
  labels = paste0(s[-nb-1], ' - ', s[-1])
  nearby_block_groups$density_cut = 
    cut(nearby_block_groups$density, breaks=natural.interval, include.lowest=TRUE, labels=labels)
  
  income = read_csv(here("Towns/ACS_Median_income/ACS_15_5YR_B19013_with_ann.csv"), 
                    col_types = cols(HD01_VD01 = col_integer(),
                                     HD02_VD01 = col_integer())) %>% 
    na.omit %>% rename(income=HD01_VD01) %>% select(GEO.id2, income)

  nearby_block_groups = merge(nearby_block_groups, income, by.x='GEOID10', by.y='GEO.id2')
  natural.interval = classIntervals(nearby_block_groups$income, n = nb, style = 'jenks')$brks

  s = signif(natural.interval, 2) %>% format(trim=TRUE)
  labels = paste0(s[-nb-1], ' - ', s[-1])
  nearby_block_groups$income_cut = 
    cut(nearby_block_groups$income, breaks=natural.interval, include.lowest=TRUE, labels=labels)
}

add_towns = function(m, color='slateblue', weight=2)
  m %>% addPolylines(data=nearby_towns, color=color, weight=weight)

add_population_density = function(m)
{
  library(RColorBrewer)
  nb = length(levels(nearby_block_groups$density_cut))
  pal = brewer.pal(n=nb, name='YlOrRd')
  tooltips = lapply(paste0('Pop: ', nearby_block_groups$POP100_RE, '<br>',
                    'Dens: ', round(nearby_block_groups$density)),
                    htmltools::HTML)
  for (i in 1:nb)
  {
    l = levels(nearby_block_groups$density_cut)[i]
    dcut = nearby_block_groups[nearby_block_groups$density_cut==l,]
    m = m %>% addPolygons(data=dcut, stroke=FALSE, fillColor=pal[i], fillOpacity=0.4, popup=tooltips[nearby_block_groups$density_cut==l], group='Population')
  }
  m %>%  addLegend(title='Pop density / sq mi', labels=levels(nearby_block_groups$density_cut), 
                   colors=pal, position='bottomright')
}

add_median_income = function(m)
{
  library(RColorBrewer)
  nb = length(levels(nearby_block_groups$income_cut))
  pal = brewer.pal(n=nb, name='YlOrRd')
  nbg = nearby_block_groups[!is.na(nearby_block_groups$income_cut),]
  tooltips = lapply(paste0('Pop: ', nbg$POP100_RE, '<br>',
                    'Income: $', round(nbg$income)),
                    htmltools::HTML)
  for (i in 1:nb)
  {
    l = levels(nbg$income_cut)[i]
    dcut = nbg[nbg$income_cut==l,]
    m = m %>% addPolygons(data=dcut, stroke=FALSE, fillColor=pal[i], fillOpacity=0.4, 
                          popup=tooltips[nbg$income_cut==l], group='Median income')
  }
  m %>% addLegend(title='Median income', labels=levels(nbg$income_cut), 
                  colors=pal, position='bottomright')
}

if (!file.exists(cache))
  save(towns, nearby_towns, block_groups, nearby_block_groups, file=cache)
