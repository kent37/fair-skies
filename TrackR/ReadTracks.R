# Read flight track data and annotate with nearest point and altitude
library(geosphere)
library(ggplot2)
library(lubridate)
library(maptools)
library(plyr)
library(rgdal)
library(stringr)

# Fixed version of getKMLcoordinates
source(here::here('TrackR/getKMLcoordinates.R'))

LAT=42.396116
LON=-71.131526
home = c(LON, LAT)
apoole = c(-71.1655453290529,42.3909620891245)

feet_per_meter = 3.28084

nearest_point_and_altitude = function(track, location)
{
  stopifnot(dim(track)[2] == 3)
  
  locs = track[,1:2]
  
  # dist2Line finds the closest point on the track, interpolating between points
  d = dist2Line(location, locs)
  min_dist = d[1] * feet_per_meter
  min_loc = d[2:3]
  # Find the altitude at the nearest point. Don't interpolate, it is close enough
  alt = track[which.min(distHaversine(min_loc, locs)), 3] * feet_per_meter
  data.frame(min_dist=min_dist, altitude_at_min=alt, min_lon=min_loc[1], min_lat=min_loc[2])
}

read_tracks = function(path, location=NULL, layer='Tracks')
{
  tracks = readOGR(path, layer, verbose=FALSE)
  with_altitude = getKMLcoordinatesFixed(path, ignoreAltitude=FALSE)
  
  # Get aircraft type and time from Description
  d = str_match(tracks$Description, 'AC:</b>(.*?)<b>.*Time:</b>(.*)')
  tracks$aircraft = factor(d[,2])
  tracks$time = ymd_hms(d[,3], tz='EST')
  tracks$date = as.Date(tracks$time, tz='EST')
  
  if (!is.null(location)) {
    nearest = ldply(with_altitude, nearest_point_and_altitude, location)
    tracks@data = cbind(tracks@data, nearest)
#   tracks$min_dist = nearest$min_dist
#   tracks$altitude_at_min = nearest$altitude_at_min
  }
  tracks$Description = NULL
  
  # Make unique IDs so we can rbind multiple sets of tracks
  tracks = spChFIDs(tracks, paste(tracks$date, seq_len(nrow(tracks))))
  tracks
}
