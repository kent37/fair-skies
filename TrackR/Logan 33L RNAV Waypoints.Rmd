---
title: |
  | Logan Runway 33L RNAV Routes
  | *Highways in the Sky*

author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

```{r results='hide'}
library(geosphere)
library(leaflet)
library(rgdal)
source('Towns.R')
source('Waypoints.R')
```

This map shows the location of RNAV waypoints and the tracks connecting them for departures from Logan Airport runway 33L. 

RNAV departure procedures are as published for the
period October 15, 2015 - November 12, 2015 at http://www.airnav.com/airport/KBOS. 

Waypoint locations are from http://opennav.com. Click on a waypoint to
see its name and a link to more information.

<!--Population and land area are from the U.S. Census Bureau 2010 census. Household median income is from
U.S. Census Bureau, 2011-2015 American Community Survey 5-Year Estimates.-->

Note: flights from runway 33L climb at heading 331° to intercept course 314° to TEKKK, so the path from KBOS to TEKKK is not straight.

```{r}

base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.098636, 42.412736, zoom = 10)

base_map %>% 
  #add_population_density() %>% add_median_income() %>% 
  add_towns() %>% add_waypoints_to_map() %>% 
  addLayersControl(baseGroups=c('Population', 'Median income'))
```

Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>
  