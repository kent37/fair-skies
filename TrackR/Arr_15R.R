# Investigate 15R and 33L in June

dep = st_read_db(con, query='select flight, time, track
                from dep_33l 
                where month = 6
                and extract(year from date)=2017
                order by time')

arr = st_read_db(con, query='select flight, time, track
                from arr_15r 
                where month = 6
                and extract(year from date)=2017
                order by time')

dep %>% as_data_frame %>% select(-track) %>% mutate(op='dep') %>% 
  bind_rows(arr %>% as_data_frame %>% select(-track) %>% mutate(op='arr')) %>% 
  ggplot(aes(as.Date(time), fill=op)) + geom_histogram(binwidth=1) + facet_wrap(~op, ncol=1)

dep %>% as_data_frame %>% select(-track) %>% mutate(op='dep') %>% 
  bind_rows(arr %>% as_data_frame %>% select(-track) %>% mutate(op='arr')) %>% 
  with(table(as.Date(time), op))

# Arrivals 6/16 has 260
# Departures 6/21 has 258
dep6_21 = st_read_db(con, query="select flight, time, track
                from dep_33l 
                where date_trunc('day', time) = '2017-06-21 00:00:00'
                order by time")
arr_6_16 = st_read_db(con, query="select flight, time, track
                from arr_15r
                where date_trunc('day', time) = '2017-06-16 00:00:00'
                order by time")

# Convert LINESTRING to line segments
to_lines <- function(tr) { 
    g = as.matrix(tr)
    hd = head(g, -1)
    tl = tail(g, -1)
    lines = lapply(seq_len(nrow(hd)), function(i) st_linestring(rbind(hd[i,], tl[i,])))
    alt = map_dbl(lines, ~.x[1,3])
    tibble(alt = alt, geom=st_sfc(lines, crs=4326))
}

dep6_21$segments = map(dep6_21$track, to_lines)
arr_6_16$segments = map(arr_6_16$track, to_lines)

dep_seg = dep6_21 %>% as_data_frame %>% select(flight, segments) %>% unnest %>% 
  filter(alt<=5000)
dep_seg$geom = st_sfc(dep_seg$geom, crs=4326)
dep_seg = st_sf(dep_seg)

arr_seg = arr_6_16 %>% as_data_frame %>% select(flight, segments) %>% unnest %>% 
  filter(alt<=2500)
arr_seg$geom = st_sfc(arr_seg$geom, crs=4326)
arr_seg = st_sf(arr_seg)

library(leaflet)

base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('Esri.WorldTopoMap') %>% 
  setView(-71.128184, 42.3769824, zoom = 12)

alts = c(arr_seg$alt, dep_seg$alt)
pal = colorNumeric('plasma', alts)

base_map %>% addPolylines(data=st_zm(dep_seg), color=~pal(alt),
                          label=~as.character(alt), popup=~flight, 
                          weight=1,
                          group='Departures') %>% 
  addPolylines(data=(arr_seg %>% filter(alt<=2500) %>% st_zm), color=~pal(alt),
                          label=~as.character(alt), popup=~flight, 
                          weight=1,
                          group='Arrivals') %>% 
  addLegend(pal=pal, values=alts, title='Altitude') %>% 
  addLayersControl(overlayGroups=c('Arrivals', 'Departures'))
