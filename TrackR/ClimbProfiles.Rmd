---
title: "Climb Profiles"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)

library(ggplot2)
source('Towns.R')
```

```{r}
source('getKMLcoordinates.R')
jan5 = getKMLcoordinatesFixed('../TrackData/Jan5 2015 33L dep.kml', ignoreAltitude=FALSE)
jan10 = getKMLcoordinatesFixed('../TrackData/Jan10 2015 33L dep.kml', ignoreAltitude=FALSE)
jan16 = getKMLcoordinatesFixed('../TrackData/Jan16 2015 33L dep.kml', ignoreAltitude=FALSE)

d = do.call(rbind, c(jan5, jan10, jan16))

dist_bos = distHaversine(kbos, d[,1:2])*miles_per_meter

# Make a data frame with waypoint info
wps = c('TEKKK', 'COUSY', 'CAMWI', 'DUBYU', 'EXXXX')
dist_bos_wp = distHaversine(kbos, waypoints[wps,]) * miles_per_meter
wp_vjust = -0.2-(0:4)
wp_df = data.frame(dist_bos_wp, y=0, hjust=-0.1, vjust=wp_vjust, label=wps)
wp_df$hjust[4] = 1.1

p = qplot(dist_bos, d[,3]*feet_per_meter, size=I(1), alpha=I(0.3), xlim=c(0, 20), ylim=c(0, 10000))
p = p + geom_vline(xintercept=dist_bos_wp, color='blue')
p = p + geom_text(data=wp_df, aes(x=dist_bos_wp, y=y, hjust=hjust, vjust=vjust, label=label), color='blue')

p + labs(x='Distance from KBOS (miles)', y='Altitude (ft)', title='Climb profiles from Logan 33L, Jan 2015')

```

