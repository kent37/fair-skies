---
title: "Logan 33L Flight Tracks, Jan 2015 by RNAV"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```
Data from Massport. Waypoints are shown in magenta.

```{r read_data,results='hide'}
library(ggplot2)
library(leaflet)
library(pander)
library(rgeos)
library(scales)
source('../ADSB/TrackUtils.R') # For waypoints; do this befor ReadTracks
source('ReadTracks.R')

jan5 = read_tracks('Jan5 2015 33L dep.kml', home)
jan10 = read_tracks('Jan10 2015 33L dep.kml', home)
jan16 = read_tracks('Jan16 2015 33L dep.kml', home)

towns = readOGR('townssurvey_shp', 'TOWNSSURVEY_ARC', stringsAsFactors=FALSE)
towns = subset(towns, OUTLINE==17)
towns = spTransform(towns, CRS("+init=EPSG:4326"))

first_waypoints = waypoints[c('COUSY', 'EXXXX', 'DUBYU', 'CAMWI'),]
waypoints_sp = SpatialPointsDataFrame(first_waypoints[,c('lon', 'lat')], first_waypoints, proj4string=CRS(proj4string(jan10)))
tekkk = waypoints['TEKKK',]
```

```{r map}
# The base map

popups = with(first_waypoints, paste0(Name, "<br>", Location))

base_map = leaflet(width='95%', height=800) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.098636, 42.412736, zoom = 10) %>% 
  addPolylines(data=towns, color='slateblue', weight=2)
```

```{r functions}
# Figure out which waypoint is closest to each line
closest_waypoint = function(all)
{
  for (i in 1:nrow(all))
  {
    d = dist2Line(waypoints_sp, all[i,])
    all$waypoint[i] = waypoints_sp$Name[which.min(d[,'distance'])]
  }
  all$waypoint = factor(all$waypoint)
  all
}

map_by_waypoints = function(all)
{
  colors = c('red', 'green', 'blue', 'cyan')
  groups = levels(all$waypoint)
  
  base_map %>% 
  addPolylines(data=all, group=all$waypoint,
                              color=colors[all$waypoint], weight=1, opacity=0.3) %>% 
  
    addCircles(first_waypoints$lon, first_waypoints$lat, color='magenta', radius=200, popup = popups) %>% 
    addCircles(tekkk$lon, tekkk$lat, color='magenta', radius=200, popup='TEKKK<br>Medford, MA') %>% 
  # Layers control
    addLayersControl(
      overlayGroups = groups,
      options = layersControlOptions(collapsed = FALSE)
  )
}
```

### Jan 5, 2015

This map shows the tracks of all flights departing from Logan Airport runway 33L on January 5, 2015.

```{r}
jan5_wp = closest_waypoint(jan5)
map_by_waypoints(jan5_wp)
```

#### Number of flights per RNAV track

```{r}
pander(table(jan5_wp$waypoint))
```

### Jan 10, 2015

This map shows the tracks of all flights departing from Logan Airport runway 33L on January 10, 2015.

```{r}
jan10_wp = closest_waypoint(jan10)
map_by_waypoints(jan10_wp)
```

#### Number of flights per RNAV track

```{r}
pander(table(jan10_wp$waypoint))
```

### Jan 16, 2015

This map shows the tracks of all flights departing from Logan Airport runway 33L on January 16, 2015.

```{r}
jan16_wp = closest_waypoint(jan16)
map_by_waypoints(jan16_wp)
```

#### Number of flights per RNAV track

```{r}
pander(table(jan16_wp$waypoint))
```

