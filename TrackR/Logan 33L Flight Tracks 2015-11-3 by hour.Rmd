---
title: "Logan 33L Flight Tracks, Nov. 3, 2015"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output: html_document
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=5, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```

This map shows the tracks of all flights departing from Logan Airport runway 33L on November 3, 2015.

Data from Massport.

```{r read_data,results='hide'}
library(ggplot2)
library(leaflet)
library(scales)
source('DistanceAndLocation.R')
source('Towns.R')
source('read_kml.R')
source('Waypoints.R')

nov3 = read_kml('../TrackData/Belmont req nov 3 2015 33Ldep.kml')
```

```{r map, fig.height=8}
# The base map
base_map = leaflet(width='95%', height=1200) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.128184, 42.3769824, zoom = 10) %>% 
  add_towns() %>% add_waypoints_to_map(color='darkslateblue')
```

```{r add_lines}
# Add lines grouped by hour
# Modifies global base_map and groups
groups = NULL
add_lines = function(tracks, color)
{
  # Remember group names for the layers control
  for (hour in unique(tracks$hour))
  {
    by_hour = tracks[tracks$hour==hour,]
    hour = as.character(hour)
    groups <<- c(groups, hour)
    base_map <<- addPolylines(base_map, data=by_hour,
                              group=hour,
                              color=color, weight=1, opacity=0.3)
  }
}

add_lines(nov3, 'red')

base_map %>% 
  addLayersControl(
    overlayGroups = as.character(sort(unique(groups))),
    options = layersControlOptions(collapsed = FALSE)
  ) 
```

### Altitude at closest approach

The track data includes the altitude of the flights at each location. For each flight, I found the point of closest approach to a location in North Cambridge and the altitude of the flight at that point. The plot shows the distance at closest approach.

```{r}
close_to_home = ldply(nov3@coords, nearest_point_and_altitude, location=home)
p = qplot(min_dist, altitude_at_min, data=close_to_home, xlim=c(0, 15000), color=I('red'))
p = p + labs(x='Minimum distance (feet)', y='Altitude') + theme_bw()
p + labs(title='Altitude vs Distance at nearest approach, N. Cambridge, Nov. 3 2015') 
```

This plot shows the points of closest approach to a location in Belmont.

```{r}
close_to_apoole = ldply(nov3@coords, nearest_point_and_altitude, location=apoole)
p = qplot(min_dist, altitude_at_min, data=close_to_apoole, xlim=c(0, 15000), color=I('red'))
p = p + labs(x='Minimum distance (feet)', y='Altitude') + theme_bw()
p + labs(title='Altitude vs Distance at nearest approach, Belmont, Nov. 3 2015') 
```

### Time of day

This histogram shows the number of flights per hour for each day.

```{r}
p = qplot(hour, data=nov3@data, binwidth=1, fill=I('red'))
p = p + labs(x='Hour', y='Number of flights') + theme_bw()
p + labs(title='Flights per hour, Nov 3, 2015')
```

