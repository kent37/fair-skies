library(rmarkdown)

basePath = '/Volumes/TRACKS/2016-02'
date = '2016-02-26'
render('ClimbPaths.Rmd', output_file=paste0('ClimbPaths_', date, '.html'))

basePath = '/Volumes/TRACKS/2016-03'
date = '2016-03-21'
render('ClimbPaths.Rmd', output_file=paste0('ClimbPaths_', date, '.html'))

basePath = '/Volumes/TRACKS/tracks'
date = '2016-05-08'
render('ClimbPaths.Rmd', output_file=paste0('ClimbPaths_', date, '.html'))
